﻿using System;
using System.Runtime.ExceptionServices;

namespace ConnectorETABS.ETABS.UI
{
    public class UIApplication : IDisposable
    {
        private bool disposed = false;
        public ETABSv1.cOAPI Application;

        ~UIApplication()
        {
        }
        public UIDocument ActiveDocument { get; set; }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        [HandleProcessCorruptedStateExceptions]
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                Application = null;
            }
        }
    }
}

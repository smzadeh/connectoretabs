﻿using System;
using System.Runtime.ExceptionServices;

namespace ConnectorETABS.ETABS.UI
{
    public class UIDocument : IDisposable
    {
        private bool disposed = false;
        public ETABSv1.cSapModel Document;

        ~UIDocument()
        {
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        [HandleProcessCorruptedStateExceptions]
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                Document = null;
            }
        }
    }
}

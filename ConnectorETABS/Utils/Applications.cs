﻿namespace ConnectorETABS
{
    public static class Applications
    {
#if ETABS19
        public const string ETABS19 = "ETABS19";
#else
        public const string ETABS18 = "ETABS18";
#endif
    }
}

﻿using Speckle.Core.Logging;
using Speckle.DesktopUI.Utils;
using Stylet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace ConnectorETABS
{
    public partial class ConnectorBindingsETABS
    {

        public List<StreamState> DocumentStreams { get; set; } = new List<StreamState>();


        public List<Exception> ConversionErrors { get; set; } = new List<Exception>();

        /// <summary>
        /// Keeps track of errors in the operations of send/receive.
        /// </summary>
        public List<Exception> OperationErrors { get; set; } = new List<Exception>();

        public override List<StreamState> GetStreamsInFile()
        {
            if (CurrentDoc != null)
                DocumentStreams = StreamStateManager.ReadState(CurrentDoc);

            return DocumentStreams;
        }

        #region Local file i/o

        /// <summary>
        /// Adds a new stream to the file.
        /// </summary>
        /// <param name="state">StreamState passed by the UI</param>
        public override void AddNewStream(StreamState state)
        {
            Tracker.TrackPageview(Tracker.STREAM_CREATE);
            var index = DocumentStreams.FindIndex(b => b.Stream.id == state.Stream.id);
            if (index == -1)
            {
                DocumentStreams.Add(state);
                WriteStateToFile();
            }
        }


        /// <summary>
        /// Removes a stream from the file.
        /// </summary>
        /// <param name="streamId"></param>
        public override void RemoveStreamFromFile(string streamId)
        {

            var streamState = DocumentStreams.FirstOrDefault(s => s.Stream.id == streamId);
            if (streamState != null)
            {
                DocumentStreams.Remove(streamState);
                WriteStateToFile();
            }
        }

        /// <summary>
        /// Update the stream state and adds adds the filtered objects
        /// </summary>
        /// <param name="state"></param>
        public override void PersistAndUpdateStreamInFile(StreamState state)
        {
            var index = DocumentStreams.FindIndex(b => b.Stream.id == state.Stream.id);
            if (index != -1)
            {
                DocumentStreams[index] = state;
                WriteStateToFile();
            }
        }

        /// <summary>
        /// Writes state to file
        /// </summary>
        private void WriteStateToFile()
        {
            StreamStateManager.WriteStreamStateList(CurrentDoc, DocumentStreams);
        }

        #endregion

        private void UpdateProgress(ConcurrentDictionary<string, int> dict, ProgressReport progress)
        {
            if (progress == null) return;

            Execute.PostToUIThread(() =>
            {
                progress.ProgressDict = dict;
                progress.Value = dict.Values.Last();
            });
        }

    }
}

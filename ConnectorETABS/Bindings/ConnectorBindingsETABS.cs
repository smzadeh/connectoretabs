﻿using ConnectorETABS.ETABS.UI;
using Speckle.Core.Models;
using Speckle.DesktopUI;
using Speckle.DesktopUI.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;



namespace ConnectorETABS
{
    public partial class ConnectorBindingsETABS : ConnectorBindings
    {
#if ETABS19
        public string ETABSAppName = Applications.ETABS19;
#else
        public string ETABSAppName = Applications.ETABS18;
#endif
        //public static UIApplication EtabsApp { get; set; }

        public static UIDocument CurrentDoc { get; set; } = new UIDocument();

        public Timer SelectionTimer;

        private static string _documentFilePath;

        public List<Exception> Exceptions { get; set; } = new List<Exception>();

        private static void GetDocumentFilePath()
        {
            _documentFilePath = CurrentDoc.Document.GetModelFilepath();
        }

        public ConnectorBindingsETABS(UIDocument doc)
        {
            CurrentDoc = doc;
            GetDocumentFilePath();
            SelectionTimer = new Timer(2000) { AutoReset = true, Enabled = true };
            SelectionTimer.Elapsed += SelectionTimer_Elapsed;
            SelectionTimer.Start();
        }

        private void SelectionTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (CurrentDoc == null)
            {
                return;
            }

            var selection = GetSelectedObjects();

            NotifyUi(new UpdateSelectionCountEvent() { SelectionCount = selection.Count });
            NotifyUi(new UpdateSelectionEvent() { ObjectIds = selection });
        }

        public override string GetActiveViewName()
        {
            throw new NotImplementedException();
        }

        public override string GetDocumentId() => GetDocHash(CurrentDoc);

        private string GetDocHash(UIDocument CurrentDoc)
        {

            GetDocumentFilePath();
            return Utilities.hashString(_documentFilePath, Utilities.HashingFuctions.MD5);
        }

        public override string GetDocumentLocation()
        {
            GetDocumentFilePath();
            return _documentFilePath;
        }

        public override string GetFileName()
        {
            GetDocumentFilePath();
            return Path.GetFileNameWithoutExtension(_documentFilePath);
        }

        public override string GetHostAppName()
        {
            return ETABSAppName;
        }

        public override List<string> GetObjectsInView()
        {
            throw new NotImplementedException();
        }

        public override void SelectClientObjects(string args)
        {
            throw new NotImplementedException();
        }

    }
}

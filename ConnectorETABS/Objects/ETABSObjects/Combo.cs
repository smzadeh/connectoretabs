﻿using Speckle.Core.Models;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class Combo : Base
    {
        public Combo()
        {
        }

        public Combo(string name, int numberItems, List<int> cNameType,
            List<string> cName, List<int> modeNumber, List<double> sf)
        {
            this.name = name;
            this.numberItems = numberItems;
            this.cNameType = cNameType;
            this.cName = cName;
            this.modeNumber = modeNumber;
            this.sf = sf;
        }

        public string name { get; set; }
        public int numberItems { get; set; }
        public List<int> cNameType { get; set; } = new List<int>();
        public List<string> cName { get; set; } = new List<string>();
        public List<int> modeNumber { get; set; } = new List<int>();
        public List<double> sf { get; set; } = new List<double>();

    }
}

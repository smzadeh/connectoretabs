﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class LoadDisp : Base
    {
        public int NumberItems { get; set; }
        public List<LoadPattern> loadPattern { get; set; } = new List<LoadPattern>();
        public List<double> u1 { get; set; } = new List<double>();
        public List<double> u2 { get; set; } = new List<double>();
        public List<double> u3 { get; set; } = new List<double>();
        public List<double> r1 { get; set; } = new List<double>();
        public List<double> r2 { get; set; } = new List<double>();
        public List<double> r3 { get; set; } = new List<double>();
    }
    public class LoadForce : Base
    {
        public int NumberItems { get; set; }
        public List<LoadPattern> loadPattern { get; set; } = new List<LoadPattern>();
        //public List<int> lcStep { get; set; }
        //public List<string> cSys { get; set; }

        public List<double> f1 { get; set; } = new List<double>();
        public List<double> f2 { get; set; } = new List<double>();
        public List<double> f3 { get; set; } = new List<double>();
        public List<double> m1 { get; set; } = new List<double>();
        public List<double> m2 { get; set; } = new List<double>();
        public List<double> m3 { get; set; } = new List<double>();
    }
    public class PanelZone : Base
    {
        public int propType { get; set; }
        public double thickness { get; set; }
        public double k1 { get; set; }
        public double k2 { get; set; }
        public string linkProp { get; set; }
        public int connectivity { get; set; }
        public int localAxisFrom { get; set; }
        public double localAxisAngle { get; set; }
    }
    public class Restraint
    {
        public List<bool> value { get; set; } = new List<bool>();
    }
    public class SpecialPoint : Base
    {
        public bool specialPoint { get; set; }
    }
    public class Spring : Base
    {
        public List<double> k { get; set; } = new List<double>();
    }
    public class SpringAssignment : Base
    {
        public string springProp { get; set; }
    }
    public class SpringCoupled : Base
    {
        public List<double> k { get; set; } = new List<double>();
    }
    public class Point : Base
    {
        public Point()
        {
        }

        public Point(double x, double y, double z, string name,
            int? diaphragmOption = null, string diaphragmName = null,
            List<string> groups = null, LoadDisp loadDisp = null, LoadForce loadForce = null,
            List<double> mass = null, PanelZone panelZone = null, Restraint restraint = null,
            SpecialPoint specialPoint = null, Spring spring = null,
            SpringAssignment springAssignment = null, SpringCoupled springCoupled = null)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            //this.label = label;
            this.diaphragmOption = diaphragmOption;
            this.diaphragmName = diaphragmName;
            this.groups = groups;
            this.loadDisp = loadDisp;
            this.loadForce = loadForce;
            this.mass = mass;
            this.panelZone = panelZone;
            this.restraint = restraint;
            this.specialPoint = specialPoint;
            this.spring = spring;
            this.springAssignment = springAssignment;
            this.springCoupled = springCoupled;
        }

        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
        public string name { get; set; }
        //public string label { get; set; } // we can't set the label of a point in ETABS
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? diaphragmOption { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string diaphragmName { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<string> groups { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadDisp loadDisp { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadForce loadForce { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<double> mass { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PanelZone panelZone { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Restraint restraint { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SpecialPoint specialPoint { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Spring spring { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SpringAssignment springAssignment { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SpringCoupled springCoupled { get; set; } = null;


    }
}

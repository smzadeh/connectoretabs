﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class SpandrelLabel : Base
    {
        public SpandrelLabel()
        {
        }

        public SpandrelLabel(string name, bool isMultiStory)
        {
            this.name = name;
            this.isMultiStory = isMultiStory;

        }
        public string name { get; set; }
        public bool isMultiStory { get; set; }
    }
}

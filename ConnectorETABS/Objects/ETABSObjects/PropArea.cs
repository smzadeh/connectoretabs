﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class Deck : Base
    {
        public int deckType { get; set; }
        public int shellType { get; set; }
        public PropMaterial matProp { get; set; }
        public double thickness { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class DeckFilled : Base
    {
        public double slabDepth { get; set; }
        public double ribDepth { get; set; }
        public double ribWidthTop { get; set; }
        public double ribWidthBot { get; set; }
        public double ribSpacing { get; set; }
        public double shearThickness { get; set; }
        public double unitWeight { get; set; }
        public double shearStudDia { get; set; }
        public double shearStudHt { get; set; }
        public double shearStudFu { get; set; }
    }
    public class DeckSolidSlab : Base
    {
        public double slabDepth { get; set; }
        public double shearStudDia { get; set; }
        public double shearStudHt { get; set; }
        public double shearStudFu { get; set; }
    }
    public class DeckUnfilled : Base
    {
        public double ribDepth { get; set; }
        public double ribWidthTop { get; set; }
        public double ribWidthBot { get; set; }
        public double ribSpacing { get; set; }
        public double shearThickness { get; set; }
        public double unitWeight { get; set; }

    }
    public class ShellDesign : Base
    {
        public PropMaterial matProp { get; set; }
        public int steelLayoutOption { get; set; }
        public double designCoverTopDir1 { get; set; }
        public double designCoverTopDir2 { get; set; }
        public double designCoverBotDir1 { get; set; }
        public double designCoverBotDir2 { get; set; }

    }
    public class ShellLayer_1 : Base
    {
        public int numberLayers { get; set; }
        public List<string> layerName { get; set; } = new List<string>() { };
        public List<double> dist { get; set; } = new List<double>() { };
        public List<double> thickness { get; set; } = new List<double>() { };
        public List<int> myType { get; set; } = new List<int>() { };
        public List<int> numIntegrationPts { get; set; } = new List<int>() { };
        public List<PropMaterial> matProp { get; set; } = new List<PropMaterial>() { };
        public List<double> matAng { get; set; } = new List<double>() { };
        public List<int> s11Type { get; set; } = new List<int>() { };
        public List<int> s22Type { get; set; } = new List<int>() { };
        public List<int> s12Type { get; set; } = new List<int>() { };
    }
    public class Slab : Base
    {
        public int slabType { get; set; }
        public int shellType { get; set; }
        public PropMaterial matProp { get; set; }
        public double thickness { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class SlabRibbed : Base
    {
        public double overallDepth { get; set; }
        public double slabThickness { get; set; }
        public double stemWidthTop { get; set; }
        public double stemWidthBot { get; set; }
        public double ribSpacing { get; set; }
        public int ribsParallelTo { get; set; }

    }
    public class SlabWaffle : Base
    {
        public double overallDepth { get; set; }
        public double slabThickness { get; set; }
        public double stemWidthTop { get; set; }
        public double stemWidthBot { get; set; }
        public double ribSpacingDir1 { get; set; }
        public double ribSpacingDir2 { get; set; }
    }
    public class Wall : Base
    {
        public int wallPropType { get; set; }
        public int shellType { get; set; }
        public PropMaterial matProp { get; set; }
        public double thickness { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class WallAutoSelectList : Base
    {
        public List<string> autoSelectList { get; set; } = new List<string>() { };
        public string startingProperty { get; set; } = "Median";
    }
    public class PropArea : Base
    {
        public PropArea()
        {
        }
        public PropArea(string name, Deck deck = null, DeckFilled deckFilled = null,
            DeckSolidSlab deckSolidSlab = null, DeckUnfilled deckUnfilled = null,
            Modifiers modifiers = null, ShellDesign shellDesign = null,
            ShellLayer_1 shellLayer_1 = null, SlabRibbed slabRibbed = null,
            SlabWaffle slabWaffle = null, Wall wall = null,
            WallAutoSelectList wallAutoSelectList = null)
        {
            this.name = name;
            this.deck = deck;
            this.deckFilled = deckFilled;
            this.deckSolidSlab = deckSolidSlab;
            this.deckUnfilled = deckUnfilled;
            this.modifiers = modifiers;
            this.shellDesign = shellDesign;
            this.shellLayer_1 = shellLayer_1;
            this.slabRibbed = slabRibbed;
            this.slabWaffle = slabWaffle;
            this.wall = wall;
            this.wallAutoSelectList = wallAutoSelectList;
        }

        public string name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Deck deck { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeckFilled deckFilled { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeckSolidSlab deckSolidSlab { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeckUnfilled deckUnfilled { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Modifiers modifiers { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ShellDesign shellDesign { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ShellLayer_1 shellLayer_1 { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Slab slab { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SlabRibbed slabRibbed { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SlabWaffle slabWaffle { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Wall wall { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public WallAutoSelectList wallAutoSelectList { get; set; } = null;

    }
}

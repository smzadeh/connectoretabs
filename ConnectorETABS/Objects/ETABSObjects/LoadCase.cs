﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class HyperStatic : Base
    {
        public string BaseCase { get; set; }
    }
    public class DirectHistoryNonlinear : Base
    {
        // does not have set methods
    }
    public class DirectHistoryLinear : Base
    {
        // does not have set methods
    }
    public class ResponseSpectrum : Base
    {
        public double Eccentricity { get; set; }
        //loads
        public int NumberLoads { get; set; }
        public List<string> LoadName { get; set; } = new List<string>();
        public List<string> Func { get; set; } = new List<string>();
        public List<double> SF { get; set; } = new List<double>();
        public List<string> CSys { get; set; } = new List<string>();
        public List<double> Ang { get; set; } = new List<double>();
        //modalcase
        public string ModalCase { get; set; }


    }
    public class ModalHistoryNonlinear : Base
    {
        // does not have set methods
    }

    public class ModalRitz : Base
    {
        // does not have set methods
    }
    public class ModalHistoryLinear : Base
    {
        // loads
        public int NumberLoads { get; set; }
        public List<string> LoadType { get; set; } = new List<string>();
        public List<string> LoadName { get; set; } = new List<string>();
        public List<string> Func { get; set; } = new List<string>();
        public List<double> SF { get; set; } = new List<double>();
        public List<double> Tf { get; set; } = new List<double>();
        public List<double> At { get; set; } = new List<double>();
        public List<string> CSys { get; set; } = new List<string>();
        public List<double> Ang { get; set; } = new List<double>();

    }

    public class LoadApplication : Base
    {
        public int LoadControl { get; set; }
        public int DispType { get; set; }
        public double Displ { get; set; }
        public int Monitor { get; set; }
        public int DOF { get; set; }
        public string PointName { get; set; }
        public string GDispl { get; set; }

    }
    public class StaticNonlinear : Base
    {
        public int NLGeomType { get; set; }
        public int HingeUnloading { get; set; }
        public string InitialCase { get; set; }
        public LoadApplication LoadApplication { get; set; }
        //loads
        public int NumberLoads { get; set; }
        public List<string> LoadType { get; set; } = new List<string>();
        public List<string> LoadName { get; set; } = new List<string>();
        public List<double> SF { get; set; } = new List<double>();
        //massSource
        public string MassSource { get; set; }
        //modalCase
        public string ModalCase { get; set; }
        //ResultsSaved
        public bool SaveMultipleSteps { get; set; }

        public int MinSavedStates { get; set; } = 10;
        public int MaxSavedStates { get; set; } = 100;
        public bool PositiveOnly { get; set; } = true;
        //SolControlParameters
        public int MaxTotalSteps { get; set; }
        public int MaxFailedSubSteps { get; set; }
        public int MaxIterCS { get; set; }
        public int MaxIterNR { get; set; }
        public double TolConvD { get; set; }
        public bool UseEventStepping { get; set; }
        public double TolEventD { get; set; }
        public int MaxLineSearchPerIter { get; set; }
        public double TolLineSearch { get; set; }
        public double LineSearchStepFact { get; set; }
        //TargetForceParameters
        public double TolConvF { get; set; }
        public int MaxIter { get; set; }
        public double AccelFact { get; set; }
        public bool NoStop { get; set; }

    }
    public class StaticLinear : Base
    {
        public string InitialCase { get; set; }
        public int NumberLoads { get; set; }
        public List<string> LoadType { get; set; } = new List<string>();
        public List<string> LoadName { get; set; } = new List<string>();
        public List<double> SF { get; set; } = new List<double>();
    }

    public class LoadCase : Base
    {


        public LoadCase()
        {
        }

        public LoadCase(string name, int designTypeOption, int designType = 1,
            StaticLinear StaticLinear = null, StaticNonlinear StaticNonlinear = null,
            ModalHistoryLinear ModalHistoryLinear = null,
            ResponseSpectrum ResponseSpectrum = null,
            HyperStatic HyperStatic = null)
        {
            this.name = name;
            this.designType = designType;
            this.designTypeOption = designTypeOption;
            this.StaticLinear = StaticLinear;
            this.StaticNonlinear = StaticNonlinear;
        }
        public string name { get; set; }
        public int caseType { get; set; }
        public int caseSubType { get; set; }

        // this is load pattern type with default of dead = 1
        public int designType { get; set; }

        // this is  0 for "program determined" or 1 for "user specified"
        public int designTypeOption { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public StaticLinear StaticLinear { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public StaticNonlinear StaticNonlinear { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ModalHistoryLinear ModalHistoryLinear { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ResponseSpectrum ResponseSpectrum { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public HyperStatic HyperStatic { get; set; } = null;

    }
}

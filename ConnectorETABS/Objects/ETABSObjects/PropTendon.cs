﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class PropTendon : Base
    {
        public PropTendon()
        {
        }

        public PropTendon(string name, string matProp, int modelingOption,
            double area, int color = 0, string notes = "", string GUID = "")
        {
            this.name = name;
            this.matProp = matProp;
            this.modelingOption = modelingOption;
            this.area = area;
            this.color = color;
            this.notes = notes;
            this.GUID = GUID;
        }
        public string name { get; set; }
        public string matProp { get; set; }
        public int modelingOption { get; set; }
        public double area { get; set; }
        public int color { get; set; } = 0;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
}

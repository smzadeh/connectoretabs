﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class PropAreaSpring : Base
    {
        public PropAreaSpring()
        {
        }

        public PropAreaSpring(string name, double u1, double u2, double u3,
            int nonlinearOption3, int springOption, string soilProfile = "",
            double endLengthRatio = 0, double period = 0, int color = 0,
            string notes = "", string iGUID = "")
        {
            this.name = name;
            this.u1 = u1;
            this.u2 = u2;
            this.u3 = u3;
            this.nonlinearOption3 = nonlinearOption3;
            this.springOption = springOption;
            this.soilProfile = soilProfile;
            this.endLengthRatio = endLengthRatio;
            this.period = period;
            this.color = color;
            this.notes = notes;
            this.iGUID = iGUID;
        }
        public string name { get; set; }
        public double u1 { get; set; }
        public double u2 { get; set; }
        public double u3 { get; set; }
        public int nonlinearOption3 { get; set; }
        public int springOption { get; set; } = 1;
        public string soilProfile { get; set; } = "";
        public double endLengthRatio { get; set; } = 0;
        public double period { get; set; } = 0;
        public int color { get; set; } = 0;
        public string notes { get; set; } = "";
        public string iGUID { get; set; } = "";

    }
}

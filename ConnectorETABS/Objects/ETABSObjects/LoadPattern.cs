﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class LoadPattern : Base
    {
        public LoadPattern()
        {
        }

        public LoadPattern(string name, int loadPatterntype, double selfWTMultiplier = 0)
        {
            this.name = name;
            this.loadPatterntype = loadPatterntype;
            this.selfWTMultiplier = selfWTMultiplier;
        }

        public string name { get; set; }
        public int loadPatterntype { get; set; }
        public double selfWTMultiplier { get; set; }
    }

}

﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class ColumnSpliceOverwrite : Base
    {
        public int spliceOption { get; set; }
        public double height { get; set; }
    }
    public class Curved_2 : Base
    {
        public int curveType { get; set; }
        public double tension { get; set; }
        public int numPnts { get; set; }
        public List<double> gx { get; set; } = new List<double>();
        public List<double> gy { get; set; } = new List<double>();
        public List<double> gz { get; set; } = new List<double>();

    }
    public class DesignOrientation : Base
    {
        /// <summary>
        /// this is an enum value
        /// </summary>
        public int designOrientation { get; set; }
    }
    public class DesignProcedure : Base
    {
        public int designProcedure { get; set; }
    }
    public class EndLengthOffset : Base
    {
        public bool autoOffset { get; set; }

        public double length1 { get; set; }
        public double length2 { get; set; }
        public double rz { get; set; }

    }
    public class GroupAssign : Base
    {
        public List<string> groupAssign { get; set; } = new List<string>();
    }
    public class InsertionPoint : Base
    {
        public int cardinalPoint { get; set; }

        public bool mirror2 { get; set; }
        public bool stiffTransform { get; set; }
        public List<double> offset1 { get; set; } = new List<double>();
        public List<double> offset2 { get; set; } = new List<double>();
        public string cSys { get; set; }

    }
    public class LateralBracing : Base
    {
        public int numberItems { get; set; }
        public List<string> frameName { get; set; } = new List<string>();
        public List<int> myType { get; set; } = new List<int>();
        public List<int> Loc { get; set; } = new List<int>();
        public List<double> rD1 { get; set; } = new List<double>();
        public List<double> rD2 { get; set; } = new List<double>();
        public List<double> dist1 { get; set; } = new List<double>();
        public List<double> dist2 { get; set; } = new List<double>();
    }
    public class LoadDistributed : Base
    {
        public int numberItems { get; set; }

        public List<string> frameName { get; set; } = new List<string>();
        public List<LoadPattern> loadPat { get; set; } = new List<LoadPattern>();
        public List<int> myType { get; set; } = new List<int>();
        public List<string> cSys { get; set; } = new List<string>();
        public List<int> dir { get; set; } = new List<int>();
        public List<double> rD1 { get; set; } = new List<double>();
        public List<double> rD2 { get; set; } = new List<double>();
        public List<double> dist1 { get; set; } = new List<double>();
        public List<double> dist2 { get; set; } = new List<double>();
        public List<double> val1 { get; set; } = new List<double>();
        public List<double> val2 { get; set; } = new List<double>();

    }
    public class LoadPoint : Base
    {
        public int numberItems { get; set; }
        public List<string> frameName { get; set; } = new List<string>();
        public List<LoadPattern> loadPat { get; set; } = new List<LoadPattern>();
        public List<int> myType { get; set; } = new List<int>();
        public List<string> cSys { get; set; } = new List<string>();
        public List<int> dir { get; set; } = new List<int>();
        public List<double> relDist { get; set; } = new List<double>();
        public List<double> dist { get; set; } = new List<double>();
        public List<double> val { get; set; } = new List<double>();
    }
    public class LoadTemperature : Base
    {
        public int numberItems { get; set; }
        public List<string> name { get; set; } = new List<string>();
        public List<LoadPattern> loadPat { get; set; } = new List<LoadPattern>();
        public List<int> myType { get; set; } = new List<int>();
        public List<double> val { get; set; } = new List<double>();
        public List<string> patternName { get; set; } = new List<string>();
    }
    public class LocalAxes : Base
    {
        public double ang { get; set; }
        public bool advanced { get; set; }

    }
    public class MaterialOverwrite : Base
    {
        public string propName { get; set; }
    }
    public class Modifiers : Base
    {
        public List<double> value { get; set; } = new List<double>();
    }
    public class OutputStations : Base
    {
        public int myType { get; set; }
        public double maxSegSize { get; set; }
        public int minSections { get; set; }
        public bool noOutPutAndDesignAtElementEnds { get; set; }
        public bool noOutPutAndDesignAtPointLoads { get; set; }
    }
    public class Pier : Base
    {
        public string pierName { get; set; }
    }
    public class Releases : Base
    {
        public List<bool> ii { get; set; } = new List<bool>();
        public List<bool> jj { get; set; } = new List<bool>();
        public List<double> startValue { get; set; } = new List<double>();
        public List<double> endValue { get; set; } = new List<double>();

    }
    public class Spandrel : Base
    {
        public string spandrelName { get; set; }
    }
    public class TCLimits : Base
    {
        public bool limitCompressionExists { get; set; }
        public double limitCompression { get; set; }
        public bool limitTensionExists { get; set; }
        public double limitTension { get; set; }
    }
    public class Frame : Base
    {
        public Frame()
        {
        }

        public Frame(string name, Point point1, Point point2,
            PropFrame section = null,
            //string label = null, 
            ColumnSpliceOverwrite columnSpliceOverwrite = null,
            //Curved_2 curved_2 = null,
            //DesignOrientation designOrientation = null,
            DesignProcedure designProcedure = null,
            EndLengthOffset endLengthOffset = null,
            GroupAssign groupAssign = null, InsertionPoint insertionPoint = null,
            LateralBracing lateralBracing = null,
            LoadDistributed loadDistributed = null,
            LoadPoint loadPoint = null, LoadTemperature loadTemperature = null,
            LocalAxes localAxes = null, double? mass = null,
            MaterialOverwrite materialOverwrite = null,
            Modifiers modifiers = null, OutputStations outputStations = null,
            Pier pier = null, Releases releases = null,
            SpringAssignment springAssignment = null, TCLimits TCLimits = null)
        {
            this.point1 = point1;
            this.point2 = point2;
            this.section = section;
            this.columnSpliceOverwrite = columnSpliceOverwrite;
            //this.curved_2 = curved_2;
            //this.designOrientation = designOrientation;
            this.designProcedure = designProcedure;
            this.endLengthOffset = endLengthOffset;
            this.groupAssign = groupAssign;
            this.insertionPoint = insertionPoint;
            this.lateralBracing = lateralBracing;
            this.loadDistributed = loadDistributed;
            this.loadPoint = loadPoint;
            this.loadTemperature = loadTemperature;
            this.localAxes = localAxes;
            this.mass = mass;
            this.materialOverwrite = materialOverwrite;
            this.modifiers = modifiers;
            this.outputStations = outputStations;
            this.pier = pier;
            this.releases = releases;
            this.springAssignment = springAssignment;
            this.TCLimits = TCLimits;
        }
        public string name { get; set; }
        public Point point1 { get; set; }
        public Point point2 { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PropFrame section { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ColumnSpliceOverwrite columnSpliceOverwrite { get; set; } = null;
        //public Curved_2 curved_2 { get; set; }
        //public DesignOrientation designOrientation { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DesignProcedure designProcedure { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EndLengthOffset endLengthOffset { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public GroupAssign groupAssign { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public InsertionPoint insertionPoint { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LateralBracing lateralBracing { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadDistributed loadDistributed { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadPoint loadPoint { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadTemperature loadTemperature { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocalAxes localAxes { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? mass { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MaterialOverwrite materialOverwrite { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Modifiers modifiers { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OutputStations outputStations { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Pier pier { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Releases releases { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Spandrel spandrel { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SpringAssignment springAssignment { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TCLimits TCLimits { get; set; } = null;
    }
}

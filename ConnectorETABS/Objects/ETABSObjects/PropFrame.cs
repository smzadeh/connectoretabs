﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class SteelAngle : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double tw { get; set; }
        public double r { get; set; }
        public bool mirrorAbout2 { get; set; }
        public bool mirrorAbout3 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class AutoSelectSteel : Base
    {
        public int numberItems { get; set; }
        public List<string> sectName { get; set; }
        public string autoStartSection { get; set; }
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class Channel : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double tw { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class Circle : Base
    {
        public RebarBeam rebarBeam { get; set; } = null;
        public RebarColumn rebarColumn { get; set; } = null;
        public int typeRebar { get; set; }
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class ConcreteL : Base
    {
        public RebarBeam rebarBeam { get; set; } = null;
        public int typeRebar { get; set; }
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double twC { get; set; }
        public double twT { get; set; }
        public bool mirrorAbout2 { get; set; }
        public bool mirrorAbout3 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class ConcreteTee : Base
    {
        public RebarBeam rebarBeam { get; set; } = null;
        public int typeRebar { get; set; }
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double twF { get; set; }
        public double twT { get; set; }
        public bool mirrorAbout3 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class CoverPlatedI : Base
    {
        public string sectName { get; set; }
        public double fyTopFlange { get; set; }
        public double fyWeb { get; set; }
        public double fyBotFlange { get; set; }
        public double tc { get; set; }
        public double bc { get; set; }
        public PropMaterial matPropTop { get; set; }
        public double tcb { get; set; }
        public double bcb { get; set; }
        public PropMaterial matPropBot { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class DblAngle : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double tw { get; set; }
        public double dis { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class DblChannel : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double tw { get; set; }
        public double dis { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class ISection : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double tw { get; set; }
        public double t2b { get; set; }
        public double tfb { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class GeneralSectionProp : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double Area { get; set; }
        public double as2 { get; set; }
        public double as3 { get; set; }
        public double torsion { get; set; }
        public double i22 { get; set; }
        public double i33 { get; set; }
        public double s22 { get; set; }
        public double s33 { get; set; }
        public double z22 { get; set; }
        public double z33 { get; set; }
        public double r22 { get; set; }
        public double r33 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class NonPrismatic : Base
    {
        public int numberItems { get; set; }
        public List<string> startSec { get; set; } = new List<string>();
        public List<string> endSec { get; set; } = new List<string>();
        public List<double> myLength { get; set; } = new List<double>();
        public List<int> myType { get; set; } = new List<int>();
        public List<int> eI33 { get; set; } = new List<int>();
        public List<int> eI22 { get; set; } = new List<int>();
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class Pipe : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double tw { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; }
        public string GUID { get; set; } = "";
    }
    public class Plate : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class RebarBeam : Base
    {
        public string matPropLong { get; set; }
        public string matPropConfine { get; set; }
        public double coverTop { get; set; }
        public double coverBot { get; set; }
        public double topLeftArea { get; set; }
        public double topRightArea { get; set; }
        public double botLeftArea { get; set; }
        public double botRightArea { get; set; }

    }
    public class RebarColumn : Base
    {
        public string matPropLong { get; set; }
        public string matPropConfine { get; set; }
        public int pattern { get; set; }
        public int confineType { get; set; }
        public double cover { get; set; }
        public int numberCBars { get; set; }
        public int numberR3Bars { get; set; }
        public int numberR2Bars { get; set; }
        public string rebarSize { get; set; }
        public string tieSize { get; set; }
        public double tieSpacingLongit { get; set; }
        public int number2DirTieBars { get; set; }
        public int number3DirTieBars { get; set; }
        public bool toBeDesigned { get; set; }

    }
    public class Rectangle : Base
    {
        public RebarBeam rebarBeam { get; set; } = null;
        public RebarColumn rebarColumn { get; set; } = null;
        public int typeRebar { get; set; }
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class Rod : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class SDSection : Base
    {
        public PropMaterial matProp { get; set; }

        public int numberItems { get; set; }
        public List<string> shapeName { get; set; } = new List<string>();
        public List<int> myType { get; set; } = new List<int>();
        public int designType { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class SteelTee : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double tw { get; set; }
        public double r { get; set; }
        public bool mirrorAbout3 { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class Tube : Base
    {
        public string fileName { get; set; }
        public PropMaterial matProp { get; set; }
        public double t3 { get; set; }
        public double t2 { get; set; }
        public double tf { get; set; }
        public double tw { get; set; }
        public int color { get; set; } = -1;
        public string notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }

    public class PropFrame : Base
    {
        public PropFrame()
        {
        }
        public PropFrame(string name, PropMaterial matProp, int framePropType,
            Modifiers modifiers = null,
            SteelAngle steelAngle = null, AutoSelectSteel autoSelectSteel = null,
            Channel channel = null, Circle circle = null, ConcreteL concreteL = null,
            ConcreteTee concreteTee = null, CoverPlatedI coverPlatedI = null,
            DblAngle dblAngle = null, DblChannel dblChannel = null,
            ISection iSection = null, GeneralSectionProp generalSectionProp = null,
            NonPrismatic nonPrismatic = null, Pipe pipe = null, Plate plate = null,
            Rectangle rectangle = null, Rod rod = null, SDSection sDSection = null,
            SteelTee steelTee = null, Tube tube = null)
        {
            this.name = name;
            this.matProp = matProp;
            this.framePropType = framePropType;
            this.modifiers = modifiers;
            this.steelAngle = steelAngle;
            this.autoSelectSteel = autoSelectSteel;
            this.channel = channel;
            this.circle = circle;
            this.concreteL = concreteL;
            this.concreteTee = concreteTee;
            this.coverPlatedI = coverPlatedI;
            this.dblAngle = dblAngle;
            this.dblChannel = dblChannel;
            this.iSection = iSection;
            this.generalSectionProp = generalSectionProp;
            this.nonPrismatic = nonPrismatic;
            this.pipe = pipe;
            this.plate = plate;
            this.rectangle = rectangle;
            this.rod = rod;
            this.sDSection = sDSection;
            this.steelTee = steelTee;
            this.tube = tube;
        }
        public string name { get; set; }
        public PropMaterial matProp { get; set; }
        public int framePropType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Modifiers modifiers { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SteelAngle steelAngle { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AutoSelectSteel autoSelectSteel { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Channel channel { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Circle circle { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ConcreteL concreteL { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ConcreteTee concreteTee { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CoverPlatedI coverPlatedI { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DblAngle dblAngle { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DblChannel dblChannel { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ISection iSection { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public GeneralSectionProp generalSectionProp { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public NonPrismatic nonPrismatic { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Pipe pipe { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Plate plate { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Rectangle rectangle { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Rod rod { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SDSection sDSection { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SteelTee steelTee { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Tube tube { get; set; } = null;

    }
}

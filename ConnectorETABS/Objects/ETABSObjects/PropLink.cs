﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class AcceptanceCriteria : Base
    {
        public int AcceptanceType { get; set; }
        public bool Symmetric { get; set; }
        public List<bool> Active { get; set; } = new List<bool>();
        public List<double> IOPos { get; set; } = new List<double>();
        public List<double> LSPos { get; set; } = new List<double>();
        public List<double> CPPos { get; set; } = new List<double>();
        public List<double> IONeg { get; set; } = new List<double>();
        public List<double> LSNeg { get; set; } = new List<double>();
        public List<double> CPNeg { get; set; } = new List<double>();

    }
    public class Damper : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> ke { get; set; } = new List<double>();
        public List<double> ce { get; set; } = new List<double>();
        public List<double> k { get; set; } = new List<double>();
        public List<double> c { get; set; } = new List<double>();
        public List<double> CExp { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class DamperBilinear : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> ke { get; set; } = new List<double>();
        public List<double> ce { get; set; } = new List<double>();
        public List<double> k { get; set; } = new List<double>();
        public List<double> c { get; set; } = new List<double>();
        public List<double> cY { get; set; } = new List<double>();
        public List<double> ForceLimit { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class DamperFrictionSpring : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> ke { get; set; } = new List<double>();
        public List<double> ce { get; set; } = new List<double>();
        public List<double> k { get; set; } = new List<double>();
        public List<double> K1 { get; set; } = new List<double>();
        public List<double> K2 { get; set; } = new List<double>();
        public List<double> u0 { get; set; } = new List<double>();
        public List<double> us { get; set; } = new List<double>();
        public List<int> direction { get; set; } = new List<int>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class FrictionIsolator : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public List<double> K { get; set; } = new List<double>();
        public List<double> Slow { get; set; } = new List<double>();
        public List<double> Fast { get; set; } = new List<double>();
        public List<double> Rate { get; set; } = new List<double>();
        public List<double> Radius { get; set; } = new List<double>();
        public double Damping { get; set; }
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class Gap : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public List<double> K { get; set; } = new List<double>();
        public List<double> Dis { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class Hook : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public List<double> K { get; set; } = new List<double>();
        public List<double> Dis { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class Linear : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public bool KeCoupled { get; set; } = false;
        public bool CeCoupled { get; set; } = false;
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class MultiLinearElastic : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class MultiLinearPlastic : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class MultiLinearPoints : Base
    {
        public int DOF { get; set; }
        public int NumberPoints { get; set; }
        public List<double> F { get; set; } = new List<double>();
        public List<double> D { get; set; } = new List<double>();
        public int MyType { get; set; } = 1;
        public double A1 { get; set; } = 0;
        public double A2 { get; set; } = 0;
        public double B1 { get; set; } = 0;
        public double B2 { get; set; } = 0;
        public double Eta { get; set; } = 0;

    }
    public class PDelta : Base
    {
        public List<double> Value { get; set; } = new List<double>();
    }
    public class PlasticWen : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public List<double> K { get; set; } = new List<double>();
        public List<double> Yield { get; set; } = new List<double>();
        public List<double> Ratio { get; set; } = new List<double>();
        public List<double> Exp { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class RubberIsolator : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public List<double> K { get; set; } = new List<double>();
        public List<double> Yield { get; set; } = new List<double>();
        public List<double> Ratio { get; set; } = new List<double>();
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";
    }
    public class SpringData : Base
    {
        public double DefinedForThisLength { get; set; }
        public double DefinedForThisArea { get; set; }

    }
    public class TCFrictionIsolator : Base
    {
        public List<bool> DOF { get; set; } = new List<bool>();
        public List<bool> Fixed { get; set; } = new List<bool>();
        public List<bool> Nonlinear { get; set; } = new List<bool>();
        public List<double> Ke { get; set; } = new List<double>();
        public List<double> Ce { get; set; } = new List<double>();
        public List<double> K { get; set; } = new List<double>();
        public List<double> Slow { get; set; } = new List<double>();
        public List<double> Fast { get; set; } = new List<double>();
        public List<double> Rate { get; set; } = new List<double>();
        public List<double> Radius { get; set; } = new List<double>();
        public List<double> SlowT { get; set; } = new List<double>();
        public List<double> FastT { get; set; } = new List<double>();
        public List<double> RateT { get; set; } = new List<double>();
        public double Kt { get; set; }
        public double Dis { get; set; }
        public double Dist { get; set; }
        public double Damping { get; set; }
        public double DJ2 { get; set; }
        public double DJ3 { get; set; }
        public string Notes { get; set; } = "";
        public string GUID { get; set; } = "";

    }
    public class PropLinkWeightAndMass : Base
    {
        public double W { get; set; }
        public double M { get; set; }
        public double R1 { get; set; }
        public double R2 { get; set; }
        public double R3 { get; set; }

    }
    public class PropLink : Base
    {
        public PropLink()
        {
        }

        public PropLink(string name,
            AcceptanceCriteria AcceptanceCriteria = null,
            Damper Damper = null,
            DamperBilinear DamperBilinear = null,
            DamperFrictionSpring DamperFrictionSpring = null,
            FrictionIsolator FrictionIsolator = null,
            Gap Gap = null, Hook Hook = null, Linear Linear = null,
            MultiLinearElastic MultiLinearElastic = null,
            PDelta PDelta = null, PlasticWen PlasticWen = null,
            RubberIsolator RubberIsolator = null,
            SpringData SpringData = null,
            TCFrictionIsolator TCFrictionIsolator = null,
            PropLinkWeightAndMass PropLinkWeightAndMass = null)
        {
            this.name = name;
            this.AcceptanceCriteria = AcceptanceCriteria;
            this.DamperBilinear = DamperBilinear;
            this.Damper = Damper;
            this.DamperFrictionSpring = DamperFrictionSpring;
            this.FrictionIsolator = FrictionIsolator;
            this.Gap = Gap;
            this.Hook = Hook;
            this.Linear = Linear;
            this.MultiLinearElastic = MultiLinearElastic;
            this.PDelta = PDelta;
            this.PlasticWen = PlasticWen;
            this.RubberIsolator = RubberIsolator;
            this.SpringData = SpringData;
            this.TCFrictionIsolator = TCFrictionIsolator;
            this.PropLinkWeightAndMass = PropLinkWeightAndMass;

        }
        public string name { get; set; }
        public AcceptanceCriteria AcceptanceCriteria { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Damper Damper { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DamperBilinear DamperBilinear { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DamperFrictionSpring DamperFrictionSpring { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FrictionIsolator FrictionIsolator { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Gap Gap { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Hook Hook { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Linear Linear { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MultiLinearElastic MultiLinearElastic { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MultiLinearPlastic MultiLinearPlastic { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MultiLinearPoints MultiLinearPoints { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PDelta PDelta { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PlasticWen PlasticWen { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RubberIsolator RubberIsolator { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SpringData SpringData { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TCFrictionIsolator TCFrictionIsolator { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PropLinkWeightAndMass PropLinkWeightAndMass { get; set; } = null;
    }
}

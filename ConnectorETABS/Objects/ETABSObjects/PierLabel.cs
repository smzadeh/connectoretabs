﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class PierLabel : Base
    {
        public PierLabel()
        {
        }

        public PierLabel(string name)
        {
            this.name = name;
        }
        public string name { get; set; }
    }
}

﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class LocalAxesAdvanced : Base
    {
        public bool active { get; set; }
        public int axVectOpt { get; set; }
        public string axCSys { get; set; }
        public List<int> axDir { get; set; } = new List<int>();
        public List<string> axPt { get; set; } = new List<string>();
        public List<double> axVect { get; set; } = new List<double>();
        public int plane2 { get; set; }
        public int pLVectOpt { get; set; }
        public string pLCSys { get; set; }
        public List<int> pLDir { get; set; } = new List<int>();
        public List<string> pLPt { get; set; } = new List<string>();
        public List<double> pLVect { get; set; } = new List<double>();
    }
    public class Link : Base
    {
        public Link()
        {
        }

        public Link(Point point1, Point point2, string name,
            PropLink section = null, GroupAssign groupAssign = null,
            LocalAxes localAxes = null, LocalAxesAdvanced localAxesAdvanced = null)
        {
            this.point1 = point1;
            this.point2 = point2;
            this.name = name;
            this.section = section;
            this.groupAssign = groupAssign;
            this.localAxes = localAxes;
            this.localAxesAdvanced = localAxesAdvanced;
        }

        public Point point1 { get; set; }
        public Point point2 { get; set; }
        public string name { get; set; }
        public PropLink section { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public GroupAssign groupAssign { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocalAxes localAxes { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocalAxesAdvanced localAxesAdvanced { get; set; } = null;
    }
}

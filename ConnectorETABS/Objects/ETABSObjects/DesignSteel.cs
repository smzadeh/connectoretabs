﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class OverWrite : Base
    {
        public string frameName { get; set; }
        public List<int> items { get; set; }
        public List<string> textValues { get; set; } = new List<string>();
        public List<double> values { get; set; } = new List<double>();
    }
    public class DesignSteel : Base
    {
        public DesignSteel()
        {
        }

        public DesignSteel(string codeName, List<int> preferenceItem = null,
            List<string> preferenceTextValue = null, List<double> preferenceValue = null,
            List<OverWrite> overWrites = null)
        {
            this.codeName = codeName;
            this.preferenceItem = preferenceItem;
            this.preferenceTextValue = preferenceTextValue;
            this.preferenceValue = preferenceValue;
            this.overWrites = overWrites;
        }
        public string codeName { get; set; }
        public List<int> preferenceItem { get; set; } = new List<int>();
        public List<string> preferenceTextValue { get; set; } = new List<string>();
        public List<double> preferenceValue { get; set; } = new List<double>();
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<OverWrite> overWrites { get; set; } = null;
    }
}

﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class DesignConcrete : Base
    {
        public DesignConcrete()
        {
        }

        public DesignConcrete(string codeName, List<int> preferenceItem = null,
            List<string> preferenceTextValue = null, List<double> preferenceValue = null,
            List<OverWrite> overWrites = null)
        {
            this.codeName = codeName;
            this.preferenceItem = preferenceItem;
            this.preferenceTextValue = preferenceTextValue;
            this.preferenceValue = preferenceValue;
            this.overWrites = overWrites;
        }
        public string codeName { get; set; }
        public List<int> preferenceItem { get; set; }
        public List<string> preferenceTextValue { get; set; }
        public List<double> preferenceValue { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<OverWrite> overWrites { get; set; } = null;
    }
}

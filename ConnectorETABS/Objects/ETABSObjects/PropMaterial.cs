﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{

    public class Damping : Base
    {
        public double modalRatio { get; set; }
        public double viscousMassCoeff { get; set; }
        public double viscousStiffCoeff { get; set; }
        public double hystereticMassCoeff { get; set; }
        public double hystereticStiffCoeff { get; set; }
        public double Temp { get; set; } = 0;
    }
    public class MassSource_1 : Base
    {
        public bool IncludeElements { get; set; }
        public bool IncludeAddedMass { get; set; }
        public bool IncludeLoads { get; set; }
        public int numberLoads { get; set; }
        public List<string> loadPat { get; set; } = new List<string>();
        public List<double> sF { get; set; } = new List<double>();
    }
    public class MPAnisotropic : Base
    {
        public List<double> e { get; set; } = new List<double>();
        public List<double> u { get; set; } = new List<double>();
        public List<double> a { get; set; } = new List<double>();
        public List<double> g { get; set; } = new List<double>();
        public double temp { get; set; } = 0;

    }
    public class MPIsotropic : Base
    {
        public double e { get; set; }
        public double u { get; set; }
        public double a { get; set; }
        public double g { get; set; }
        public double temp { get; set; } = 0;

    }
    public class MPOrthotropic : Base
    {
        public List<double> e { get; set; } = new List<double>();
        public List<double> u { get; set; } = new List<double>();
        public List<double> a { get; set; } = new List<double>();
        public List<double> g { get; set; } = new List<double>();
        public double temp { get; set; } = 0;
    }
    public class MPUniaxial : Base
    {
        public double e { get; set; }

        public double a { get; set; }
        public double temp { get; set; } = 0;

    }

    public class OConcrete_1 : Base
    {
        public double fc { get; set; }
        public bool isLightweight { get; set; }
        public double fcsFactor { get; set; }
        public int sSType { get; set; }
        public int sSHysType { get; set; }
        public double strainAtFc { get; set; }
        public double strainUltimate { get; set; }
        public double finalSlope { get; set; }
        public double frictionAngle { get; set; }
        public double dilatationalAngle { get; set; }
        public double temp { get; set; } = 0;

    }
    public class ONoDesign : Base
    {
        public double frictionAngle { get; set; }
        public double dilatationalAngle { get; set; }
        public double temp { get; set; } = 0;

    }
    public class ORebar_1 : Base
    {
        public double fy { get; set; }

        public double fu { get; set; }
        public double eFy { get; set; }
        public double eFu { get; set; }
        public int sSType { get; set; }
        public int sSHysType { get; set; }
        public double strainAtHardening { get; set; }
        public double strainUltimate { get; set; }
        public double finalSlope { get; set; }
        public bool useCaltransSSDefaults { get; set; }
        public double temp { get; set; } = 0;
    }
    public class OSteel_1 : Base
    {
        public double fy { get; set; }
        public double fu { get; set; }
        public double eFy { get; set; }
        public double eFu { get; set; }
        public int sSType { get; set; }
        public int sSHysType { get; set; }
        public double strainAtHardening { get; set; }
        public double strainAtMaxStress { get; set; }
        public double strainAtRupture { get; set; }
        public double FinalSlope { get; set; }
        public double temp { get; set; } = 0;
    }
    public class OTendon_1 : Base
    {
        public double fy { get; set; }
        public double fu { get; set; }
        public int sSType { get; set; }
        public int sSHysType { get; set; }
        public double finalSlope { get; set; }
        public double temp { get; set; } = 0;
    }
    public class SSCurve : Base
    {
        public int numberPoints { get; set; }

        public List<int> pointID { get; set; } = new List<int>();
        public List<double> strain { get; set; } = new List<double>();
        public List<double> stress { get; set; } = new List<double>();
        public string sectName { get; set; } = "";
        public double rebarArea { get; set; } = 0;
        public double temp { get; set; } = 0;

    }
    public class Temp : Base
    {
        public int numberItems { get; set; }
        public List<double> temp { get; set; } = new List<double>();
    }
    public class WeightAndMass : Base
    {
        public double? w { get; set; } = null;
        public double? m { get; set; } = null;
        public double temp { get; set; } = 0;

    }
    public class PropMaterial : Base
    {
        public PropMaterial()
        {

        }
        public PropMaterial(string name, int matType, string region, string standard,
            string grade, string userName = "", Damping damping = null, MassSource_1 massSource_1 = null,
            MPAnisotropic mPAnisotropic = null, MPIsotropic mPIsotropic = null,
            MPOrthotropic mPOrthotropic = null, MPUniaxial mPUniaxial = null,
            OConcrete_1 oConcrete_1 = null, ONoDesign oNoDesign = null,
            ORebar_1 oRebar_1 = null, OSteel_1 oSteel_1 = null, OTendon_1 oTendon_1 = null,
            SSCurve sSCurve = null, Temp temp = null, WeightAndMass eeightAndMass = null)
        {
            this.name = name;
            this.matType = matType;
            this.damping = damping;
            this.massSource_1 = massSource_1;
            this.mPAnisotropic = mPAnisotropic;
            this.mPIsotropic = mPIsotropic;
            this.mPOrthotropic = mPOrthotropic;
            this.mPUniaxial = mPUniaxial;
            this.oConcrete_1 = oConcrete_1;
            this.oNoDesign = oNoDesign;
            this.oRebar_1 = oRebar_1;
            this.oSteel_1 = oSteel_1;
            this.oTendon_1 = oTendon_1;
            this.sSCurve = sSCurve;
            this.temp = temp;
            this.weightAndMass = weightAndMass;
        }
        public string name { get; set; }
        public int matType { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Damping damping { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MassSource_1 massSource_1 { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MPAnisotropic mPAnisotropic { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MPIsotropic mPIsotropic { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MPOrthotropic mPOrthotropic { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MPUniaxial mPUniaxial { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OConcrete_1 oConcrete_1 { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ONoDesign oNoDesign { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ORebar_1 oRebar_1 { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OSteel_1 oSteel_1 { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OTendon_1 oTendon_1 { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SSCurve sSCurve { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Temp temp { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public WeightAndMass weightAndMass { get; set; } = null;
    }
}

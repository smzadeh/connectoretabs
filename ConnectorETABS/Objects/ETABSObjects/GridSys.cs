﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class GridSys : Base
    {
        public GridSys()
        {
        }

        public GridSys(string name, double x, double y, double rz)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            this.rz = rz;
        }
        public string name { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public double rz { get; set; }
    }
}

﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class Diaphragm : Base
    {
        public Diaphragm()
        {
        }

        public Diaphragm(string name, bool semiRigid)
        {
            this.name = name;
            this.semiRigid = semiRigid;

        }
        public string name { get; set; }
        public bool semiRigid { get; set; }

    }
}

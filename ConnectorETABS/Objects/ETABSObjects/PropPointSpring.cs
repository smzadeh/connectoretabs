﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{

    public class SingleJointLinks : Base
    {
        public int numberLinks { get; set; }
        public List<string> linkNames { get; set; } = new List<string>();
        public List<int> linkAxialDirs { get; set; } = new List<int>();
        public List<double> linkAngles { get; set; } = new List<double>();
    }
    public class PropPointSpring : Base
    {
        public PropPointSpring()
        {
        }

        public PropPointSpring(string name, int springOption,
            List<double> k, string cSys = "", string soilProfile = "",
            string footing = "", double period = 0, int color = 0, string notes = "",
            string iGUID = "", SingleJointLinks singleJointLinks = null)
        {
            this.name = name;
            this.springOption = springOption;
            this.k = k;
            this.cSys = cSys;
            this.soilProfile = soilProfile;
            this.footing = footing;
            this.period = period;
            this.color = color;
            this.notes = notes;
            this.iGUID = iGUID;
            this.singleJointLinks = singleJointLinks;

        }
        public string name { get; set; }
        public int springOption { get; set; }
        public List<double> k { get; set; } = new List<double>();
        public string cSys { get; set; } = "";
        public string soilProfile { get; set; } = "";
        public string footing { get; set; } = "";
        public double period { get; set; } = 0;
        public int color { get; set; } = 0;
        public string notes { get; set; } = "";
        public string iGUID { get; set; } = "";
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SingleJointLinks singleJointLinks { get; set; } = null;

    }
}

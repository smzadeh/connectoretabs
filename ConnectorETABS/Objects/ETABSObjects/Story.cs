﻿using Speckle.Core.Models;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class Story : Base
    {
        public Story()
        {
        }

        public Story(double baseElevation, int numberStories,
            List<string> storyNames, List<double> storyHeights,
            List<bool> isMasterStory, List<string> similarToStory,
            List<bool> spliceAbove, List<double> spliceHeight,
            List<int> color)
        {
            this.baseElevation = baseElevation;
            this.numberStories = numberStories;
            this.storyNames = storyNames;
            this.storyHeights = storyHeights;
            this.isMasterStory = isMasterStory;
            this.similarToStory = similarToStory;
            this.spliceAbove = spliceAbove;
            this.spliceHeight = spliceHeight;
            this.color = color;
        }
        public double baseElevation { get; set; }
        public int numberStories { get; set; }

        public List<string> storyNames { get; set; } = new List<string>();
        public List<double> storyHeights { get; set; } = new List<double>();
        public List<bool> isMasterStory { get; set; } = new List<bool>();
        public List<string> similarToStory { get; set; } = new List<string>();
        public List<bool> spliceAbove { get; set; } = new List<bool>();
        public List<double> spliceHeight { get; set; } = new List<double>();
        public List<int> color { get; set; } = new List<int>();

    }
}

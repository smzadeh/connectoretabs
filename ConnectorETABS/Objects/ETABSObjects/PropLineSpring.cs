﻿using Speckle.Core.Models;

namespace Objects.ETABSObjects
{
    public class PropLineSpring : Base
    {
        public PropLineSpring()
        {
        }

        public PropLineSpring(string name, double u1, double u2, double u3,
            double r1, int nonlinearOption2, int nonlinearOption3,
            int color = 0, string notes = "", string iGUID = "")
        {
            this.name = name;
            this.u1 = u1;
            this.u2 = u2;
            this.u3 = u3;
            this.r1 = r1;
            this.nonlinearOption2 = nonlinearOption2;
            this.nonlinearOption3 = nonlinearOption3;
            this.color = color;
            this.notes = notes;
            this.iGUID = iGUID;
        }
        public string name { get; set; }
        public double u1 { get; set; }
        public double u2 { get; set; }
        public double u3 { get; set; }
        public double r1 { get; set; }
        public int nonlinearOption2 { get; set; }
        public int nonlinearOption3 { get; set; }
        public int color { get; set; } = 0;
        public string notes { get; set; } = "";
        public string iGUID { get; set; } = "";

    }
}

﻿using Speckle.Core.Models;
using Speckle.Newtonsoft.Json;
using System.Collections.Generic;

namespace Objects.ETABSObjects
{
    public class LoadUniform : Base
    {
        public int numberItems { get; set; }
        public List<string> areaName { get; set; } = new List<string>();
        public List<LoadPattern> loadPat { get; set; } = new List<LoadPattern>();
        public List<string> cSys { get; set; } = new List<string>();
        public List<int> dir { get; set; } = new List<int>();
        public List<double> value { get; set; } = new List<double>();

    }
    public class LoadUniformToFrame : Base
    {
        public int numberItems { get; set; }
        public List<string> areaName { get; set; } = new List<string>();
        public List<LoadPattern> loadPat { get; set; } = new List<LoadPattern>();
        public List<string> cSys { get; set; } = new List<string>();
        public List<int> dir { get; set; } = new List<int>();
        public List<double> value { get; set; } = new List<double>();
        public List<int> distType { get; set; } = new List<int>();
    }
    public class LoadWindPressure : Base
    {
        public int numberItems { get; set; }
        public List<string> areaName { get; set; } = new List<string>();
        public List<LoadPattern> loadPat { get; set; } = new List<LoadPattern>();
        public List<int> myType { get; set; } = new List<int>();
        public List<double> cp { get; set; } = new List<double>();
    }
    public class Opening : Base
    {
        public bool isOpening { get; set; }
    }
    public class Area : Base
    {
        public Area()
        {
        }
        public Area(List<Point> points, string name, PropArea section,
            Diaphragm diaphragm = null, bool? edgeConstraintExists = null,
            GroupAssign groupAssign = null, LoadTemperature loadTemperature = null,
            LoadUniform loadUniform = null, LoadUniformToFrame loadUniformToFrame = null,
            LoadWindPressure loadWindPressure = null, LocalAxes localAxes = null,
            double? mass = null, MaterialOverwrite materialOverwrite = null,
            Modifiers modifiers = null, Opening opening = null, Pier pier = null,
            Spandrel spandrel = null, SpringAssignment springAssignment = null)
        {
            this.points = points;
            this.name = name;
            this.section = section;
            this.diaphragm = diaphragm;
            this.edgeConstraintExists = edgeConstraintExists;
            this.groupAssign = groupAssign;
            this.loadTemperature = loadTemperature;
            this.loadUniform = loadUniform;
            this.loadUniformToFrame = loadUniformToFrame;
            this.loadWindPressure = loadWindPressure;
            this.localAxes = localAxes;
            this.mass = mass;
            this.materialOverwrite = materialOverwrite;
            this.modifiers = modifiers;
            this.opening = opening;
            this.pier = pier;
            this.spandrel = spandrel;
            this.springAssignment = springAssignment;
        }

        public string name { get; set; }
        public PropArea section { get; set; }
        public List<Point> points { get; set; }
        //public string label { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Diaphragm diaphragm { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? edgeConstraintExists { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public GroupAssign groupAssign { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadTemperature loadTemperature { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadUniform loadUniform { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadUniformToFrame loadUniformToFrame { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LoadWindPressure loadWindPressure { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocalAxes localAxes { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? mass { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MaterialOverwrite materialOverwrite { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Modifiers modifiers { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Opening opening { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Pier pier { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Spandrel spandrel { get; set; } = null;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SpringAssignment springAssignment { get; set; } = null;
    }
}

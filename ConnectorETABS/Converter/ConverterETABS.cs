﻿using ConnectorETABS.ETABS.UI;
using Objects.ETABSObjects;
using Speckle.Core.Kits;
using Speckle.Core.Logging;
using Speckle.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using utils = ConnectorETABS.ConnectorETABSUtils;


namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS : ISpeckleConverter
    {
        public string Description => "Speckle Kit for ETABS";

        public string Name => nameof(ConverterETABS);

        public string Author => "Soroush Mohammadzadeh";

        public string WebsiteOrEmail => "smz.connectorETABS@gmail.com";

        public IEnumerable<string> GetServicedApplications() => new string[] { ConnectorETABS.Applications.ETABS18 };

        public HashSet<Exception> ConversionErrors { get; private set; } = new HashSet<Exception>();

        public List<ApplicationPlaceholderObject> ContextObjects { get; set; } = new List<ApplicationPlaceholderObject>();
        public void SetContextObjects(List<ApplicationPlaceholderObject> objects) => ContextObjects = objects;
        public void SetPreviousContextObjects(List<ApplicationPlaceholderObject> objects) => throw new NotImplementedException();

        //public UIApplication Doc { get; private set; }
        public UIDocument Doc { get; private set; }

        public void SetContextDocument(object doc)
        {
            Doc = (UIDocument)doc;
        }

        public bool CanConvertToSpeckle(object @object)
        {
            foreach (var type in Enum.GetNames(typeof(utils.ETABSAPIUsableTypes)))
            {
                if (type == (string)@object)
                {
                    return true;
                }
            }
            return false;
        }

        public bool CanConvertToNative(Base @object)
        {
            foreach (var type in Enum.GetNames(typeof(utils.ETABSAPIUsableTypes)))
            {
                if (type == @object.GetType().Name)
                {
                    return true;
                }
            }
            return false;
        }


        public List<object> ConvertToNative(List<Base> objects)
        {
            return objects.Select(x => ConvertToNative(x)).ToList();
        }

        public object ConvertToNative(Base @object)
        {
            switch (@object)
            {
                case Point o:
                    return PointToNative(o);
                case Frame o:
                    return FrameToNative(o);
                case Area o:
                    return AreaToNative(o);
                case Link o:
                    return LinkToNative(o);
                case PropMaterial o:
                    return PropMaterialToNative(o);
                case PropFrame o:
                    return PropFrameToNative(o);
                case LoadCase o:
                    return LoadCaseToNative(o);
                case LoadPattern o:
                    return LoadPatternToNative(o);
                case GridSys o:
                    return GridSysToNative(o);
                case Combo o:
                    return ComboToNative(o);
                case DesignSteel o:
                    return DesignSteelToNative(o);
                case DesignConcrete o:
                    return DesignConcreteToNative(o);
                case Story o:
                    return StoryToNative(o);
                case Diaphragm o:
                    return DiaphragmToNative(o);
                case PierLabel o:
                    return PierLabelToNative(o);
                case PropAreaSpring o:
                    return PropAreaSpringToNative(o);
                case PropLineSpring o:
                    return PropLineSpringToNative(o);
                case PropPointSpring o:
                    return PropPointSpringToNative(o);
                case SpandrelLabel o:
                    return SpandrelLabelToNative(o);
                case PropTendon o:
                    return PropTendonToNative(o);
                case PropLink o:
                    return PropLinkToNative(o);
                case PropArea o:
                    return PropAreaToNative(o);
                default:
                    ConversionErrors.Add(new SpeckleException($"Unsupported Speckle Object: Can not convert {@object.GetType().Name} to native"
                        , level: Sentry.SentryLevel.Warning));
                    return null;
            }
        }


        public List<Base> ConvertToSpeckle(List<object> objects)
        {
            return objects.Select(x => ConvertToSpeckle(x)).ToList();
        }

        public Base ConvertToSpeckle(object @object)
        {
            (string type, string name) = ((string, string))@object;
            Base returnObject = null;
            switch (type)
            {
                case "Point":
                    returnObject = PointToSpeckle(name);
                    break;
                case "Frame":
                    returnObject = FrameToSpeckle(name);
                    break;
                case "Area":
                    returnObject = AreaToSpeckle(name);
                    break;
                case "Link":
                    returnObject = LinkToSpeckle(name);
                    break;
                case "PropMaterial":
                    returnObject = PropMaterialToSpeckle(name);
                    break;
                case "PropFrame":
                    returnObject = PropFrameToSpeckle(name);
                    break;
                case "LoadCase":
                    returnObject = LoadCaseToSpeckle(name);
                    break;
                case "LoadPattern":
                    returnObject = LoadPatternToSpeckle(name);
                    break;
                case "GridSys":
                    returnObject = GridSysToSpeckle(name);
                    break;
                case "Combo":
                    returnObject = ComboToSpeckle(name);
                    break;
                case "DesignSteel":
                    returnObject = DesignSteelToSpeckle(name);
                    break;
                case "DeisgnConcrete":
                    returnObject = DesignConcreteToSpeckle(name);
                    break;
                case "Story":
                    returnObject = StoryToSpeckle(name);
                    break;
                case "Diaphragm":
                    returnObject = DiaphragmToSpeckle(name);
                    break;
                case "PierLabel":
                    returnObject = PierLabelToSpeckle(name);
                    break;
                case "PropAreaSpring":
                    returnObject = PropAreaSpringToSpeckle(name);
                    break;
                case "PropLineSpring":
                    returnObject = PropLineSpringToSpeckle(name);
                    break;
                case "PropPointSpring":
                    returnObject = PropPointSpringToSpeckle(name);
                    break;
                case "SpandrelLabel":
                    returnObject = SpandrelLabelToSpeckle(name);
                    break;
                case "PropTendon":
                    returnObject = PropTendonToSpeckle(name);
                    break;
                case "PropLink":
                    returnObject = PropLinkToSpeckle(name);
                    break;
                case "PropArea":
                    returnObject = PropAreaToSpeckle(name);
                    break;
                default:
                    ConversionErrors.Add(new SpeckleException($"Unsupported native Object: Can not convert {type} to native"));
                    returnObject = null;
                    break;
            }
            return returnObject;
        }

        public bool tryWrapper(Func<int> apiCall)
        {
            try
            {
                int ret = apiCall();
                if (ret == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                ConversionErrors.Add(new SpeckleException($"API call {apiCall.Method.Name} failed with error {e}",
                    level: Sentry.SentryLevel.Warning));
                return false;
            }
        }
    }
}


﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System.Linq;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropPointSpringToNative util functions

        private PropPointSpring SetPropPointSpring(PropPointSpring propPointSpring)
        {
            double[] k = propPointSpring.k.ToArray();
            if (!tryWrapper(() => Doc.Document.PropPointSpring.SetPointSpringProp(propPointSpring.name,
                propPointSpring.springOption, ref k, propPointSpring.cSys, propPointSpring.soilProfile,
                propPointSpring.footing, propPointSpring.period, propPointSpring.color,
                propPointSpring.notes, propPointSpring.iGUID)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native propPointSpring ${propPointSpring.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            if (propPointSpring.singleJointLinks != null)
            {
                string[] linkNames = propPointSpring.singleJointLinks.linkNames.ToArray();
                int[] linkAxialDirs = propPointSpring.singleJointLinks.linkAxialDirs.ToArray();
                double[] linkAngles = propPointSpring.singleJointLinks.linkAngles.ToArray();
                tryWrapper(() => Doc.Document.PropPointSpring.SetLinks(propPointSpring.name,
                    propPointSpring.singleJointLinks.numberLinks, ref linkNames, ref linkAxialDirs,
                    ref linkAngles));
            }
            return propPointSpring;
        }
        #endregion
        public object PropPointSpringToNative(PropPointSpring propPointSpring)
        {
            propPointSpring = SetPropPointSpring(propPointSpring);
            return propPointSpring.name;
        }

        #region PropPointSpringToSpeckle util functions

        private PropPointSpring GetPropPointSpring(PropPointSpring propPointSpring)
        {
            int springOption = 0; double[] k = { }; string cSys = "";
            string soilProfile = ""; string footing = ""; double period = 0; int color = 0;
            string notes = ""; string iGUID = "";
            if (tryWrapper(() => Doc.Document.PropPointSpring.GetPointSpringProp(propPointSpring.name,
                ref springOption, ref k, ref cSys, ref soilProfile, ref footing, ref period,
                ref color, ref notes, ref iGUID)))
            {
                propPointSpring.springOption = springOption;
                propPointSpring.k = k.ToList(); propPointSpring.cSys = cSys;
                propPointSpring.soilProfile = soilProfile;
                propPointSpring.footing = footing; propPointSpring.period = period;
                propPointSpring.color = color; propPointSpring.notes = notes;
                propPointSpring.iGUID = iGUID;
            }
            int numberLinks = 0; string[] linkNames = { };
            int[] linkAxialDirs = { }; double[] linkAngles = { };
            if (tryWrapper(() => Doc.Document.PropPointSpring.GetLinks(propPointSpring.name,
                ref numberLinks, ref linkNames, ref linkAxialDirs, ref linkAngles)))
            {
                if (numberLinks > 0)
                {
                    propPointSpring.singleJointLinks = new SingleJointLinks();
                    for (int index = 0; index < numberLinks; index++)
                    {
                        propPointSpring.singleJointLinks.linkNames.Add(linkNames[index]);
                        propPointSpring.singleJointLinks.linkAxialDirs.Add(linkAxialDirs[index]);
                        propPointSpring.singleJointLinks.linkAngles.Add(linkAngles[index]);
                    }
                }
            }
            return propPointSpring;
        }
        #endregion
        public PropPointSpring PropPointSpringToSpeckle(string name)
        {
            var propPointSpring = new PropPointSpring { name = name };
            propPointSpring = GetPropPointSpring(propPointSpring);
            return propPointSpring;
        }
    }
}

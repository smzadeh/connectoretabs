﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System.Linq;
using ET = ETABSv1;


namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropFrameToNative util functions

        private PropFrame SetISection(PropFrame propFrame)
        {
            if (propFrame.iSection != null)
            {
                PropMaterialToNative(propFrame.iSection.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetISection(propFrame.name,
                propFrame.iSection.matProp.name, propFrame.iSection.t3, propFrame.iSection.t2,
                propFrame.iSection.tf, propFrame.iSection.tw, propFrame.iSection.t2b,
                propFrame.iSection.tfb, propFrame.iSection.color, propFrame.iSection.notes,
                propFrame.iSection.GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                        $" of type {(ET.eFramePropType)propFrame.framePropType}",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propFrame;
        }
        private PropFrame SetChannelSection(PropFrame propFrame)
        {
            if (propFrame.channel != null)
            {
                PropMaterialToNative(propFrame.channel.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetChannel(propFrame.name,
                propFrame.channel.matProp.name, propFrame.channel.t3, propFrame.channel.t2, propFrame.channel.tf,
                propFrame.channel.tw, propFrame.channel.color, propFrame.channel.notes,
                propFrame.channel.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                        $" of type {(ET.eFramePropType)propFrame.framePropType}",
                        level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetSteelTeeSection(PropFrame propFrame)
        {
            if (propFrame.steelTee != null)
            {
                PropMaterialToNative(propFrame.steelTee.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetTee(propFrame.name,
                propFrame.steelTee.matProp.name, propFrame.steelTee.t3, propFrame.steelTee.t2,
                propFrame.steelTee.tf, propFrame.steelTee.tw, propFrame.steelTee.color,
                propFrame.steelTee.notes, propFrame.steelTee.GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning));
                }
            }
            return propFrame;
        }
        private PropFrame SetAngleSection(PropFrame propFrame)
        {
            if (propFrame.steelAngle != null)
            {
                PropMaterialToNative(propFrame.steelAngle.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetSteelAngle(propFrame.name,
                propFrame.steelAngle.matProp.name, propFrame.steelAngle.t3, propFrame.steelAngle.t2,
                propFrame.steelAngle.tf, propFrame.steelAngle.tw, propFrame.steelAngle.r,
                propFrame.steelAngle.mirrorAbout2, propFrame.steelAngle.mirrorAbout3,
                propFrame.steelAngle.color, propFrame.steelAngle.notes,
                propFrame.steelAngle.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetDblAngleSection(PropFrame propFrame)
        {
            if (propFrame.dblAngle != null)
            {
                PropMaterialToNative(propFrame.dblAngle.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetDblAngle(propFrame.name,
                propFrame.dblAngle.matProp.name, propFrame.dblAngle.t3, propFrame.dblAngle.t2,
                propFrame.dblAngle.tf, propFrame.dblAngle.tw, propFrame.dblAngle.dis,
                propFrame.dblAngle.color, propFrame.dblAngle.notes,
                propFrame.dblAngle.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetBoxSection(PropFrame propFrame)
        {
            if (propFrame.tube != null)
            {
                PropMaterialToNative(propFrame.tube.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetTube(propFrame.name,
                    propFrame.tube.matProp.name, propFrame.tube.t3, propFrame.tube.t2,
                    propFrame.tube.tf, propFrame.tube.tw, propFrame.tube.color,
                    propFrame.tube.notes, propFrame.tube.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetPipeSection(PropFrame propFrame)
        {
            if (propFrame.pipe != null)
            {
                PropMaterialToNative(propFrame.pipe.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetPipe(propFrame.name,
                propFrame.pipe.matProp.name, propFrame.pipe.t3, propFrame.pipe.tw,
                propFrame.pipe.color, propFrame.pipe.notes,
                propFrame.pipe.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetRectangularSection(PropFrame propFrame)
        {
            if (propFrame.rectangle != null)
            {
                PropMaterialToNative(propFrame.rectangle.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRectangle(propFrame.name,
                propFrame.rectangle.matProp.name, propFrame.rectangle.t3, propFrame.rectangle.t2,
                propFrame.rectangle.color, propFrame.rectangle.notes,
                propFrame.rectangle.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            if (propFrame.rectangle.rebarBeam != null)
            {
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRebarBeam(propFrame.name,
                    propFrame.rectangle.rebarBeam.matPropLong, propFrame.rectangle.rebarBeam.matPropConfine,
                    propFrame.rectangle.rebarBeam.coverTop, propFrame.rectangle.rebarBeam.coverBot,
                    propFrame.rectangle.rebarBeam.topLeftArea, propFrame.rectangle.rebarBeam.topRightArea,
                    propFrame.rectangle.rebarBeam.botLeftArea, propFrame.rectangle.rebarBeam.botRightArea)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            if (propFrame.rectangle.rebarColumn != null)
            {
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRebarColumn(propFrame.name,
                    propFrame.rectangle.rebarColumn.matPropLong, propFrame.rectangle.rebarColumn.matPropConfine,
                    propFrame.rectangle.rebarColumn.pattern, propFrame.rectangle.rebarColumn.confineType,
                    propFrame.rectangle.rebarColumn.cover, propFrame.rectangle.rebarColumn.numberCBars,
                    propFrame.rectangle.rebarColumn.numberR3Bars, propFrame.rectangle.rebarColumn.numberR2Bars,
                    propFrame.rectangle.rebarColumn.rebarSize,
                    propFrame.rectangle.rebarColumn.tieSize, propFrame.rectangle.rebarColumn.tieSpacingLongit,
                    propFrame.rectangle.rebarColumn.number2DirTieBars,
                    propFrame.rectangle.rebarColumn.number3DirTieBars,
                    propFrame.rectangle.rebarColumn.toBeDesigned)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetCircleSection(PropFrame propFrame)
        {
            if (propFrame.circle != null)
            {
                PropMaterialToNative(propFrame.circle.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetCircle(propFrame.name,
                propFrame.circle.matProp.name, propFrame.circle.t3, propFrame.circle.color, propFrame.circle.notes,
                propFrame.circle.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }

            }
            if (propFrame.circle.rebarBeam != null)
            {
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRebarBeam(propFrame.name,
                    propFrame.circle.rebarBeam.matPropLong, propFrame.circle.rebarBeam.matPropConfine,
                    propFrame.circle.rebarBeam.coverTop, propFrame.circle.rebarBeam.coverBot,
                    propFrame.circle.rebarBeam.topLeftArea, propFrame.circle.rebarBeam.topRightArea,
                    propFrame.circle.rebarBeam.botLeftArea, propFrame.circle.rebarBeam.botRightArea)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            if (propFrame.circle.rebarColumn != null)
            {
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRebarColumn(propFrame.name,
                    propFrame.circle.rebarColumn.matPropLong, propFrame.circle.rebarColumn.matPropConfine,
                    propFrame.circle.rebarColumn.pattern, propFrame.circle.rebarColumn.confineType,
                    propFrame.circle.rebarColumn.cover, propFrame.circle.rebarColumn.numberCBars,
                    propFrame.circle.rebarColumn.numberR3Bars, propFrame.circle.rebarColumn.numberR2Bars,
                    propFrame.circle.rebarColumn.rebarSize,
                    propFrame.circle.rebarColumn.tieSize, propFrame.circle.rebarColumn.tieSpacingLongit,
                    propFrame.circle.rebarColumn.number2DirTieBars,
                    propFrame.circle.rebarColumn.number3DirTieBars,
                    propFrame.circle.rebarColumn.toBeDesigned)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetGeneralSection(PropFrame propFrame)
        {
            if (propFrame.generalSectionProp != null)
            {
                PropMaterialToNative(propFrame.generalSectionProp.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetGeneral(propFrame.name,
                propFrame.generalSectionProp.matProp.name, propFrame.generalSectionProp.t3, propFrame.generalSectionProp.t2,
                propFrame.generalSectionProp.Area, propFrame.generalSectionProp.as2,
                propFrame.generalSectionProp.as3, propFrame.generalSectionProp.torsion,
                propFrame.generalSectionProp.i22, propFrame.generalSectionProp.i33,
                propFrame.generalSectionProp.s22, propFrame.generalSectionProp.s33,
                propFrame.generalSectionProp.z22, propFrame.generalSectionProp.z33,
                propFrame.generalSectionProp.r22, propFrame.generalSectionProp.r33,
                propFrame.generalSectionProp.color, propFrame.generalSectionProp.notes,
                propFrame.generalSectionProp.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetDbChannelSection(PropFrame propFrame)
        {
            if (propFrame.dblChannel != null)
            {
                PropMaterialToNative(propFrame.dblChannel.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetDblChannel(propFrame.name,
                propFrame.dblChannel.matProp.name, propFrame.dblChannel.t3, propFrame.dblChannel.t2,
                propFrame.dblChannel.tf, propFrame.dblChannel.tw, propFrame.dblChannel.dis,
                propFrame.dblChannel.color, propFrame.dblChannel.notes,
                propFrame.dblChannel.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }

        private PropFrame SetAutoSection(PropFrame propFrame)
        {
            if (propFrame.autoSelectSteel != null)
            {
                string[] sectionName = propFrame.autoSelectSteel.sectName.ToArray();
                if (!tryWrapper(() => Doc.Document.PropFrame.SetAutoSelectSteel(propFrame.name,
                propFrame.autoSelectSteel.numberItems, ref sectionName,
                propFrame.autoSelectSteel.autoStartSection, propFrame.autoSelectSteel.notes,
                propFrame.autoSelectSteel.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetSDSection(PropFrame propFrame)
        {
            if (propFrame.sDSection != null)
            {
                PropMaterialToNative(propFrame.sDSection.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetSDSection(propFrame.name,
                    propFrame.sDSection.matProp.name, propFrame.sDSection.designType,
                    propFrame.sDSection.color, propFrame.sDSection.notes,
                    propFrame.sDSection.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetConcrete_LSection(PropFrame propFrame)
        {
            if (propFrame.concreteL != null)
            {
                PropMaterialToNative(propFrame.concreteL.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetConcreteL(propFrame.name,
                    propFrame.concreteL.matProp.name, propFrame.concreteL.t3, propFrame.concreteL.t2,
                    propFrame.concreteL.tf, propFrame.concreteL.twC, propFrame.concreteL.twT,
                    propFrame.concreteL.mirrorAbout2, propFrame.concreteL.mirrorAbout3,
                    propFrame.concreteL.color, propFrame.concreteL.notes,
                    propFrame.concreteL.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            if (propFrame.concreteL.rebarBeam != null)
            {
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRebarBeam(propFrame.name,
                    propFrame.concreteL.rebarBeam.matPropLong, propFrame.concreteL.rebarBeam.matPropConfine,
                    propFrame.concreteL.rebarBeam.coverTop, propFrame.concreteL.rebarBeam.coverBot,
                    propFrame.concreteL.rebarBeam.topLeftArea, propFrame.concreteL.rebarBeam.topRightArea,
                    propFrame.concreteL.rebarBeam.botLeftArea, propFrame.concreteL.rebarBeam.botRightArea)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetConcreteTeeSection(PropFrame propFrame)
        {
            if (propFrame.concreteTee != null)
            {
                PropMaterialToNative(propFrame.concreteTee.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetConcreteTee(propFrame.name,
                    propFrame.concreteTee.matProp.name, propFrame.concreteTee.t3,
                    propFrame.concreteTee.t2, propFrame.concreteTee.tf,
                    propFrame.concreteTee.twF, propFrame.concreteTee.twT,
                    propFrame.concreteTee.mirrorAbout3,
                    propFrame.concreteTee.color, propFrame.concreteTee.notes,
                    propFrame.concreteTee.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            if (propFrame.concreteTee.rebarBeam != null)
            {
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRebarBeam(propFrame.name,
                    propFrame.concreteTee.rebarBeam.matPropLong, propFrame.concreteTee.rebarBeam.matPropConfine,
                    propFrame.concreteTee.rebarBeam.coverTop, propFrame.concreteTee.rebarBeam.coverBot,
                    propFrame.concreteTee.rebarBeam.topLeftArea, propFrame.concreteTee.rebarBeam.topRightArea,
                    propFrame.concreteTee.rebarBeam.botLeftArea, propFrame.concreteTee.rebarBeam.botRightArea)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetBuiltupICoverplateSection(PropFrame propFrame)
        {
            if (propFrame.coverPlatedI != null)
            {
                PropMaterialToNative(propFrame.coverPlatedI.matPropTop);
                PropMaterialToNative(propFrame.coverPlatedI.matPropBot);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetCoverPlatedI(propFrame.name,
                    propFrame.coverPlatedI.sectName, propFrame.coverPlatedI.fyTopFlange,
                    propFrame.coverPlatedI.fyWeb, propFrame.coverPlatedI.fyBotFlange,
                    propFrame.coverPlatedI.tc, propFrame.coverPlatedI.bc,
                    propFrame.coverPlatedI.matPropTop.name, propFrame.coverPlatedI.tcb,
                    propFrame.coverPlatedI.bcb, propFrame.coverPlatedI.matPropBot.name,
                    propFrame.coverPlatedI.color, propFrame.coverPlatedI.notes,
                    propFrame.coverPlatedI.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetVariableSection(PropFrame propFrame)
        {
            if (propFrame.nonPrismatic != null)
            {
                string[] startSec = propFrame.nonPrismatic.startSec.ToArray();
                string[] endSec = propFrame.nonPrismatic.endSec.ToArray();
                double[] myLength = propFrame.nonPrismatic.myLength.ToArray();
                int[] myType = propFrame.nonPrismatic.myType.ToArray();
                int[] eI33 = propFrame.nonPrismatic.eI33.ToArray();
                int[] eI22 = propFrame.nonPrismatic.eI22.ToArray();
                if (!tryWrapper(() => Doc.Document.PropFrame.SetNonPrismatic(propFrame.name,
                    propFrame.nonPrismatic.numberItems, ref startSec, ref endSec, ref myLength,
                    ref myType, ref eI33, ref eI22, propFrame.nonPrismatic.color,
                    propFrame.nonPrismatic.notes, propFrame.nonPrismatic.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetSteelPlateSection(PropFrame propFrame)
        {
            if (propFrame.plate != null)
            {
                PropMaterialToNative(propFrame.plate.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetPlate(propFrame.name,
                    propFrame.plate.matProp.name, propFrame.plate.t3, propFrame.plate.t2,
                    propFrame.plate.color, propFrame.plate.notes, propFrame.plate.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }
        private PropFrame SetSteelRodSection(PropFrame propFrame)
        {
            if (propFrame.rod != null)
            {
                PropMaterialToNative(propFrame.rod.matProp);
                if (!tryWrapper(() => Doc.Document.PropFrame.SetRod(propFrame.name,
                    propFrame.rod.matProp.name, propFrame.rod.t3, propFrame.rod.color,
                    propFrame.rod.notes, propFrame.rod.GUID)))
                {
                    throw new SpeckleException($"Failed to create native propFrame ${propFrame.name}" +
                    $" of type {(ET.eFramePropType)propFrame.framePropType}",
                    level: Sentry.SentryLevel.Warning);
                }
            }
            return propFrame;
        }




        #endregion
        public object PropFrameToNative(PropFrame propFrame)
        {
            switch (propFrame.framePropType)
            {
                case 1:
                    return SetISection(propFrame);
                case 2:
                    return SetChannelSection(propFrame);
                case 3:
                    return SetSteelTeeSection(propFrame);
                case 4:
                    return SetAngleSection(propFrame);
                case 5:
                    return SetDblAngleSection(propFrame);
                case 6:
                    // same as tube
                    return SetBoxSection(propFrame);
                case 7:
                    return SetPipeSection(propFrame);
                case 8:
                    return SetRectangularSection(propFrame);
                case 9:
                    return SetCircleSection(propFrame);
                case 10:
                    return SetGeneralSection(propFrame);
                case 11:
                    return SetDbChannelSection(propFrame);
                case 12:
                    return SetAutoSection(propFrame);
                case 13:
                    return SetSDSection(propFrame);
                case 14:
                    // same as nonprismatic
                    return SetVariableSection(propFrame);
                case 23:
                    return SetBuiltupICoverplateSection(propFrame);
                case 28:
                    return SetConcrete_LSection(propFrame);
                case 35:
                    return SetConcreteTeeSection(propFrame);
                case 39:
                    return SetSteelPlateSection(propFrame);
                case 40:
                    return SetSteelRodSection(propFrame);
                default:
                    throw new SpeckleException($"Skipping not supported type: " +
                        $"{(ET.eFramePropType)propFrame.framePropType}");
            }
        }
        #region PropFrameToSpeckle util functions

        private PropFrame GetISection(PropFrame propFrame)
        {
            var iSection = new ISection();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, tw = 0, t2b = 0, tfb = 0;
            int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetISection(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref tw, ref t2b,
                ref tfb, ref color, ref notes, ref GUID)))
            {
                propFrame.iSection = iSection;
                propFrame.iSection.fileName = fileName; 
                propFrame.iSection.matProp = PropMaterialToSpeckle(matProp);
                propFrame.iSection.t3 = t3; propFrame.iSection.t2 = t2;
                propFrame.iSection.tf = tf; propFrame.iSection.tw = tw;
                propFrame.iSection.t2b = t2b; propFrame.iSection.tfb = tfb;
                propFrame.iSection.color = color; propFrame.iSection.notes = notes;
                propFrame.iSection.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetChannelSection(PropFrame propFrame)
        {
            var channel = new Channel();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, tw = 0;
            int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetChannel(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref tw,
                ref color, ref notes, ref GUID)))
            {
                propFrame.channel = channel;
                propFrame.channel.fileName = fileName; 
                propFrame.channel.matProp = PropMaterialToSpeckle(matProp);
                propFrame.channel.t3 = t3; propFrame.channel.t2 = t2;
                propFrame.channel.tf = tf; propFrame.channel.tw = tw;
                propFrame.channel.color = color; propFrame.channel.notes = notes;
                propFrame.channel.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetSteelTeeSection(PropFrame propFrame)
        {
            var tee = new SteelTee();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, tw = 0, r = 0;
            int color = -1; bool mirrorAbout3 = false;
            if (tryWrapper(() => Doc.Document.PropFrame.GetSteelTee(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref tw, ref r, ref mirrorAbout3,
                ref color, ref notes, ref GUID)))
            {
                propFrame.steelTee = tee;
                propFrame.steelTee.fileName = fileName; 
                propFrame.steelTee.matProp = PropMaterialToSpeckle(matProp);
                propFrame.steelTee.t3 = t3; propFrame.steelTee.t2 = t2;
                propFrame.steelTee.tf = tf; propFrame.steelTee.tw = tw;
                propFrame.steelTee.r = r; propFrame.steelTee.mirrorAbout3 = mirrorAbout3;
                propFrame.steelTee.color = color; propFrame.steelTee.notes = notes;
                propFrame.steelTee.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetAngleSection(PropFrame propFrame)
        {
            var angle = new SteelAngle();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, tw = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetAngle(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref tw,
                ref color, ref notes, ref GUID)))
            {
                propFrame.steelAngle = angle;
                propFrame.steelAngle.fileName = fileName; 
                propFrame.steelAngle.matProp = PropMaterialToSpeckle(matProp);
                propFrame.steelAngle.t3 = t3; propFrame.steelAngle.t2 = t2;
                propFrame.steelAngle.tf = tf; propFrame.steelAngle.tw = tw;
                propFrame.steelAngle.color = color; propFrame.steelAngle.notes = notes;
                propFrame.steelAngle.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetDblAngleSection(PropFrame propFrame)
        {
            var dblAngle = new DblAngle();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, tw = 0, dis = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetDblAngle(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref tw, ref dis,
                ref color, ref notes, ref GUID)))
            {
                propFrame.dblAngle = dblAngle;
                propFrame.dblAngle.fileName = fileName; 
                propFrame.dblAngle.matProp = PropMaterialToSpeckle(matProp);
                propFrame.dblAngle.t3 = t3; propFrame.dblAngle.t2 = t2;
                propFrame.dblAngle.tf = tf; propFrame.dblAngle.tw = tw;
                propFrame.dblAngle.dis = dis;
                propFrame.dblAngle.color = color; propFrame.dblAngle.notes = notes;
                propFrame.dblAngle.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetBoxSection(PropFrame propFrame)
        {
            var tube = new Tube();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, tw = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetTube(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref tw,
                ref color, ref notes, ref GUID)))
            {
                propFrame.tube = tube;
                propFrame.tube.fileName = fileName; 
                propFrame.tube.matProp = PropMaterialToSpeckle(matProp);
                propFrame.tube.t3 = t3; propFrame.tube.t2 = t2;
                propFrame.tube.tf = tf; propFrame.tube.tw = tw;
                propFrame.tube.color = color; propFrame.tube.notes = notes;
                propFrame.tube.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetPipeSection(PropFrame propFrame)
        {
            var pipe = new Pipe();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, tw = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetPipe(propFrame.name,
                ref fileName, ref matProp, ref t3, ref tw,
                ref color, ref notes, ref GUID)))
            {
                propFrame.pipe = pipe;
                propFrame.pipe.fileName = fileName; 
                propFrame.pipe.matProp = PropMaterialToSpeckle(matProp);
                propFrame.pipe.t3 = t3; propFrame.pipe.tw = tw;
                propFrame.pipe.color = color; propFrame.pipe.notes = notes;
                propFrame.pipe.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetRectangularSection(PropFrame propFrame)
        {
            var rectangle = new Rectangle();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetRectangle(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2,
                ref color, ref notes, ref GUID)))
            {
                propFrame.rectangle = rectangle;
                propFrame.rectangle.fileName = fileName; 
                propFrame.rectangle.matProp = PropMaterialToSpeckle(matProp);
                propFrame.rectangle.t3 = t3; propFrame.rectangle.t2 = t2;
                propFrame.rectangle.color = color; propFrame.rectangle.notes = notes;
                propFrame.rectangle.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetCircleSection(PropFrame propFrame)
        {
            var circle = new Circle();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetCircle(propFrame.name,
                ref fileName, ref matProp, ref t3,
                ref color, ref notes, ref GUID)))
            {
                propFrame.circle = circle;
                propFrame.circle.fileName = fileName; 
                propFrame.circle.matProp = PropMaterialToSpeckle(matProp);
                propFrame.circle.t3 = t3;
                propFrame.circle.color = color; propFrame.circle.notes = notes;
                propFrame.circle.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetGeneralSection(PropFrame propFrame)
        {
            var generalSectionProp = new GeneralSectionProp();
            string fileName = "", matProp = "", notes = "", GUID = "";
            int color = -1;
            double t3 = 0, t2 = 0, Area = 0, as2 = 0, as3 = 0, torsion = 0, i22 = 0, i33 = 0, s22 = 0;
            double s33 = 0, z22 = 0, z33 = 0, r22 = 0, r33 = 0;
            if (tryWrapper(() => Doc.Document.PropFrame.GetGeneral(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref Area, ref as2, ref as3,
                ref torsion, ref i22, ref i33, ref s22, ref s33, ref z22, ref z33,
                ref r22, ref r33, ref color, ref notes, ref GUID)))
            {
                propFrame.generalSectionProp = generalSectionProp;
                propFrame.generalSectionProp.fileName = fileName; 
                propFrame.generalSectionProp.matProp = PropMaterialToSpeckle(matProp);
                propFrame.generalSectionProp.t3 = t3; propFrame.generalSectionProp.t2 = t2;
                propFrame.generalSectionProp.Area = Area; propFrame.generalSectionProp.as2 = as2;
                propFrame.generalSectionProp.as3 = as3; propFrame.generalSectionProp.torsion = torsion;
                propFrame.generalSectionProp.i22 = i22; propFrame.generalSectionProp.i33 = i33;
                propFrame.generalSectionProp.s22 = s22; propFrame.generalSectionProp.s33 = s33;
                propFrame.generalSectionProp.z22 = z33; propFrame.generalSectionProp.r22 = r22;
                propFrame.generalSectionProp.r33 = r33; propFrame.generalSectionProp.color = color;
                propFrame.generalSectionProp.notes = notes; propFrame.generalSectionProp.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetDbChannelSection(PropFrame propFrame)
        {
            var dblChannel = new DblChannel();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, tw = 0, dis = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetDblChannel(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref tw, ref dis,
                ref color, ref notes, ref GUID)))
            {
                propFrame.dblChannel = dblChannel;
                propFrame.dblChannel.fileName = fileName; 
                propFrame.dblChannel.matProp = PropMaterialToSpeckle(matProp);
                propFrame.dblChannel.t3 = t3; propFrame.dblChannel.t2 = t2;
                propFrame.dblChannel.tf = tf; propFrame.dblChannel.tw = tw;
                propFrame.dblChannel.dis = dis;
                propFrame.dblChannel.color = color; propFrame.dblChannel.notes = notes;
                propFrame.dblChannel.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetAutoSection(PropFrame propFrame)
        {
            var autoSelectSteel = new AutoSelectSteel();
            string autoStartSection = "", notes = "", GUID = "";
            int numberItems = 0; string[] sectNames = { };
            if (tryWrapper(() => Doc.Document.PropFrame.GetAutoSelectSteel(propFrame.name,
                ref numberItems, ref sectNames, ref autoStartSection, ref notes, ref GUID)))
            {
                if (numberItems > 0)
                {
                    propFrame.autoSelectSteel = autoSelectSteel;
                    foreach (var name in sectNames.ToList())
                    {
                        propFrame.autoSelectSteel.sectName.Add(name);
                    }
                    propFrame.autoSelectSteel.notes = notes;
                    propFrame.autoSelectSteel.GUID = GUID;
                }
            }
            return propFrame;
        }
        private PropFrame GetSDSection(PropFrame propFrame)
        {
            var sDsection = new SDSection();
            string matProp = "", notes = "", GUID = "";
            int numberItems = 0, designType = 0, color = 0;
            int[] myType = { }; string[] shapeNames = { };
            if (tryWrapper(() => Doc.Document.PropFrame.GetSDSection(propFrame.name,
                ref matProp, ref numberItems, ref shapeNames, ref myType, ref designType,
                ref color, ref notes, ref GUID)))
            {
                if (numberItems > 0)
                {
                    propFrame.sDSection = sDsection;
                    foreach (var name in shapeNames.ToList())
                    {
                        propFrame.sDSection.shapeName.Add(name);
                    }
                    foreach (var item in myType.ToList())
                    {
                        propFrame.sDSection.myType.Add(item);
                    }
                    propFrame.sDSection.designType = designType;
                    propFrame.sDSection.color = color;
                    propFrame.sDSection.notes = notes;
                    propFrame.sDSection.GUID = GUID;
                }
            }
            return propFrame;
        }
        private PropFrame GetVariableSection(PropFrame propFrame)
        {
            var nonPrismatic = new NonPrismatic();
            string notes = "", GUID = "";
            int numberItems = 0, color = 0;
            int[] myType = { }, EI33 = { }, EI22 = { };
            string[] startSec = { }, endSec = { };
            double[] myLength = { };
            if (tryWrapper(() => Doc.Document.PropFrame.GetNonPrismatic(propFrame.name,
                ref numberItems, ref startSec, ref endSec, ref myLength, ref myType,
                ref EI33, ref EI22, ref color, ref notes, ref GUID)))
            {
                if (numberItems > 0)
                {
                    propFrame.nonPrismatic = nonPrismatic;
                    for (int index = 0; index < numberItems; index++)
                    {
                        propFrame.nonPrismatic.startSec.Add(startSec[index]);
                        propFrame.nonPrismatic.endSec.Add(endSec[index]);
                        propFrame.nonPrismatic.myLength.Add(myLength[index]);
                        propFrame.nonPrismatic.myType.Add(myType[index]);
                        propFrame.nonPrismatic.eI33.Add(EI22[index]);
                        propFrame.nonPrismatic.eI22.Add(EI22[index]);
                    }
                    propFrame.nonPrismatic.color = color;
                    propFrame.nonPrismatic.notes = notes;
                    propFrame.nonPrismatic.GUID = GUID;
                }
            }
            return propFrame;
        }
        private PropFrame GetBuiltupICoverplateSection(PropFrame propFrame)
        {
            var coverPlatedI = new CoverPlatedI();
            string sectName = "", matPropTop = "", matPropBot = "";
            double fyTopFlange = 0, fyWeb = 0, fyBotFlange = 0, tc = 0;
            double bc = 0, tcb = 0, bcb = 0; int color = 0;
            string notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropFrame.GetCoverPlatedI(propFrame.name,
                ref sectName, ref fyTopFlange, ref fyWeb, ref fyBotFlange, ref tc, ref bc, ref matPropTop,
                ref tcb, ref bcb, ref matPropBot, ref color, ref notes, ref GUID)))
            {
                propFrame.coverPlatedI = coverPlatedI;
                propFrame.coverPlatedI.sectName = sectName;
                propFrame.coverPlatedI.fyTopFlange = fyTopFlange;
                propFrame.coverPlatedI.fyWeb = fyWeb;
                propFrame.coverPlatedI.fyBotFlange = fyBotFlange;
                propFrame.coverPlatedI.tc = tc;
                propFrame.coverPlatedI.bc = bc;
                propFrame.coverPlatedI.matPropTop = PropMaterialToSpeckle(matPropTop);
                propFrame.coverPlatedI.tcb = tcb;
                propFrame.coverPlatedI.bcb = bcb;
                propFrame.coverPlatedI.matPropBot = PropMaterialToSpeckle(matPropBot);
                propFrame.coverPlatedI.color = color;
                propFrame.coverPlatedI.notes = notes;
                propFrame.coverPlatedI.GUID = GUID;
            }
            return propFrame;
        }

        private PropFrame GetConcrete_LSection(PropFrame propFrame)
        {
            var concreteL = new ConcreteL();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, twc = 0, twt = 0;
            int color = -1; bool mirror2 = false, mirror3 = false;
            if (tryWrapper(() => Doc.Document.PropFrame.GetConcreteL(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref twc, ref twt,
                ref mirror2, ref mirror3, ref color, ref notes, ref GUID)))
            {
                propFrame.concreteL = concreteL;
                propFrame.concreteL.fileName = fileName; 
                propFrame.concreteL.matProp = PropMaterialToSpeckle(matProp);
                propFrame.concreteL.t3 = t3; propFrame.concreteL.t2 = t2;
                propFrame.concreteL.tf = tf;
                propFrame.concreteL.twC = twc;
                propFrame.concreteL.twT = twt;
                propFrame.concreteL.mirrorAbout2 = mirror2;
                propFrame.concreteL.mirrorAbout3 = mirror3;
                propFrame.concreteL.color = color; propFrame.concreteL.notes = notes;
                propFrame.concreteL.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetConcreteTeeSection(PropFrame propFrame)
        {
            var concreteTee = new ConcreteTee();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0, tf = 0, twf = 0, twt = 0;
            int color = -1; bool mirror3 = false;
            if (tryWrapper(() => Doc.Document.PropFrame.GetConcreteTee(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref tf, ref twf, ref twt,
                ref mirror3, ref color, ref notes, ref GUID)))
            {
                propFrame.concreteTee = concreteTee;
                propFrame.concreteTee.fileName = fileName; 
                propFrame.concreteTee.matProp = PropMaterialToSpeckle(matProp);
                propFrame.concreteTee.t3 = t3; propFrame.concreteTee.t2 = t2;
                propFrame.concreteTee.tf = tf;
                propFrame.concreteTee.twF = twf;
                propFrame.concreteTee.twT = twt;
                propFrame.concreteTee.mirrorAbout3 = mirror3;
                propFrame.concreteTee.color = color; propFrame.concreteTee.notes = notes;
                propFrame.concreteTee.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetSteelPlateSection(PropFrame propFrame)
        {
            var plate = new Plate();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0, t2 = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetPlate(propFrame.name,
                ref fileName, ref matProp, ref t3, ref t2, ref color, ref notes, ref GUID)))
            {
                propFrame.plate = plate;
                propFrame.plate.fileName = fileName; 
                propFrame.plate.matProp = PropMaterialToSpeckle(matProp);
                propFrame.plate.t3 = t3; propFrame.plate.t2 = t2;
                propFrame.plate.color = color; propFrame.plate.notes = notes;
                propFrame.plate.GUID = GUID;
            }
            return propFrame;
        }
        private PropFrame GetSteelRodSection(PropFrame propFrame)
        {
            var rod = new Rod();
            string fileName = "", matProp = "", notes = "", GUID = "";
            double t3 = 0; int color = -1;
            if (tryWrapper(() => Doc.Document.PropFrame.GetRod(propFrame.name,
                ref fileName, ref matProp, ref t3, ref color, ref notes, ref GUID)))
            {
                propFrame.rod = rod;
                propFrame.rod.fileName = fileName; 
                propFrame.rod.matProp = PropMaterialToSpeckle(matProp);
                propFrame.rod.t3 = t3;
                propFrame.rod.color = color; propFrame.rod.notes = notes;
                propFrame.rod.GUID = GUID;
            }
            return propFrame;
        }

        #endregion
        public PropFrame PropFrameToSpeckle(string name)
        {
            var propFrame = new PropFrame { name = name };
            ET.eFramePropType propType = 0;
            tryWrapper(() => Doc.Document.PropFrame.GetTypeOAPI(name, ref propType));
            switch ((int)propType)
            {
                case 1:
                    return GetISection(propFrame);
                case 2:
                    return GetChannelSection(propFrame);
                case 3:
                    return GetSteelTeeSection(propFrame);
                case 4:
                    return GetAngleSection(propFrame);
                case 5:
                    return GetDblAngleSection(propFrame);
                case 6:
                    // same as tube
                    return GetBoxSection(propFrame);
                case 7:
                    return GetPipeSection(propFrame);
                case 8:
                    return GetRectangularSection(propFrame);
                case 9:
                    return GetCircleSection(propFrame);
                case 10:
                    return GetGeneralSection(propFrame);
                case 11:
                    return GetDbChannelSection(propFrame);
                case 12:
                    return GetAutoSection(propFrame);
                case 13:
                    return GetSDSection(propFrame);
                case 14:
                    // same as nonprismatic
                    return GetVariableSection(propFrame);
                case 23:
                    return GetBuiltupICoverplateSection(propFrame);
                case 28:
                    return GetConcrete_LSection(propFrame);
                case 35:
                    return GetConcreteTeeSection(propFrame);
                case 39:
                    return GetSteelPlateSection(propFrame);
                case 40:
                    return GetSteelRodSection(propFrame);
                default:
                    throw new SpeckleException($"Skipping not supported type: " +
                        $"{(ET.eFramePropType)propFrame.framePropType}");
            }
        }
    }
}

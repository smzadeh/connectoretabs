﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using ET = ETABSv1;
using ConnectorETABS;


namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropMaterialToNative util functions

        private bool speckleMaterialNameExistsInModel(PropMaterial speckleMaterial)
        {
            var names = ConnectorETABSUtils.GetAllPropMaterialNames(Doc);
            return names.Contains(speckleMaterial.name);
        }
        private (PropMaterial, bool) SetMaterial(PropMaterial propMaterial)
        {
            if (speckleMaterialNameExistsInModel(propMaterial))
            {
                    // no need to create a new propMaterial, only need to merge point props
                    return (propMaterial, true);
            }
            string propMaterialName = propMaterial.name;
            if (!tryWrapper(() => Doc.Document.PropMaterial.SetMaterial(propMaterialName,
                (ET.eMatType)propMaterial.matType)))
            {
                return (propMaterial, false);
            }
            propMaterial.name = propMaterialName;
            return (propMaterial, true);
        }
        private PropMaterial SetDamping(PropMaterial propMaterial)
        {
            if (propMaterial.damping != null)
            {
                tryWrapper(() => Doc.Document.PropMaterial.SetDamping(propMaterial.name,
                propMaterial.damping.modalRatio, propMaterial.damping.viscousMassCoeff,
                propMaterial.damping.viscousStiffCoeff, propMaterial.damping.hystereticMassCoeff,
                propMaterial.damping.hystereticStiffCoeff, (double)propMaterial.damping.Temp));
            }
            return propMaterial;
        }
        private PropMaterial SetMassSource(PropMaterial propMaterial)
        {
            if (propMaterial.massSource_1 != null)
            {
                bool IncludeElements = false, IncludeAddedMass = false, IncludeLoads = false;
                string[] loadPat = propMaterial.massSource_1.loadPat.ToArray();
                double[] sf = propMaterial.massSource_1.sF.ToArray();
                tryWrapper(() => Doc.Document.PropMaterial.SetMassSource_1(ref IncludeElements,
                ref IncludeAddedMass, ref IncludeLoads, propMaterial.massSource_1.numberLoads,
                ref loadPat, ref sf));
            }
            return propMaterial;
        }
        private PropMaterial SetMPAnisotropic(PropMaterial propMaterial)
        {
            if (propMaterial.mPAnisotropic != null)
            {
                double[] E = propMaterial.mPAnisotropic.e.ToArray();
                double[] U = propMaterial.mPAnisotropic.u.ToArray();
                double[] A = propMaterial.mPAnisotropic.a.ToArray();
                double[] G = propMaterial.mPAnisotropic.g.ToArray();
                tryWrapper(() => Doc.Document.PropMaterial.SetMPAnisotropic(propMaterial.name,
                    ref E, ref U, ref A, ref G, propMaterial.mPAnisotropic.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetMPIsotropic(PropMaterial propMaterial)
        {
            if (propMaterial.mPIsotropic != null)
            {
                tryWrapper(() => Doc.Document.PropMaterial.SetMPIsotropic(propMaterial.name,
                    propMaterial.mPIsotropic.e, propMaterial.mPIsotropic.u,
                    propMaterial.mPIsotropic.a, propMaterial.mPAnisotropic.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetMPOrthotropic(PropMaterial propMaterial)
        {
            if (propMaterial.mPOrthotropic != null)
            {
                double[] E = propMaterial.mPOrthotropic.e.ToArray();
                double[] U = propMaterial.mPOrthotropic.u.ToArray();
                double[] A = propMaterial.mPOrthotropic.a.ToArray();
                double[] G = propMaterial.mPOrthotropic.g.ToArray();
                tryWrapper(() => Doc.Document.PropMaterial.SetMPOrthotropic(propMaterial.name,
                    ref E, ref U, ref A, ref G, propMaterial.mPOrthotropic.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetMPUniaxial(PropMaterial propMaterial)
        {
            if (propMaterial.mPUniaxial != null)
            {
                tryWrapper(() => Doc.Document.PropMaterial.SetMPUniaxial(propMaterial.name,
                    propMaterial.mPUniaxial.e, propMaterial.mPUniaxial.a, propMaterial.mPUniaxial.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetOConcrete_1(PropMaterial propMaterial)
        {
            if (propMaterial.oConcrete_1 != null)
            {
                tryWrapper(() => Doc.Document.PropMaterial.SetOConcrete_1(propMaterial.name,
                    propMaterial.oConcrete_1.fc, propMaterial.oConcrete_1.isLightweight,
                    propMaterial.oConcrete_1.fcsFactor, propMaterial.oConcrete_1.sSType,
                    propMaterial.oConcrete_1.sSHysType, propMaterial.oConcrete_1.strainAtFc,
                    propMaterial.oConcrete_1.strainUltimate, propMaterial.oConcrete_1.finalSlope,
                    propMaterial.oConcrete_1.frictionAngle, propMaterial.oConcrete_1.dilatationalAngle,
                    propMaterial.oConcrete_1.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetONoDesign(PropMaterial propMaterial)
        {
            if (propMaterial.oNoDesign != null)
            {
                tryWrapper(() => Doc.Document.PropMaterial.SetONoDesign(propMaterial.name,
                    propMaterial.oNoDesign.frictionAngle, propMaterial.oNoDesign.dilatationalAngle,
                    propMaterial.oNoDesign.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetORebar_1(PropMaterial propMaterial)
        {
            if (propMaterial.oRebar_1 != null)
            {
                tryWrapper(() => Doc.Document.PropMaterial.SetORebar_1(propMaterial.name,
                    propMaterial.oRebar_1.fy, propMaterial.oRebar_1.fu, propMaterial.oRebar_1.eFy,
                    propMaterial.oRebar_1.eFu, propMaterial.oRebar_1.sSType,
                    propMaterial.oRebar_1.sSHysType, propMaterial.oRebar_1.strainAtHardening,
                    propMaterial.oRebar_1.strainUltimate, propMaterial.oRebar_1.finalSlope,
                    propMaterial.oRebar_1.useCaltransSSDefaults, propMaterial.oRebar_1.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetOSteel_1(PropMaterial propMaterial)
        {
            if (propMaterial.oSteel_1 != null)
            {
                double fy = propMaterial.oSteel_1.fy;
                double fu = propMaterial.oSteel_1.fu;
                double eFy = propMaterial.oSteel_1.eFy;
                double eFu = propMaterial.oSteel_1.eFu;
                int sSType = propMaterial.oSteel_1.sSType;
                int sSHysType = propMaterial.oSteel_1.sSHysType;
                double strainAtHardening = propMaterial.oSteel_1.strainAtHardening;
                double strainAtMaxStress = propMaterial.oSteel_1.strainAtMaxStress;
                double strainAtRupture = propMaterial.oSteel_1.strainAtRupture;
                double finalSlope = propMaterial.oSteel_1.FinalSlope;
                tryWrapper(() => Doc.Document.PropMaterial.SetOSteel_1(propMaterial.name,
                    propMaterial.oSteel_1.fy, propMaterial.oSteel_1.fu,
                    propMaterial.oSteel_1.eFy, propMaterial.oSteel_1.eFu,
                    propMaterial.oSteel_1.sSType, propMaterial.oSteel_1.sSHysType,
                    propMaterial.oSteel_1.strainAtHardening,
                    propMaterial.oSteel_1.strainAtMaxStress,
                    propMaterial.oSteel_1.strainAtRupture,
                    propMaterial.oSteel_1.FinalSlope,
                    propMaterial.oSteel_1.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetOTendon_1(PropMaterial propMaterial)
        {
            if (propMaterial.oTendon_1 != null)
            {
                tryWrapper(() => Doc.Document.PropMaterial.SetOTendon_1(propMaterial.name,
                    propMaterial.oTendon_1.fy, propMaterial.oTendon_1.fu,
                    propMaterial.oTendon_1.sSType, propMaterial.oTendon_1.sSHysType,
                    propMaterial.oTendon_1.finalSlope, propMaterial.oTendon_1.temp));
            }
            return propMaterial;
        }
        private PropMaterial SetSSCurve(PropMaterial propMaterial)
        {
            if (propMaterial.sSCurve != null)
            {
                int[] pointID = propMaterial.sSCurve.pointID.ToArray();
                double[] strain = propMaterial.sSCurve.strain.ToArray();
                double[] stress = propMaterial.sSCurve.stress.ToArray();
                tryWrapper(() => Doc.Document.PropMaterial.SetSSCurve(propMaterial.name,
                    propMaterial.sSCurve.numberPoints, ref pointID, ref strain, ref stress));
            }
            return propMaterial;
        }
        private PropMaterial SetTemp(PropMaterial propMaterial)
        {
            if (propMaterial.temp != null)
            {
                double[] temp = propMaterial.temp.temp.ToArray();
                tryWrapper(() => Doc.Document.PropMaterial.SetTemp(propMaterial.name,
                    propMaterial.temp.numberItems, ref temp));
            }
            return propMaterial;
        }
        private PropMaterial SetWeightAndMass(PropMaterial propMaterial)
        {
            if (propMaterial.weightAndMass != null)
            {
                if (propMaterial.weightAndMass.w != null)
                {
                    tryWrapper(() => Doc.Document.PropMaterial.SetWeightAndMass(propMaterial.name,
                    1, (double)propMaterial.weightAndMass.w, propMaterial.weightAndMass.temp));
                }
                if (propMaterial.weightAndMass.m != null)
                {
                    tryWrapper(() => Doc.Document.PropMaterial.SetWeightAndMass(propMaterial.name,
                    2, (double)propMaterial.weightAndMass.m, propMaterial.weightAndMass.temp));
                }
            }
            return propMaterial;
        }

        #endregion
        public object PropMaterialToNative(PropMaterial propMaterial)
        {
            bool isCreated = false;
            (propMaterial, isCreated) = SetMaterial(propMaterial);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native PropMaterial ${propMaterial.name}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            var funcList = new List<Func<PropMaterial, PropMaterial>>() { SetDamping,
                SetMassSource, SetMPAnisotropic, SetMPIsotropic, SetMPOrthotropic, SetMPUniaxial,
                SetOConcrete_1, SetONoDesign, SetORebar_1, SetOSteel_1, SetOTendon_1,
                SetSSCurve, SetTemp, SetWeightAndMass};
            foreach (var fun in funcList)
            {
                propMaterial = fun(propMaterial);
            }
            return propMaterial.name;
        }
        #region PropMaterialToSpeckle util functions

        private PropMaterial GetDamping(PropMaterial propMaterial)
        {
            var damping = new Damping();
            double modalRatio = 0, viscousMassCoeff = 0, viscousStiffCoeff = 0;
            double hystereticMassCoeff = 0, hystereticStiffCoeff = 0, Temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetDamping(propMaterial.name,
                ref modalRatio, ref viscousMassCoeff, ref viscousStiffCoeff, ref hystereticMassCoeff,
                ref hystereticStiffCoeff, Temp)))
            {
                propMaterial.damping = damping;
                propMaterial.damping.modalRatio = modalRatio;
                propMaterial.damping.viscousMassCoeff = viscousMassCoeff;
                propMaterial.damping.viscousStiffCoeff = viscousStiffCoeff;
                propMaterial.damping.hystereticMassCoeff = hystereticMassCoeff;
                propMaterial.damping.hystereticStiffCoeff = hystereticStiffCoeff;
            }
            return propMaterial;
        }
        private PropMaterial GetMassSource_1(PropMaterial propMaterial)
        {
            var massSource_1 = new MassSource_1();
            bool IncludeElements = false, IncludeAddedMass = false, IncludeLoads = false;
            int numberLoads = 0; string[] loadPat = { };
            double[] sf = { };
            if (tryWrapper(() => Doc.Document.PropMaterial.GetMassSource_1(
                ref IncludeElements, ref IncludeAddedMass, ref IncludeLoads, ref numberLoads,
                ref loadPat, ref sf)))
            {
                propMaterial.massSource_1 = massSource_1;
                propMaterial.massSource_1.IncludeElements = IncludeElements;
                propMaterial.massSource_1.IncludeAddedMass = IncludeAddedMass;
                propMaterial.massSource_1.IncludeLoads = IncludeLoads;
                if (loadPat.Length > 0)
                {
                    foreach (var pat in loadPat.ToList())
                    {
                        propMaterial.massSource_1.loadPat.Add(pat);
                    }
                }
                if (sf.Length > 0)
                {
                    foreach (var item in sf.ToList())
                    {
                        propMaterial.massSource_1.sF.Add(item);
                    }
                }

            }
            return propMaterial;
        }

        private PropMaterial GetOSteel_1(PropMaterial propMaterial, int type)
        {
            var oSteel_1 = new OSteel_1();
            double fy = 0, fu = 0, eFy = 0, eFu = 0, strainAtHardening = 0, strainAtMaxStress = 0;
            double strainAtRupture = 0, FinalSlope = 0, temp = 0;
            int sSType = 0, sSHysType = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetOSteel_1(propMaterial.name,
                ref fy, ref fu, ref eFy, ref eFu, ref sSType, ref sSHysType, ref strainAtHardening,
                ref strainAtMaxStress, ref strainAtRupture, ref FinalSlope, temp)))
            {
                propMaterial.oSteel_1 = oSteel_1;
                propMaterial.matType = type; // steel
                propMaterial.oSteel_1.fy = fy; propMaterial.oSteel_1.fu = fu;
                propMaterial.oSteel_1.eFy = eFy; propMaterial.oSteel_1.eFu = eFu;
                propMaterial.oSteel_1.sSType = sSType;
                propMaterial.oSteel_1.sSHysType = sSHysType;
                propMaterial.oSteel_1.strainAtHardening = strainAtHardening;
                propMaterial.oSteel_1.strainAtMaxStress = strainAtMaxStress;
                propMaterial.oSteel_1.strainAtRupture = strainAtRupture;
                propMaterial.oSteel_1.FinalSlope = FinalSlope;
                propMaterial.oSteel_1.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetOConcrete_1(PropMaterial propMaterial, int type)
        {
            var oConcrete_1 = new OConcrete_1();
            bool isLightweight = false; int sSType = 0, sSHysType = 0;
            double fc = 0, fcsFactor = 0, strainAtFc = 0, strainUltimate = 0, finalSlope = 0;
            double frictionAngle = 0, dilatationalAngle = 0, temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetOConcrete_1(propMaterial.name,
                ref fc, ref isLightweight, ref fcsFactor, ref sSType, ref sSHysType,
                ref strainAtFc, ref strainUltimate, ref finalSlope, ref frictionAngle,
                ref dilatationalAngle, temp)))
            {
                propMaterial.oConcrete_1 = oConcrete_1;
                propMaterial.matType = type;
                propMaterial.oConcrete_1.fc = fc; propMaterial.oConcrete_1.isLightweight = isLightweight;
                propMaterial.oConcrete_1.fcsFactor = fcsFactor;
                propMaterial.oConcrete_1.sSType = sSType;
                propMaterial.oConcrete_1.sSHysType = sSHysType;
                propMaterial.oConcrete_1.strainAtFc = strainAtFc;
                propMaterial.oConcrete_1.strainUltimate = strainUltimate;
                propMaterial.oConcrete_1.finalSlope = finalSlope;
                propMaterial.oConcrete_1.frictionAngle = frictionAngle;
                propMaterial.oConcrete_1.dilatationalAngle = dilatationalAngle;
                propMaterial.oConcrete_1.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetONoDesign(PropMaterial propMaterial)
        {
            var oNoDesign = new ONoDesign();
            double frictionAngle = 0, dilatationalAngle = 0, temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetONoDesign(propMaterial.name,
                ref frictionAngle, ref dilatationalAngle, temp)))
            {
                propMaterial.oNoDesign = oNoDesign;
                propMaterial.oNoDesign.frictionAngle = frictionAngle;
                propMaterial.oNoDesign.dilatationalAngle = dilatationalAngle;
                propMaterial.oNoDesign.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetORebar_1(PropMaterial propMaterial)
        {
            var oRebar_1 = new ORebar_1();
            double fy = 0, fu = 0, eFy = 0, eFu = 0, strainAtHardening = 0, strainUltimate = 0;
            double FinalSlope = 0, temp = 0;
            int sSType = 0, sSHysType = 0; bool useCaltransSSDefaults = false;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetORebar_1(propMaterial.name,
                ref fy, ref fu, ref eFy, ref eFu, ref sSType, ref sSHysType,
                ref strainAtHardening, ref strainUltimate, ref FinalSlope,
                ref useCaltransSSDefaults, temp)))
            {
                propMaterial.oRebar_1 = oRebar_1; propMaterial.oRebar_1.fy = fy;
                propMaterial.oRebar_1.fu = fu; propMaterial.oRebar_1.eFy = eFy;
                propMaterial.oRebar_1.eFu = eFu;
                propMaterial.oRebar_1.sSType = sSType;
                propMaterial.oRebar_1.sSHysType = sSHysType;
                propMaterial.oRebar_1.strainAtHardening = strainAtHardening;
                propMaterial.oRebar_1.strainUltimate = strainUltimate;
                propMaterial.oRebar_1.finalSlope = FinalSlope;
                propMaterial.oRebar_1.useCaltransSSDefaults = useCaltransSSDefaults;
                propMaterial.oRebar_1.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetOTendon_1(PropMaterial propMaterial)
        {
            var oTendon_1 = new OTendon_1();
            double fy = 0, fu = 0, FinalSlope = 0, temp = 0;
            int sSType = 0, sSHysType = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetOTendon_1(propMaterial.name,
                ref fy, ref fu, ref sSType, ref sSHysType, ref FinalSlope, temp)))
            {
                propMaterial.oTendon_1 = oTendon_1; propMaterial.oTendon_1.fy = fy;
                propMaterial.oTendon_1.fu = fu; propMaterial.oTendon_1.sSType = sSType;
                propMaterial.oTendon_1.sSHysType = sSHysType;
                propMaterial.oTendon_1.finalSlope = FinalSlope;
                propMaterial.oTendon_1.temp = temp;
            }
            return propMaterial;
        }

        private PropMaterial GetMaterialInfo(PropMaterial propMaterial)
        {
            var type = new ET.eMatType();
            int color = 0; string notes = ""; string guid = "";
            if (tryWrapper(() => Doc.Document.PropMaterial.GetMaterial(propMaterial.name,
                ref type, ref color, ref notes, ref guid)))
            {
                switch ((int)type)
                {
                    case 1:
                        // steel
                        return GetOSteel_1(propMaterial, (int)type);
                    case 2:
                        // concrete
                        return GetOConcrete_1(propMaterial, (int)type);
                    case 3:
                        // NoDesign
                        return GetONoDesign(propMaterial);
                    case 4:
                        // Aluminum
                        // etabs doesn't have special get/set methods for aluminum, so using steel
                        return GetOSteel_1(propMaterial, (int)type);
                    case 5:
                        // ColdFormed
                        // etabs doesn't have special get/set methods for coldformed, so using steel
                        return GetOSteel_1(propMaterial, (int)type);
                    case 6:
                        // Rebar
                        return GetORebar_1(propMaterial);
                    case 7:
                        // Tendon
                        return GetOTendon_1(propMaterial);
                    case 8:
                        // masonry
                        // etabs doesn't have special get/set methods for masonry, using concrete method for it
                        return GetOConcrete_1(propMaterial, (int)type);
                    default:
                        ConversionErrors.Add(new SpeckleException($"Unsupported material property named ${propMaterial.name}" +
                            $"with type ${propMaterial.matType}", level: Sentry.SentryLevel.Warning));
                        return propMaterial;
                }
            }
            return propMaterial;
        }
        private PropMaterial GetMPAnisotropic(PropMaterial propMaterial)
        {
            var mPAnisotropic = new MPAnisotropic();
            double[] e = { }, u = { }, a = { }, g = { }; double temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetMPAnisotropic(propMaterial.name,
                ref e, ref u, ref a, ref g, temp)))
            {
                propMaterial.mPAnisotropic = mPAnisotropic;
                propMaterial.mPAnisotropic.e = e.ToList();
                propMaterial.mPAnisotropic.u = u.ToList();
                propMaterial.mPAnisotropic.a = a.ToList();
                propMaterial.mPAnisotropic.g = g.ToList();
                propMaterial.mPAnisotropic.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetMPIsotropic(PropMaterial propMaterial)
        {
            var mPIsotropic = new MPIsotropic();
            double e = 0, u = 0, a = 0, g = 0, temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetMPIsotropic(propMaterial.name,
                ref e, ref u, ref a, ref g, temp)))
            {
                propMaterial.mPIsotropic = mPIsotropic;
                propMaterial.mPIsotropic.e = e; propMaterial.mPIsotropic.u = u;
                propMaterial.mPIsotropic.a = a; propMaterial.mPIsotropic.g = g;
                propMaterial.mPIsotropic.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetMPOrthotropic(PropMaterial propMaterial)
        {
            var mPOrthotropic = new MPOrthotropic();
            double[] e = { }, u = { }, a = { }, g = { };
            double temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetMPOrthotropic(propMaterial.name,
                ref e, ref u, ref a, ref g, temp)))
            {
                propMaterial.mPOrthotropic = mPOrthotropic;
                propMaterial.mPOrthotropic.e = e.ToList();
                propMaterial.mPOrthotropic.u = u.ToList();
                propMaterial.mPOrthotropic.a = a.ToList();
                propMaterial.mPOrthotropic.g = g.ToList();
                propMaterial.mPOrthotropic.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetMPUniaxial(PropMaterial propMaterial)
        {
            var mPUniaxial = new MPUniaxial();
            double e = 0, a = 0, temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetMPUniaxial(propMaterial.name,
                ref e, ref a, temp)))
            {
                propMaterial.mPUniaxial = mPUniaxial;
                propMaterial.mPUniaxial.e = e; propMaterial.mPUniaxial.a = a;
                propMaterial.mPUniaxial.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetSSCurve(PropMaterial propMaterial)
        {
            var sSCurve = new SSCurve();
            int numberPoints = 0; int[] pointID = { }; double[] strain = { }, stress = { };
            string sectName = ""; double rebarArea = 0, temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetSSCurve(propMaterial.name,
                ref numberPoints, ref pointID, ref strain, ref stress, sectName, rebarArea, temp)))
            {
                propMaterial.sSCurve = sSCurve;
                propMaterial.sSCurve.numberPoints = numberPoints;
                propMaterial.sSCurve.pointID = pointID.ToList();
                propMaterial.sSCurve.strain = strain.ToList();
                propMaterial.sSCurve.stress = stress.ToList();
                propMaterial.sSCurve.sectName = sectName;
                propMaterial.sSCurve.rebarArea = rebarArea;
                propMaterial.sSCurve.temp = temp;
            }
            return propMaterial;
        }
        private PropMaterial GetTemp(PropMaterial propMaterial)
        {
            var temp = new Temp();
            int numberItems = 0; double[] temps = { };
            if (tryWrapper(() => Doc.Document.PropMaterial.GetTemp(propMaterial.name,
                ref numberItems, ref temps)))
            {
                propMaterial.temp = temp;
                propMaterial.temp.numberItems = numberItems;
                propMaterial.temp.temp = temps.ToList();
            }
            return propMaterial;
        }
        private PropMaterial GetWeightAndMass(PropMaterial propMaterial)
        {
            var weightAndMass = new WeightAndMass();
            double w = 0, m = 0, temp = 0;
            if (tryWrapper(() => Doc.Document.PropMaterial.GetWeightAndMass(propMaterial.name,
                ref w, ref m, temp)))
            {
                propMaterial.weightAndMass = weightAndMass;
                propMaterial.weightAndMass.w = w; propMaterial.weightAndMass.m = m;
                propMaterial.weightAndMass.temp = temp;
            }
            return propMaterial;
        }

        #endregion
        public PropMaterial PropMaterialToSpeckle(string name)
        {
            var propMaterial = new PropMaterial { name = name };
            var funcList = new List<Func<PropMaterial, PropMaterial>>() { GetMassSource_1, GetDamping,
                GetMaterialInfo, GetMPAnisotropic, GetMPIsotropic, GetMPOrthotropic, GetMPUniaxial,
                GetSSCurve, GetTemp, GetWeightAndMass};
            foreach (var fun in funcList)
            {
                propMaterial = fun(propMaterial);
            }
            return propMaterial;
        }

    }
}

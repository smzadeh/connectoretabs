﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region SpandrelLabelToNative util functions

        private SpandrelLabel SetSpandrelLabel(SpandrelLabel spandrelLabel)
        {
            if (!tryWrapper(() => Doc.Document.SpandrelLabel.SetSpandrel(spandrelLabel.name,
                spandrelLabel.isMultiStory)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native spandrelLabel ${spandrelLabel.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return spandrelLabel;
        }
        #endregion
        public object SpandrelLabelToNative(SpandrelLabel spandrelLabel)
        {
            spandrelLabel = SetSpandrelLabel(spandrelLabel);
            return spandrelLabel.name;
        }

        #region SpandrelLabelToSpeckle util functions

        private SpandrelLabel GetSpandrelLabel(SpandrelLabel spandrelLabel)
        {
            bool isMultiStory = false;
            if (tryWrapper(() => Doc.Document.SpandrelLabel.GetSpandrel(spandrelLabel.name,
                ref isMultiStory)))
            {
                spandrelLabel.isMultiStory = isMultiStory;
            }
            return spandrelLabel;
        }
        #endregion
        public SpandrelLabel SpandrelLabelToSpeckle(string name)
        {
            var spandrelLabel = new SpandrelLabel { name = name };
            spandrelLabel = GetSpandrelLabel(spandrelLabel);
            return spandrelLabel;
        }
    }
}

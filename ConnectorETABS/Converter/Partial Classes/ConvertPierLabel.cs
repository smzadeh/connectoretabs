﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PierLabelToNative util functions

        private PierLabel SetPierLabel(PierLabel pierLabel)
        {
            if (!tryWrapper(() => Doc.Document.PierLabel.SetPier(pierLabel.name)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native pierLabel ${pierLabel.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return pierLabel;
        }
        #endregion
        public object PierLabelToNative(PierLabel pierLabel)
        {
            pierLabel = SetPierLabel(pierLabel);
            return pierLabel.name;
        }

        #region PierLabelToSpeckle util functions

        private PierLabel GetPierLabel(PierLabel pierLabel)
        {
            if (tryWrapper(() => Doc.Document.PierLabel.GetPier(pierLabel.name)))
            {
                //just don't do anything
            }
            return pierLabel;
        }
        #endregion
        public PierLabel PierLabelToSpeckle(string name)
        {
            var pierLabel = new PierLabel { name = name };
            pierLabel = GetPierLabel(pierLabel);
            return pierLabel;
        }
    }
}

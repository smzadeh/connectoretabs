﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region DesignConcreteToNative util functions

        private (DesignConcrete, bool) SetDesignConcreteCodeName(DesignConcrete designConcrete)
        {
            if (!tryWrapper(() => Doc.Document.DesignConcrete.SetCode(designConcrete.codeName)))
            {
                return (designConcrete, false);
            }
            return (designConcrete, true);
        }

        private int ACI318_14_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.ACI318_14.SetPreference(prefItem, prefValue);
        }
        private int ACI318_14_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.ACI318_14.SetOverwrite(frameName, item, value);
        }
        private int ACI318_08_IBC2009_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.ACI318_08_IBC2009.SetPreference(prefItem, prefValue);
        }
        private int ACI318_08_IBC2009_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.ACI318_08_IBC2009.SetOverwrite(frameName, item, value);
        }
        private int ACI318_19_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.ACI318_19.SetPreference(prefItem, prefValue);
        }
        private int ACI318_19_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.ACI318_19.SetOverwrite(frameName, item, value);
        }
        private int AS_3600_09_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.AS_3600_09.SetPreference(prefItem, prefValue);
        }
        private int AS_3600_09_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.AS_3600_09.SetOverwrite(frameName, item, value);
        }
        private int AS_3600_2018_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.AS_3600_2018.SetPreference(prefItem, prefValue);
        }
        private int AS_3600_2018_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.AS_3600_2018.SetOverwrite(frameName, item, value);
        }
        private int BS8110_97_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.BS8110_97.SetPreference(prefItem, prefValue);
        }
        private int BS8110_97_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.BS8110_97.SetOverwrite(frameName, item, value);
        }
        private int Chinese_2010_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Chinese_2010.SetPreference(prefItem, prefValue);
        }
        private int Chinese_2010_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.Chinese_2010.SetOverwrite(frameName, item, value);
        }
        private int Indian_IS_456_2000_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Indian_IS_456_2000.SetPreference(prefItem, prefValue);
        }
        private int Indian_IS_456_2000_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.Indian_IS_456_2000.SetOverwrite(frameName, item, value);
        }
        private int Mexican_RCDF_2017_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Mexican_RCDF_2017.SetPreference(prefItem, prefValue);
        }
        private int Mexican_RCDF_2017_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.Mexican_RCDF_2017.SetOverwrite(frameName, item, value);
        }
        private int TS_500_2000_R2018_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.TS_500_2000_R2018.SetPreference(prefItem, prefValue);
        }
        private int TS_500_2000_R2018_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.TS_500_2000_R2018.SetOverwrite(frameName, item, value);
        }
        private int SP63_13330_2012_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.SP63_13330_2012.SetPreference(prefItem, prefValue);
        }
        private int SP63_13330_2012_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.SP63_13330_2012.SetOverwrite(frameName, item, value);
        }
        private int Eurocode_2_2004_SetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Eurocode_2_2004.SetPreference(prefItem, prefValue);
        }
        private int Eurocode_2_2004_SetConcreteOverwrite(string frameName, int item, double value)
        {
            return Doc.Document.DesignConcrete.Eurocode_2_2004.SetOverwrite(frameName, item, value);
        }
        private DesignConcrete SetPrefOverWrite(DesignConcrete designConcrete,
            Func<int, double, int> setPreferece, Func<string, int, double, int> setOverWrite)
        {
            if (designConcrete.preferenceItem.Count > 0)
            {
                for (int index = 0; index < designConcrete.preferenceItem.Count; index++)
                {
                    tryWrapper(() => setPreferece(
                        designConcrete.preferenceItem[index], designConcrete.preferenceValue[index]));
                }
            }
            for (int index = 0; index < designConcrete.overWrites.Count; index++)
            {

                for (int itemsIndex = 0; itemsIndex < designConcrete.overWrites[index].items.Count;
                    itemsIndex++)
                {
                    tryWrapper(() => setOverWrite(
                        designConcrete.overWrites[index].frameName,
                        designConcrete.overWrites[index].items[itemsIndex],
                        designConcrete.overWrites[index].values[itemsIndex]));
                }

            }
            return designConcrete;
        }

        private DesignConcrete SetDesignConcretePrefOversWrites(DesignConcrete designConcrete)
        {
            switch (designConcrete.codeName)
            {
                case "ACI318_14":
                    return SetPrefOverWrite(designConcrete,
                        ACI318_14_SetConcretePreference, ACI318_14_SetConcreteOverwrite);
                case "ACI318_08_IBC2009":
                    return SetPrefOverWrite(designConcrete,
                        ACI318_08_IBC2009_SetConcretePreference, ACI318_08_IBC2009_SetConcreteOverwrite);
                case "ACI318_19":
                    return SetPrefOverWrite(designConcrete,
                        ACI318_19_SetConcretePreference, ACI318_19_SetConcreteOverwrite);
                case "AS_3600_09":
                    return SetPrefOverWrite(designConcrete,
                        AS_3600_09_SetConcretePreference, AS_3600_09_SetConcreteOverwrite);
                case "AS_3600_2018":
                    return SetPrefOverWrite(designConcrete,
                        AS_3600_2018_SetConcretePreference, AS_3600_2018_SetConcreteOverwrite);
                case "BS8110_97":
                    return SetPrefOverWrite(designConcrete,
                        BS8110_97_SetConcretePreference, BS8110_97_SetConcreteOverwrite);
                case "Chinese_2010":
                    return SetPrefOverWrite(designConcrete,
                        Chinese_2010_SetConcretePreference, Chinese_2010_SetConcreteOverwrite);
                case "Indian_IS_456_2000":
                    return SetPrefOverWrite(designConcrete,
                        Indian_IS_456_2000_SetConcretePreference, Indian_IS_456_2000_SetConcreteOverwrite);
                case "Mexican_RCDF_2017":
                    return SetPrefOverWrite(designConcrete,
                        Mexican_RCDF_2017_SetConcretePreference, Mexican_RCDF_2017_SetConcreteOverwrite);
                case "SP63_13330_2012":
                    return SetPrefOverWrite(designConcrete,
                        SP63_13330_2012_SetConcretePreference, SP63_13330_2012_SetConcreteOverwrite);
                case "TS_500_2000_R2018":
                    return SetPrefOverWrite(designConcrete,
                        TS_500_2000_R2018_SetConcretePreference, TS_500_2000_R2018_SetConcreteOverwrite);
                case "Eurocode_2_2004":
                    return SetPrefOverWrite(designConcrete,
                        Eurocode_2_2004_SetConcretePreference, Eurocode_2_2004_SetConcreteOverwrite);
                default:
                    ConversionErrors.Add(new SpeckleException($"Unsupported design concrete code named {designConcrete.codeName}",
                    level: Sentry.SentryLevel.Warning));
                    return designConcrete;
            }
        }

        #endregion
        public object DesignConcreteToNative(DesignConcrete designConcrete)
        {
            bool isCreated = false;
            (designConcrete, isCreated) = SetDesignConcreteCodeName(designConcrete);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native designConcrete ${designConcrete.codeName}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            designConcrete = SetDesignConcretePrefOversWrites(designConcrete);
            return designConcrete.codeName;
        }
        #region DesignConcreteToSpeckle util functions

        private DesignConcrete GetDesignConcreteCodeName(DesignConcrete designConcrete)
        {
            var codeName = "";
            if (tryWrapper(() => Doc.Document.DesignConcrete.GetCode(ref codeName)))
            {
                designConcrete.codeName = codeName;
            }
            return designConcrete;
        }

        private int ACI318_14_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.ACI318_14.GetPreference(prefItem, ref prefValue);
        }
        private int ACI318_14_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.ACI318_14.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int ACI318_08_IBC2009_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.ACI318_08_IBC2009.GetPreference(prefItem, ref prefValue);
        }
        private int ACI318_08_IBC2009_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.ACI318_08_IBC2009.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int ACI318_19_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.ACI318_19.GetPreference(prefItem, ref prefValue);
        }
        private int ACI318_19_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.ACI318_19.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int AS_3600_09_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.AS_3600_09.GetPreference(prefItem, ref prefValue);
        }
        private int AS_3600_09_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.AS_3600_09.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int AS_3600_2018_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.AS_3600_2018.GetPreference(prefItem, ref prefValue);
        }
        private int AS_3600_2018_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.AS_3600_2018.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int BS8110_97_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.BS8110_97.GetPreference(prefItem, ref prefValue);
        }
        private int BS8110_97_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.BS8110_97.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Chinese_2010_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Chinese_2010.GetPreference(prefItem, ref prefValue);
        }
        private int Chinese_2010_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.Chinese_2010.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Indian_IS_456_2000_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Indian_IS_456_2000.GetPreference(prefItem, ref prefValue);
        }
        private int Indian_IS_456_2000_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.Indian_IS_456_2000.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Mexican_RCDF_2017_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Mexican_RCDF_2017.GetPreference(prefItem, ref prefValue);
        }
        private int Mexican_RCDF_2017_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.Mexican_RCDF_2017.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int TS_500_2000_R2018_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.TS_500_2000_R2018.GetPreference(prefItem, ref prefValue);
        }
        private int TS_500_2000_R2018_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.TS_500_2000_R2018.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int SP63_13330_2012_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.SP63_13330_2012.GetPreference(prefItem, ref prefValue);
        }
        private int SP63_13330_2012_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.SP63_13330_2012.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Eurocode_2_2004_GetConcretePreference(int prefItem, double prefValue)
        {
            return Doc.Document.DesignConcrete.Eurocode_2_2004.GetPreference(prefItem, ref prefValue);
        }
        private int Eurocode_2_2004_GetConcreteOverwrite(string frameName, int item, double value, bool progDet)
        {
            return Doc.Document.DesignConcrete.Eurocode_2_2004.GetOverwrite(frameName, item, ref value, ref progDet);
        }

        private DesignConcrete GetPrefOverWrite(DesignConcrete designConcrete,
            Func<int, double, int> getPreference, Func<string, int, double, bool, int> getOverWrite)
        {
            designConcrete.preferenceItem = new List<int>();
            designConcrete.preferenceValue = new List<double>();
            for (int itemNumber = 0; itemNumber < 200; itemNumber++)
            {
                double value = 0;
                if (tryWrapper(() => getPreference(itemNumber, value)))
                {
                    designConcrete.preferenceItem.Add(itemNumber);
                    designConcrete.preferenceValue.Add(value);
                }
                else
                {
                    break;
                }
            }
            List<string> frameNames = ConnectorETABS.ConnectorETABSUtils.GetAllFrameNames(Doc);
            designConcrete.overWrites = new List<OverWrite>();
            for (int index = 0; index < frameNames.Count; index++)
            {
                var overWrite = new OverWrite();
                for (int itemNumber = 0; itemNumber < 200; itemNumber++)
                {
                    double value = 0; bool progDet = false;
                    if (tryWrapper(() => getOverWrite(frameNames[index], itemNumber, value, progDet)))
                    {
                        overWrite.frameName = frameNames[index];
                        overWrite.items.Add(itemNumber);
                        overWrite.values.Add(value);
                    }
                    else
                    {
                        designConcrete.overWrites.Add(overWrite);
                        break;
                    }
                }
            }
            return designConcrete;
        }

        private DesignConcrete GetDesignConcretePrefsOverWrites(DesignConcrete designConcrete)
        {
            switch (designConcrete.codeName)
            {
                case "ACI318_14":
                    return GetPrefOverWrite(designConcrete,
                        ACI318_14_GetConcretePreference, ACI318_14_GetConcreteOverwrite);
                case "ACI318_08_IBC2009":
                    return GetPrefOverWrite(designConcrete,
                        ACI318_08_IBC2009_GetConcretePreference, ACI318_08_IBC2009_GetConcreteOverwrite);
                case "ACI318_19":
                    return GetPrefOverWrite(designConcrete,
                        ACI318_19_GetConcretePreference, ACI318_19_GetConcreteOverwrite);
                case "AS_3600_09":
                    return GetPrefOverWrite(designConcrete,
                        AS_3600_09_GetConcretePreference, AS_3600_09_GetConcreteOverwrite);
                case "AS_3600_2018":
                    return GetPrefOverWrite(designConcrete,
                        AS_3600_2018_GetConcretePreference, AS_3600_2018_GetConcreteOverwrite);
                case "BS8110_97":
                    return GetPrefOverWrite(designConcrete,
                        BS8110_97_GetConcretePreference, BS8110_97_GetConcreteOverwrite);
                case "Chinese_2010":
                    return GetPrefOverWrite(designConcrete,
                        Chinese_2010_GetConcretePreference, Chinese_2010_GetConcreteOverwrite);
                case "Indian_IS_456_2000":
                    return GetPrefOverWrite(designConcrete,
                        Indian_IS_456_2000_GetConcretePreference, Indian_IS_456_2000_GetConcreteOverwrite);
                case "Mexican_RCDF_2017":
                    return GetPrefOverWrite(designConcrete,
                        Mexican_RCDF_2017_GetConcretePreference, Mexican_RCDF_2017_GetConcreteOverwrite);
                case "SP63_13330_2012":
                    return GetPrefOverWrite(designConcrete,
                        SP63_13330_2012_GetConcretePreference, SP63_13330_2012_GetConcreteOverwrite);
                case "TS_500_2000_R2018":
                    return GetPrefOverWrite(designConcrete,
                        TS_500_2000_R2018_GetConcretePreference, TS_500_2000_R2018_GetConcreteOverwrite);
                default:
                    ConversionErrors.Add(new SpeckleException($"Unsupported design concrete code named {designConcrete.codeName}",
                    level: Sentry.SentryLevel.Warning));
                    return designConcrete;
            }
        }

        #endregion
        public DesignConcrete DesignConcreteToSpeckle(string name)
        {
            var designConcrete = new DesignConcrete { codeName = name };
            designConcrete = GetDesignConcreteCodeName(designConcrete);
            designConcrete = GetDesignConcretePrefsOverWrites(designConcrete);
            return designConcrete;
        }
    }
}

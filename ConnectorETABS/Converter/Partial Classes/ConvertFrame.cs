﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using ConnectorETABS;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region FrameToNative util functions

        private bool specklePropFrameNameExistsInModel(Frame speckleFrame)
        {
            var names = ConnectorETABSUtils.GetAllPropFrameNames(Doc);
            return names.Contains(speckleFrame.section.name);
        }
        /// <summary>
        /// Creates a frame with given end points and section.
        /// If end points or section exists, their fields are merged.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        private (Frame, bool) AddByPoint(Frame frame)
        {
            string frameName = frame.name;
            ConvertToNative(frame.point1);
            ConvertToNative(frame.point2);
            ConvertToNative(frame.section);
            if (!tryWrapper(() => Doc.Document.FrameObj.AddByPoint(frame.point1.name,
                frame.point2.name, ref frameName, frame.section.name)))
            {
                return (frame, false);
            }
            frame.name = frameName;
            return (frame, true);
        }
        private Frame SetColumnSpliceOverwrite(Frame frame)
        {
            if (frame.columnSpliceOverwrite != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetColumnSpliceOverwrite(frame.name,
                    frame.columnSpliceOverwrite.spliceOption, frame.columnSpliceOverwrite.height));
            }
            return frame;
        }
        private Frame SetDesignProcedure(Frame frame)
        {
            if (frame.designProcedure != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetDesignProcedure(frame.name,
                    frame.designProcedure.designProcedure));
            }
            return frame;
        }
        private Frame SetEndLengthOffset(Frame frame)
        {
            if (frame.endLengthOffset != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetEndLengthOffset(frame.name,
                    frame.endLengthOffset.autoOffset, frame.endLengthOffset.length1,
                    frame.endLengthOffset.length2, frame.endLengthOffset.rz));
            }
            return frame;
        }
        private Frame SetGroupAssign(Frame frame)
        {
            if (frame.groupAssign != null)
            {
                foreach (var group in frame.groupAssign.groupAssign)
                {
                    tryWrapper(() => Doc.Document.FrameObj.SetGroupAssign(frame.name, group));
                }
            }
            return frame;
        }
        private Frame SetInsertionPoint(Frame frame)
        {
            if (frame.insertionPoint != null)
            {
                double[] offset1 = frame.insertionPoint.offset1.ToArray();
                double[] offset2 = frame.insertionPoint.offset2.ToArray();
                tryWrapper(() => Doc.Document.FrameObj.SetInsertionPoint(frame.name,
                    frame.insertionPoint.cardinalPoint, frame.insertionPoint.mirror2,
                    frame.insertionPoint.stiffTransform, ref offset1, ref offset2));
            }
            return frame;
        }
        private Frame SetLateralBracing(Frame frame)
        {
            if (frame.lateralBracing != null)
            {
                for (int index = 0; index < frame.lateralBracing.numberItems; index++)
                {
                    tryWrapper(() => Doc.Document.FrameObj.SetLateralBracing(frame.name,
                        frame.lateralBracing.myType[index], frame.lateralBracing.Loc[index],
                        frame.lateralBracing.dist1[index], frame.lateralBracing.dist2[index]));
                }
            }
            return frame;
        }
        private Frame SetLoadDistributed(Frame frame)
        {
            if (frame.loadDistributed != null)
            {
                for (int index = 0; index < frame.loadDistributed.numberItems; index++)
                {
                    LoadPatternToNative(frame.loadDistributed.loadPat[index]);
                    tryWrapper(() => Doc.Document.FrameObj.SetLoadDistributed(
                        frame.name, frame.loadDistributed.loadPat[index].name,
                        frame.loadDistributed.myType[index], frame.loadDistributed.dir[index],
                        frame.loadDistributed.dist1[index], frame.loadDistributed.dist2[index],
                        frame.loadDistributed.val1[index], frame.loadDistributed.val2[index]));
                }
            }
            return frame;
        }
        private Frame SetLoadPoint(Frame frame)
        {
            if (frame.loadPoint != null)
            {
                for (int index = 0; index < frame.loadPoint.numberItems; index++)
                {
                    LoadPatternToNative(frame.loadPoint.loadPat[index]);
                    tryWrapper(() => Doc.Document.FrameObj.SetLoadPoint(frame.name,
                        frame.loadPoint.loadPat[index].name, frame.loadPoint.myType[index],
                        frame.loadPoint.dir[index], frame.loadPoint.dist[index],
                        frame.loadPoint.val[index]));
                }
            }
            return frame;
        }
        private Frame SetLoadTemperature(Frame frame)
        {
            if (frame.loadTemperature != null)
            {
                for (int index = 0; index < frame.loadTemperature.numberItems; index++)
                {
                    LoadPatternToNative(frame.loadTemperature.loadPat[index]);
                    tryWrapper(() => Doc.Document.FrameObj.SetLoadTemperature(frame.name,
                        frame.loadTemperature.loadPat[index].name, frame.loadTemperature.myType[index],
                        frame.loadTemperature.val[index]));
                }
            }
            return frame;
        }
        private Frame SetLocalAxes(Frame frame)
        {
            if (frame.localAxes != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetLocalAxes(frame.name,
                    frame.localAxes.ang));
            }
            return frame;
        }
        private Frame SetMass(Frame frame)
        {
            if (frame.mass != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetMass(frame.name,
                    (double)frame.mass));
            }
            return frame;
        }
        private Frame SetMaterialOverwrite(Frame frame)
        {
            if (frame.materialOverwrite != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetMaterialOverwrite(frame.name,
                    frame.materialOverwrite.propName));
            }
            return frame;
        }
        private Frame SetModifiers(Frame frame)
        {
            if (frame.modifiers != null)
            {
                double[] mod = frame.modifiers.value.ToArray();
                tryWrapper(() => Doc.Document.FrameObj.SetModifiers(frame.name,
                    ref mod));
            }
            return frame;
        }
        private Frame SetOutputStations(Frame frame)
        {
            if (frame.outputStations != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetOutputStations(frame.name,
                    frame.outputStations.myType, frame.outputStations.maxSegSize,
                    frame.outputStations.minSections));
            }
            return frame;
        }
        private Frame SetPier(Frame frame)
        {
            if (frame.pier != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetPier(frame.name,
                    frame.pier.pierName));
            }
            return frame;
        }
        private Frame SetReleases(Frame frame)
        {
            if (frame.releases != null)
            {
                bool[] ii = frame.releases.ii.ToArray();
                bool[] jj = frame.releases.jj.ToArray();
                double[] startVal = frame.releases.startValue.ToArray();
                double[] endValue = frame.releases.endValue.ToArray();
                tryWrapper(() => Doc.Document.FrameObj.SetReleases(frame.name,
                    ref ii, ref jj, ref startVal, ref endValue));
            }
            return frame;
        }
        private Frame SetSpandrel(Frame frame)
        {
            if (frame.spandrel != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetSpandrel(frame.name,
                    frame.spandrel.spandrelName));
            }
            return frame;
        }
        private Frame SetSpringAssignment(Frame frame)
        {
            if (frame.springAssignment != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetSpringAssignment(frame.name,
                    frame.springAssignment.springProp));
            }
            return frame;
        }
        private Frame SetTCLimits(Frame frame)
        {
            if (frame.TCLimits != null)
            {
                tryWrapper(() => Doc.Document.FrameObj.SetTCLimits(frame.name,
                    frame.TCLimits.limitCompressionExists, frame.TCLimits.limitCompression,
                    frame.TCLimits.limitTensionExists, frame.TCLimits.limitTension));
            }
            return frame;
        }
        #endregion
        public object FrameToNative(Frame frame)
        {
            bool isCreated = false;
            (frame, isCreated) = AddByPoint(frame);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native frame ${frame.name}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            var funcList = new List<Func<Frame, Frame>>(){ SetColumnSpliceOverwrite,
                    SetDesignProcedure, SetEndLengthOffset, SetGroupAssign, SetInsertionPoint,
                    SetLateralBracing, SetLoadDistributed, SetLoadPoint, SetLoadTemperature,
                    SetLocalAxes, SetMass, SetMaterialOverwrite, SetModifiers, SetOutputStations,
                    SetPier, SetReleases, SetSpandrel, SetSpringAssignment, SetTCLimits};

            foreach (var fun in funcList)
            {
                frame = fun(frame);
            }
            return frame.name;
        }

        #region FrameToSpeckle util functions

        private Frame GetPoints(Frame frame)
        {
            string point1 = "", point2 = "";
            if (tryWrapper(() => Doc.Document.FrameObj.GetPoints(frame.name, ref point1,
                ref point2)))
            {
                frame.point1 = PointToSpeckle(point1);
                frame.point2 = PointToSpeckle(point2);
            }
            return frame;
        }

        private Frame GetSection(Frame frame)
        {
            string section = "", autoSelect = "";
            if (tryWrapper(() => Doc.Document.FrameObj.GetSection(frame.name,
                ref section, ref autoSelect)))
            {
                frame.section = PropFrameToSpeckle(section);
            }
            return frame;
        }
        private Frame GetColumnSpliceOverwrite(Frame frame)
        {
            var columnSpliceOverwrite = new ColumnSpliceOverwrite();
            int SpliceOption = 0; double Height = 0;
            if (tryWrapper(() => Doc.Document.FrameObj.GetColumnSpliceOverwrite(frame.name,
                ref SpliceOption, ref Height)))
            {
                frame.columnSpliceOverwrite = columnSpliceOverwrite;
                frame.columnSpliceOverwrite.spliceOption = SpliceOption;
                frame.columnSpliceOverwrite.height = Height;
            }
            return frame;
        }
        private Frame GetDesignProcedure(Frame frame)
        {
            var designProcedure = new DesignProcedure();
            int MyType = 0;
            if (tryWrapper(() => Doc.Document.FrameObj.GetDesignProcedure(frame.name,
                ref MyType)))
            {
                frame.designProcedure = designProcedure;
                frame.designProcedure.designProcedure = MyType;
            }
            return frame;
        }
        private Frame GetEndLengthOffset(Frame frame)
        {
            var endLengthOffset = new EndLengthOffset();
            bool AutoOffset = false; double Length1 = 0, Length2 = 0, RZ = 0;
            if (tryWrapper(() => Doc.Document.FrameObj.GetEndLengthOffset(frame.name,
                ref AutoOffset, ref Length1, ref Length2, ref RZ)))
            {
                frame.endLengthOffset = endLengthOffset;
                frame.endLengthOffset.autoOffset = AutoOffset;
                frame.endLengthOffset.length1 = Length1;
                frame.endLengthOffset.length2 = Length2;
                frame.endLengthOffset.rz = RZ;
            }
            return frame;
        }
        private Frame GetGroupAssign(Frame frame)
        {
            var groupAssign = new GroupAssign();
            int numberGroups = 0; string[] Groups = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetGroupAssign(frame.name,
                ref numberGroups, ref Groups)))
            {
                if (numberGroups > 0)
                {
                    frame.groupAssign = groupAssign;
                    foreach (var group in Groups)
                    {
                        frame.groupAssign.groupAssign.Add(group);
                    }
                }
            }
            return frame;
        }
        private Frame GetInsertionPoint(Frame frame)
        {
            var insertionPoint = new InsertionPoint();
            int cardinalPoint = 0; string cSys = "";
            bool mirror2 = false, stiffTransform = false;
            double[] offset1 = { }, offset2 = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetInsertionPoint(frame.name,
                ref cardinalPoint, ref mirror2, ref stiffTransform, ref offset1, ref offset2, ref cSys)))
            {
                frame.insertionPoint = insertionPoint;
                frame.insertionPoint.cardinalPoint = cardinalPoint;
                frame.insertionPoint.mirror2 = mirror2;
                frame.insertionPoint.stiffTransform = stiffTransform;
                foreach (var offset in offset1.ToList())
                {
                    frame.insertionPoint.offset1.Add(offset);
                }
                foreach (var offset in offset2.ToList())
                {
                    frame.insertionPoint.offset2.Add(offset);
                }
                frame.insertionPoint.cSys = cSys;
            }
            return frame;
        }
        private Frame GetLateralBracing(Frame frame)
        {
            var lateralBracing = new LateralBracing();
            int numberItems = 0;
            string[] frameName = { }; int[] myType = { }, Loc = { };
            double[] RD1 = { }, RD2 = { }, Dist1 = { }, Dist2 = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetLateralBracing(frame.name,
                    ref numberItems, ref frameName, ref myType, ref Loc, ref RD1,
                    ref RD2, ref Dist1, ref Dist2)))
            {
                if (numberItems > 0)
                {
                    frame.lateralBracing = lateralBracing;
                    frame.lateralBracing.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        frame.lateralBracing.frameName.Add(frameName[index]);
                        frame.lateralBracing.myType.Add(myType[index]);
                        frame.lateralBracing.Loc.Add(Loc[index]);
                        frame.lateralBracing.rD1.Add(RD1[index]);
                        frame.lateralBracing.rD2.Add(RD2[index]);
                        frame.lateralBracing.dist1.Add(Dist1[index]);
                        frame.lateralBracing.dist2.Add(Dist2[index]);
                    }
                }
            }
            return frame;
        }
        private Frame GetLoadDistributed(Frame frame)
        {
            var loadDistributed = new LoadDistributed();
            int numberItems = 0;
            string[] frameName = { }, loadPat = { }, cSys = { }; int[] myType = { }, dir = { };
            double[] RD1 = { }, RD2 = { }, Dist1 = { }, Dist2 = { }, Val1 = { }, Val2 = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetLoadDistributed(frame.name,
                    ref numberItems, ref frameName, ref loadPat, ref myType, ref cSys, ref dir, ref RD1,
                    ref RD2, ref Dist1, ref Dist2, ref Val1, ref Val2)))
            {
                if (numberItems > 0)
                {
                    frame.loadDistributed = loadDistributed;
                    frame.loadDistributed.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        frame.loadDistributed.frameName.Add(frameName[index]);
                        frame.loadDistributed.loadPat.Add(LoadPatternToSpeckle(loadPat[index]));
                        frame.loadDistributed.myType.Add(myType[index]);
                        frame.loadDistributed.cSys.Add(cSys[index]);
                        frame.loadDistributed.dir.Add(dir[index]);
                        frame.loadDistributed.rD1.Add(RD1[index]);
                        frame.loadDistributed.rD2.Add(RD2[index]);
                        frame.loadDistributed.dist1.Add(Dist1[index]);
                        frame.loadDistributed.dist2.Add(Dist2[index]);
                        frame.loadDistributed.val1.Add(Val1[index]);
                        frame.loadDistributed.val2.Add(Val2[index]);
                    }
                }
            }
            return frame;
        }
        private Frame GetLoadPoint(Frame frame)
        {
            var loadPoint = new LoadPoint();
            int numberItems = 0;
            string[] frameName = { }, loadPat = { }, cSys = { }; int[] myType = { }, dir = { };
            double[] relDist = { }, Dist = { }, Val = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetLoadPoint(frame.name,
                    ref numberItems, ref frameName, ref loadPat, ref myType, ref cSys,
                    ref dir, ref relDist, ref Dist, ref Val)))
            {
                if (numberItems > 0)
                {
                    frame.loadPoint = loadPoint;
                    frame.loadPoint.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        frame.loadPoint.frameName.Add(frameName[index]);
                        frame.loadPoint.loadPat.Add(LoadPatternToSpeckle(loadPat[index]));
                        frame.loadPoint.myType.Add(myType[index]);
                        frame.loadPoint.cSys.Add(cSys[index]);
                        frame.loadPoint.dir.Add(dir[index]);
                        frame.loadPoint.relDist.Add(relDist[index]);
                        frame.loadPoint.dist.Add(Dist[index]);
                        frame.loadPoint.val.Add(Val[index]);
                    }
                }
            }
            return frame;
        }
        private Frame GetLoadTemperature(Frame frame)
        {
            var loadTemperature = new LoadTemperature();
            int numberItems = 0;
            string[] frameName = { }, loadPat = { }, PatternName = { };
            int[] myType = { }, dir = { };
            double[] Val = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetLoadTemperature(frame.name,
                    ref numberItems, ref frameName, ref loadPat, ref myType, ref Val,
                    ref PatternName)))
            {
                if (numberItems > 0)
                {
                    frame.loadTemperature = loadTemperature;
                    frame.loadTemperature.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        frame.loadTemperature.name.Add(frameName[index]);
                        frame.loadTemperature.loadPat.Add(LoadPatternToSpeckle(loadPat[index]));
                        frame.loadTemperature.myType.Add(myType[index]);
                        frame.loadTemperature.val.Add(Val[index]);
                        frame.loadTemperature.patternName.Add(PatternName[index]);
                    }
                }
            }
            return frame;
        }
        private Frame GetLocalAxes(Frame frame)
        {
            var localAxes = new LocalAxes();
            double ang = 0; bool advanced = false;
            if (tryWrapper(() => Doc.Document.FrameObj.GetLocalAxes(frame.name,
                ref ang, ref advanced)))
            {
                frame.localAxes = localAxes;
                frame.localAxes.ang = ang; frame.localAxes.advanced = advanced;
            }
            return frame;
        }
        private Frame GetMass(Frame frame)
        {
            double mass = 0;
            if (tryWrapper(() => Doc.Document.FrameObj.GetMass(frame.name,
                ref mass)))
            {
                frame.mass = mass;
            }
            return frame;
        }
        private Frame GetMaterialOverwrite(Frame frame)
        {
            var materialOverwrite = new MaterialOverwrite();
            string propName = "";
            if (tryWrapper(() => Doc.Document.FrameObj.GetMaterialOverwrite(frame.name,
                ref propName)))
            {
                frame.materialOverwrite = materialOverwrite;
                frame.materialOverwrite.propName = propName;
            }
            return frame;
        }
        private Frame GetModifiers(Frame frame)
        {
            var modifiers = new Modifiers();
            double[] mod = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetModifiers(frame.name,
                ref mod)))
            {
                foreach (var item in mod.ToList())
                {
                    frame.modifiers = modifiers;
                    frame.modifiers.value.Add(item);
                }
            }
            return frame;
        }
        private Frame GetOutputStations(Frame frame)
        {
            var outputStations = new OutputStations();
            int myType = 0, minSection = 0; double maxSegSize = 0;
            bool ends = false, loads = false;
            if (tryWrapper(() => Doc.Document.FrameObj.GetOutputStations(frame.name,
                ref myType, ref maxSegSize, ref minSection, ref ends, ref loads)))
            {
                frame.outputStations = outputStations;
                frame.outputStations.myType = myType; frame.outputStations.maxSegSize = maxSegSize;
                frame.outputStations.minSections = minSection;
                frame.outputStations.noOutPutAndDesignAtElementEnds = ends;
                frame.outputStations.noOutPutAndDesignAtPointLoads = loads;
            }
            return frame;
        }
        private Frame GetPier(Frame frame)
        {
            var pier = new Pier();
            string pierName = "";
            if (tryWrapper(() => Doc.Document.FrameObj.GetPier(frame.name, ref pierName)))
            {
                frame.pier = pier;
                frame.pier.pierName = pierName;
            }
            return frame;
        }
        private Frame GetReleases(Frame frame)
        {
            var releases = new Releases();
            bool[] ii = { }, jj = { };
            double[] startValue = { }, endValue = { };
            if (tryWrapper(() => Doc.Document.FrameObj.GetReleases(frame.name,
                ref ii, ref jj, ref startValue, ref endValue)))
            {
                if (ii.Length > 0)
                {
                    if (frame.releases == null)
                    {
                        frame.releases = releases;
                    }
                    foreach (var i in ii.ToList())
                    {
                        frame.releases.ii.Add(i);
                    }
                }
                if (jj.Length > 0)
                {
                    if (frame.releases == null)
                    {
                        frame.releases = releases;
                    }
                    foreach (var j in jj.ToList())
                    {
                        frame.releases.jj.Add(j);
                    }
                }
                if (startValue.Length > 0)
                {
                    if (frame.releases == null)
                    {
                        frame.releases = releases;
                    }
                    foreach (var s in startValue.ToList())
                    {
                        frame.releases.startValue.Add(s);
                    }
                }
                if (endValue.Length > 0)
                {
                    if (frame.releases == null)
                    {
                        frame.releases = releases;
                    }
                    foreach (var e in endValue.ToList())
                    {
                        frame.releases.endValue.Add(e);
                    }
                }
            }
            return frame;
        }
        private Frame GetSpandrel(Frame frame)
        {
            var spandrel = new Spandrel();
            string spandrelName = "";
            if (tryWrapper(() => Doc.Document.FrameObj.GetSpandrel(frame.name,
                ref spandrelName)))
            {
                frame.spandrel = spandrel;
                frame.spandrel.spandrelName = spandrelName;
            }
            return frame;
        }
        private Frame GetSpringAssignment(Frame frame)
        {
            var springAssignment = new SpringAssignment();
            string springProp = "";
            if (tryWrapper(() => Doc.Document.FrameObj.GetSpringAssignment(frame.name,
                ref springProp)))
            {
                frame.springAssignment = springAssignment;
                frame.springAssignment.springProp = springProp;
            }
            return frame;
        }
        private Frame GetTCLimits(Frame frame)
        {
            var TCLimits = new TCLimits();
            bool limitCompressionExists = false, limitTensionExists = false;
            double limitCompression = 0, limitTension = 0;
            if (tryWrapper(() => Doc.Document.FrameObj.GetTCLimits(frame.name,
                ref limitCompressionExists, ref limitCompression,
                ref limitTensionExists, ref limitTension)))
            {
                frame.TCLimits = TCLimits;
                frame.TCLimits.limitCompressionExists = limitCompressionExists;
                frame.TCLimits.limitCompression = limitCompression;
                frame.TCLimits.limitTensionExists = limitTensionExists;
                frame.TCLimits.limitTension = limitTension;
            }
            return frame;
        }
        #endregion
        public Frame FrameToSpeckle(string name)
        {
            var frame = new Frame { name = name };
            var funcList = new List<Func<Frame, Frame>>() { GetPoints, GetSection,
                GetColumnSpliceOverwrite, GetDesignProcedure, GetEndLengthOffset,
                GetGroupAssign, GetInsertionPoint, GetLateralBracing,
                GetLoadDistributed, GetLoadPoint, GetLoadTemperature,
                GetLocalAxes, GetMass, GetMaterialOverwrite, GetModifiers,
                GetOutputStations, GetPier, GetReleases, GetSpandrel,
                GetSpringAssignment, GetTCLimits};

            foreach (var fun in funcList)
            {
                frame = fun(frame);
            }
            return frame;
        }
    }
}

﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System.Linq;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region StoryToNative util functions

        private Story SetStory(Story story)
        {
            string[] names = story.storyNames.ToArray();
            double[] heights = story.storyHeights.ToArray();
            bool[] isMaster = story.isMasterStory.ToArray();
            string[] similar = story.similarToStory.ToArray();
            bool[] spliceAbove = story.spliceAbove.ToArray();
            double[] spliceHeight = story.spliceHeight.ToArray();
            int[] color = story.color.ToArray();
            if (!tryWrapper(() => Doc.Document.Story.SetStories_2(story.baseElevation,
                story.numberStories, ref names, ref heights, ref isMaster, ref similar,
                ref spliceAbove, ref spliceHeight, ref color)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native story object",
                    level: Sentry.SentryLevel.Warning));
            }
            return story;
        }
        #endregion
        public object StoryToNative(Story story)
        {
            story = SetStory(story);
            return story;
        }

        #region StoryToSpeckle util functions

        private Story GetStory(Story story)
        {
            double baseElevation = 0;
            string[] names = { }, similar = { };
            double[] heights = { }, elevations = { }, spliceHeight = { };
            bool[] isMaster = { }, spliceAbove = { };
            int[] color = { }; int numberItems = 0;
            if (tryWrapper(() => Doc.Document.Story.GetStories_2(ref baseElevation,
                ref numberItems, ref names, ref elevations, ref heights, ref isMaster, ref similar,
                ref spliceAbove, ref spliceHeight, ref color)))
            {
                story.baseElevation = baseElevation;
                story.numberStories = numberItems; story.storyNames = names.ToList();
                story.storyHeights = heights.ToList(); story.isMasterStory = isMaster.ToList();
                story.similarToStory = similar.ToList();
                story.spliceAbove = spliceAbove.ToList();
                story.spliceHeight = spliceHeight.ToList(); story.color = color.ToList();

            }
            return story;
        }
        #endregion
        public Story StoryToSpeckle(string name)
        {
            var story = new Story();
            story = GetStory(story);
            return story;
        }
    }
}

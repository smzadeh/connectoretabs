﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropTendonToNative util functions

        private PropTendon SetPropTendon(PropTendon propTendon)
        {
            if (!tryWrapper(() => Doc.Document.PropTendon.SetProp(propTendon.name,
                propTendon.matProp, propTendon.modelingOption, propTendon.area,
                propTendon.color, propTendon.notes, propTendon.GUID)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native propTendon ${propTendon.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return propTendon;
        }
        #endregion
        public object PropTendonToNative(PropTendon propTendon)
        {
            propTendon = SetPropTendon(propTendon);
            return propTendon.name;
        }

        #region PropTendonToSpeckle util functions

        private PropTendon GetPropTendon(PropTendon propTendon)
        {
            string matProp = "", notes = "", GUID = "";
            int modelingOption = 0, color = 0; double area = 0;
            if (tryWrapper(() => Doc.Document.PropTendon.GetProp(propTendon.name,
                ref matProp, ref modelingOption, ref area, ref color, ref notes, ref GUID)))
            {
                propTendon.matProp = matProp; propTendon.modelingOption = modelingOption;
                propTendon.area = area; propTendon.color = color;
                propTendon.notes = notes; propTendon.GUID = GUID;
            }
            return propTendon;
        }
        #endregion
        public PropTendon PropTendonToSpeckle(string name)
        {
            var propTendon = new PropTendon { name = name };
            propTendon = GetPropTendon(propTendon);
            return propTendon;
        }
    }
}

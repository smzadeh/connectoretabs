﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region LinkToNative util functions

        private (Link, bool) AddByPoint(Link link)
        {
            PointToNative(link.point1);
            PointToNative(link.point2);
            string linkName = link.name;
            if (!tryWrapper(() => Doc.Document.LinkObj.AddByPoint(link.point1.name,
                link.point2.name, ref linkName)))
            {
                return (link, false);
            }
            link.name = linkName;
            return (link, true);
        }
        private Link SetProperty(Link link)
        {
            if (link.section != null)
            {
                PropLinkToNative(link.section);
                tryWrapper(() => Doc.Document.LinkObj.SetProperty(link.name,
                link.section.name));
            }
            return link;
        }
        private Link SetGroupAssign(Link link)
        {
            if (link.groupAssign != null)
            {
                foreach (var group in link.groupAssign.groupAssign)
                {
                    tryWrapper(() => Doc.Document.LinkObj.SetGroupAssign(link.name, group));
                }
            }
            return link;
        }
        private Link SetLocalAxes(Link link)
        {
            if (link.localAxes != null)
            {
                tryWrapper(() => Doc.Document.LinkObj.SetLocalAxes(link.name,
                    link.localAxes.ang));
            }
            return link;
        }
        private Link SetLocalAxesAdvanced(Link link)
        {
            if (link.localAxesAdvanced != null)
            {
                int[] pLDir = link.localAxesAdvanced.pLDir.ToArray();
                string[] axPT = link.localAxesAdvanced.axPt.ToArray();
                string[] pLPt = link.localAxesAdvanced.pLPt.ToArray();
                double[] pLVect = link.localAxesAdvanced.pLVect.ToArray();
                double[] axVect = link.localAxesAdvanced.axVect.ToArray();
                int[] axDir = link.localAxesAdvanced.axDir.ToArray();
                tryWrapper(() => Doc.Document.LinkObj.SetLocalAxesAdvanced(link.name,
                            link.localAxesAdvanced.active, link.localAxesAdvanced.axVectOpt,
                            link.localAxesAdvanced.axCSys, ref axDir, ref axPT, ref axVect,
                            link.localAxesAdvanced.plane2, link.localAxesAdvanced.pLVectOpt,
                            link.localAxesAdvanced.pLCSys, ref pLDir, ref pLPt, ref pLVect));
            }
            return link;
        }

        #endregion
        public object LinkToNative(Link link)
        {
            bool isCreated = false;
            (link, isCreated) = AddByPoint(link);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native link ${link.name}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            var funcList = new List<Func<Link, Link>>() { SetProperty,
                SetGroupAssign, SetLocalAxes, SetLocalAxesAdvanced};
            foreach (var fun in funcList)
            {
                link = fun(link);
            }
            return link.name;
        }

        #region LinkToSpeckle util functions

        private Link GetPoints(Link link)
        {
            string point1 = "", point2 = "";
            if (tryWrapper(() => Doc.Document.LinkObj.GetPoints(link.name,
                ref point1, ref point2)))
            {
                link.point1 = PointToSpeckle(point1); link.point2 = PointToSpeckle(point2);
            }
            return link;
        }

        private Link GetSection(Link link)
        {
            string section = "";
            if (tryWrapper(() => Doc.Document.LinkObj.GetProperty(link.name,
                ref section)))
            {
                link.section = PropLinkToSpeckle(section);
            }
            return link;
        }

        private Link GetGroupAssign(Link link)
        {
            var groupAssign = new GroupAssign();
            int numberGroups = 0;
            string[] Groups = { };
            if (tryWrapper(() => Doc.Document.LinkObj.GetGroupAssign(link.name,
                ref numberGroups, ref Groups)))
            {
                if (numberGroups > 0)
                {
                    link.groupAssign = groupAssign;
                    foreach (var group in Groups)
                    {
                        link.groupAssign.groupAssign.Add(group);
                    }
                }
            }
            return link;
        }

        private Link GetLocalAxes(Link link)
        {
            var localAxes = new LocalAxes();
            double ang = 0; bool advanced = false;
            if (tryWrapper(() => Doc.Document.LinkObj.GetLocalAxes(link.name,
                ref ang, ref advanced)))
            {
                link.localAxes = localAxes;
                link.localAxes.ang = ang; link.localAxes.advanced = advanced;
            }
            return link;
        }

        private Link GetLocalAxesAdvanced(Link link)
        {
            var localAxesAdvanced = new LocalAxesAdvanced();
            int[] pLDir = { }, axDir = { };
            string[] axPT = { }, pLPt = { };
            double[] pLVect = { }, axVect = { };
            bool active = false;
            int axVectOpt = 0, Plane2 = 0, pLVectOpt = 0; ;
            string axCSys = "", pLCSys = "";
            if (tryWrapper(() => Doc.Document.LinkObj.GetLocalAxesAdvanced(link.name,
                ref active, ref axVectOpt, ref axCSys, ref axDir, ref axPT, ref axVect, ref Plane2,
                ref pLVectOpt, ref pLCSys, ref pLDir, ref pLPt, ref pLVect)))
            {
                link.localAxesAdvanced = localAxesAdvanced;
                link.localAxesAdvanced.active = active;
                link.localAxesAdvanced.axVectOpt = axVectOpt;
                link.localAxesAdvanced.axCSys = axCSys;
                link.localAxesAdvanced.axDir = axDir.ToList();
                link.localAxesAdvanced.axPt = axPT.ToList();
                link.localAxesAdvanced.axVect = axVect.ToList();
                link.localAxesAdvanced.plane2 = Plane2;
                link.localAxesAdvanced.pLVectOpt = pLVectOpt;
                link.localAxesAdvanced.pLCSys = pLCSys;
                link.localAxesAdvanced.pLDir = pLDir.ToList();
                link.localAxesAdvanced.pLPt = pLPt.ToList();
                link.localAxesAdvanced.pLVect = pLVect.ToList();
            }
            return link;
        }

        #endregion
        public Link LinkToSpeckle(string name)
        {
            var link = new Link { name = name };
            var funcList = new List<Func<Link, Link>>() { GetPoints, GetSection,
               GetGroupAssign, GetLocalAxes, GetLocalAxesAdvanced };
            foreach (var fun in funcList)
            {
                link = fun(link);
            }
            return link;
        }
    }
}

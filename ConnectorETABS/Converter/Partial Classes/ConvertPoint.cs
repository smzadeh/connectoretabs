﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using ET = ETABSv1;
using ConnectorETABS;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PointToNative util functions
        private bool areDoublesEqual(double num1, double num2)
        {
            double diff = Math.Abs(num1 - num2);
            double relDiff = diff / Math.Max((Math.Abs(num1) + Math.Abs(num2)) / 2, Double.Epsilon);
            return (relDiff < 0.00001);
        }
        private bool specklePointNameExistsInModel(Point specklePoint)
        {
            var names = ConnectorETABSUtils.GetAllPointNames(Doc);
            return names.Contains(specklePoint.name);
        }
        private bool specklePointHasSameCoordsWithModelPoint(Point specklePoint)
        {
            double x = 0, y = 0, z = 0;
            tryWrapper(() => Doc.Document.PointObj.GetCoordCartesian(specklePoint.name, ref x, ref y, ref z));
            if (areDoublesEqual(x, specklePoint.x) &&
                areDoublesEqual(y, specklePoint.y) &&
                areDoublesEqual(z, specklePoint.z))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Add a point by its cartesian coordinates. 
        /// If the speckle point has same coods with a point existing in mode,
        /// a new point is not created.
        /// If speckle point name exists in model, but the coords are different,
        /// a new point is created with a name created by ETABS.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private (Point, bool) AddCartesian(Point point)
        {
            if (specklePointNameExistsInModel(point))
            {
                if (specklePointHasSameCoordsWithModelPoint(point))
                {
                    // no need to create a new point, only need to merge point props
                    return (point, true);
                }
            }
            string pointName = point.name;
            if (!tryWrapper(() => Doc.Document.PointObj.AddCartesian(point.x, point.y, point.z, ref pointName, point.name)))
            {
                return (point, false);
            }
            point.name = pointName;
            return (point, true);
        }

        private Point SetDiaphragm(Point point)
        {
            if (point.diaphragmOption != null)
            {
                tryWrapper(() => Doc.Document.PointObj.SetDiaphragm(point.name, (ET.eDiaphragmOption)point.diaphragmOption, point.diaphragmName));
            }
            return point;
        }
        private Point SetGroupAssign(Point point)
        {
            if (point.groups != null)
            {
                foreach (var groupName in point.groups)
                {
                    tryWrapper(() => Doc.Document.PointObj.SetGroupAssign(point.name, groupName));
                }
            }
            return point;
        }
        private Point SetLoadDispl(Point point)
        {
            if (point.loadDisp != null)
            {
                int index = 0;
                foreach (var loadPattern in point.loadDisp.loadPattern)
                {
                    var values = new double[6] {
                        point.loadDisp.u1[index], point.loadDisp.u2[index],
                    point.loadDisp.u3[index], point.loadDisp.r1[index], point.loadDisp.r2[index], point.loadDisp.r3[index]};
                    LoadPatternToNative(loadPattern);
                    tryWrapper(() => Doc.Document.PointObj.SetLoadDispl(point.name,
                        loadPattern.name, ref values));

                    index += 1;
                }
            }
            return point;
        }
        private Point SetLoadForce(Point point)
        {
            if (point.loadForce != null)
            {
                int index = 0;
                foreach (var loadPattern in point.loadForce.loadPattern)
                {
                    var values = new double[6] {
                        point.loadForce.f1[index], point.loadForce.f2[index],
                    point.loadForce.f3[index], point.loadForce.m1[index],
                        point.loadForce.m2[index], point.loadForce.m3[index]};
                    LoadPatternToNative(loadPattern);
                    tryWrapper(() => Doc.Document.PointObj.SetLoadForce(point.name,
                        loadPattern.name, ref values));

                    index += 1;
                }
            }
            return point;
        }
        private Point SetMass(Point point)
        {
            if (point.mass != null)
            {
                var mass = point.mass.ToArray();
                tryWrapper(() => Doc.Document.PointObj.SetMass(point.name, ref mass));
            }
            return point;
        }

        private Point SetPanelZone(Point point)
        {
            if (point.panelZone != null)
            {
                tryWrapper(() => Doc.Document.PointObj.SetPanelZone(point.name,
                    point.panelZone.propType, point.panelZone.thickness, point.panelZone.k1,
                    point.panelZone.k2, point.panelZone.linkProp, point.panelZone.connectivity,
                    point.panelZone.localAxisFrom, point.panelZone.localAxisAngle));
            }
            return point;
        }
        private Point SetRestraint(Point point)
        {
            if (point.restraint != null)
            {
                var value = point.restraint.value.ToArray();
                tryWrapper(() => Doc.Document.PointObj.SetRestraint(point.name, ref value));
            }
            return point;
        }
        private Point SetSpecialPoint(Point point)
        {
            if (point.specialPoint != null)
            {
                tryWrapper(() => Doc.Document.PointObj.SetSpecialPoint(point.name,
                    point.specialPoint.specialPoint));
            }
            return point;
        }
        private Point SetSpring(Point point)
        {
            if (point.spring != null)
            {
                var k = point.spring.k.ToArray();
                tryWrapper(() => Doc.Document.PointObj.SetSpring(point.name, ref k));
            }
            return point;
        }
        private Point SetSpringAssignment(Point point)
        {
            if (point.springAssignment != null)
            {
                tryWrapper(() => Doc.Document.PointObj.SetSpringAssignment(point.name,
                    point.springAssignment.springProp));
            }
            return point;
        }
        private Point SetSpringCoupled(Point point)
        {
            if (point.springCoupled != null)
            {
                var k = point.springCoupled.k.ToArray();
                tryWrapper(() => Doc.Document.PointObj.SetSpringCoupled(point.name, ref k));
            }
            return point;
        }

        #endregion
        public object PointToNative(Point point)
        {
            bool isCreated = false;
            (point, isCreated) = AddCartesian(point);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native point ${point.name}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            var funcList = new List<Func<Point, Point>>() { SetDiaphragm, SetGroupAssign,
                SetLoadDispl, SetLoadForce, SetMass, SetPanelZone, SetRestraint, SetSpecialPoint,
                SetSpring, SetSpringAssignment, SetSpringCoupled};
            foreach (var fun in funcList)
            {
                point = fun(point);
            }
            return point.name;
        }

        #region PointToSpeckle util functions
        private Point GetCoordCartesian(Point point)
        {
            double x = 0, y = 0, z = 0;
            if (tryWrapper(() => Doc.Document.PointObj.GetCoordCartesian(point.name, ref x, ref y, ref z)))
            {
                point.x = x; point.y = y; point.z = z;
            }
            return point;
        }
        private Point GetDiaphragm(Point point)
        {
            var diaphragmOption = new ET.eDiaphragmOption();
            string diaphragmName = null;
            if (tryWrapper(() => Doc.Document.PointObj.GetDiaphragm(point.name, ref diaphragmOption, ref diaphragmName)))
            {
                if (diaphragmName != null) { point.diaphragmOption = (int)diaphragmOption; point.diaphragmName = diaphragmName; }
            }
            return point;
        }
        private Point GetGroupAssign(Point point)
        {
            int num = 0;
            string[] groups = { };
            if (tryWrapper(() => Doc.Document.PointObj.GetGroupAssign(point.name, ref num, ref groups)))
            {
                if (groups.Length > 0)
                {
                    point.groups = new List<string>();
                    foreach (string group in groups.ToList())
                    {
                        point.groups.Add(group);
                    }
                }
            }
            return point;
        }
        private Point GetLoadDispl(Point point)
        {
            var loadDisp = new LoadDisp();
            int num = 0;
            string[] pointName = { }, loadPat = { }, sys = { };
            int[] step = { };
            double[] u1 = { }, u2 = { }, u3 = { }, r1 = { }, r2 = { }, r3 = { };
            if (tryWrapper(() => Doc.Document.PointObj.GetLoadDispl(point.name, ref num,
                ref pointName, ref loadPat, ref step, ref sys, ref u1, ref u2, ref u3, ref r1, ref r2, ref r3)))
            {
                if (num > 0)
                {
                    point.loadDisp = loadDisp;
                    point.loadDisp.NumberItems = num;
                    for (int index = 0; index < num; index++)
                    {
                        point.loadDisp.loadPattern.Add(LoadPatternToSpeckle(loadPat[index]));
                        point.loadDisp.u1.Add(u1[index]); point.loadDisp.u2.Add(u2[index]);
                        point.loadDisp.u3.Add(u3[index]); point.loadDisp.r1.Add(r1[index]);
                        point.loadDisp.r2.Add(r2[index]); point.loadDisp.r3.Add(r3[index]);
                    }
                }
            }
            return point;
        }
        private Point GetLoadForce(Point point)
        {
            var loadForce = new LoadForce();
            int num = 0;
            string[] pointName = { }, loadPat = { }, sys = { };
            int[] step = { };
            double[] u1 = { }, u2 = { }, u3 = { }, r1 = { }, r2 = { }, r3 = { };
            if (tryWrapper(() => Doc.Document.PointObj.GetLoadForce(point.name, ref num,
                ref pointName, ref loadPat, ref step, ref sys, ref u1, ref u2, ref u3, ref r1, ref r2, ref r3)))
            {
                if (num > 0)
                {
                    point.loadForce = loadForce;
                    point.loadForce.NumberItems = num;
                    for (int index = 0; index < num; index++)
                    {
                        point.loadForce.loadPattern.Add(LoadPatternToSpeckle(loadPat[index]));
                        point.loadForce.f1.Add(u1[index]); point.loadForce.f2.Add(u2[index]);
                        point.loadForce.f3.Add(u3[index]); point.loadForce.m1.Add(r1[index]);
                        point.loadForce.m2.Add(r2[index]); point.loadForce.m3.Add(r3[index]);
                    }
                }
            }
            return point;
        }
        private Point GetMass(Point point)
        {
            double[] mass = { };
            if (tryWrapper(() => Doc.Document.PointObj.GetMass(point.name, ref mass)))
            {
                if (mass.Length > 0)
                {
                    point.mass = new List<double>();
                    foreach (var m in mass.ToList())
                    {
                        point.mass.Add(m);
                    }
                }
            }
            return point;
        }
        private Point GetPanelZone(Point point)
        {
            var panel = new PanelZone();
            int propType = 0, connectivity = 0, localAxisFrom = 0;
            double thickness = 0, k1 = 0, k2 = 0, localAxisAngle = 0;
            string linkProp = "";

            if (tryWrapper(() => Doc.Document.PointObj.GetPanelZone(point.name, ref propType, ref thickness, ref k1,
                ref k2, ref linkProp, ref connectivity, ref localAxisFrom, ref localAxisAngle)))
            {
                if (propType > 0)
                {
                    point.panelZone = panel;
                    point.panelZone.propType = propType; point.panelZone.connectivity = connectivity;
                    point.panelZone.thickness = thickness; point.panelZone.k1 = k1; point.panelZone.k2 = k2;
                    point.panelZone.linkProp = linkProp; point.panelZone.localAxisFrom = localAxisFrom;
                    point.panelZone.localAxisAngle = localAxisAngle;
                }
            }
            return point;
        }
        private Point GetRestraint(Point point)
        {
            var restraint = new Restraint { value = { } };
            bool[] res = { };
            if (tryWrapper(() => Doc.Document.PointObj.GetRestraint(point.name, ref res)))
            {
                if (res.Length > 0)
                {
                    point.restraint = restraint;
                    foreach (bool item in res)
                    {
                        point.restraint.value.Add(item);
                    }
                }
            }
            return point;
        }
        private Point GetSpecialPoint(Point point)
        {
            var special = new SpecialPoint();
            bool spec = false;
            if (tryWrapper(() => Doc.Document.PointObj.GetSpecialPoint(point.name, ref spec)))
            {
                if (spec == true)
                {
                    point.specialPoint = special; point.specialPoint.specialPoint = spec;
                }
            }
            return point;
        }
        private Point GetSpring(Point point)
        {
            var spring = new Spring();
            double[] k = { };
            if (tryWrapper(() => Doc.Document.PointObj.GetSpring(point.name, ref k)))
            {
                if (k.Length > 0)
                {
                    point.spring = spring;
                    foreach (var item in k)
                    {
                        point.spring.k.Add(item);
                    }
                }
            }
            return point;
        }
        private Point GetSpringAssignment(Point point)
        {
            var springAssingment = new SpringAssignment();
            string prop = "";
            if (tryWrapper(() => Doc.Document.PointObj.GetSpringAssignment(point.name, ref prop)))
            {
                if (prop != "")
                {
                    point.springAssignment = springAssingment;
                    point.springAssignment.springProp = prop;
                }
            }
            return point;
        }
        private Point GetSpringCoupled(Point point)
        {
            var springCoupled = new SpringCoupled();
            double[] k = { };
            if (tryWrapper(() => Doc.Document.PointObj.GetSpringCoupled(point.name, ref k)))
            {
                if (k.Length > 0)
                {
                    point.springCoupled = springCoupled;
                    foreach (var item in k)
                    {
                        point.springCoupled.k.Add(item);
                    }
                }
            }
            return point;
        }

        #endregion
        public Point PointToSpeckle(string name)
        {
            Point point = new Point { name = name };
            var funcList = new List<Func<Point, Point>>() { GetCoordCartesian, GetDiaphragm,
                GetGroupAssign, GetLoadDispl, GetLoadForce, GetMass, GetPanelZone,
                GetRestraint, GetSpecialPoint, GetSpring, GetSpringAssignment,
                GetSpringCoupled};
            foreach (var fun in funcList)
            {
                point = fun(point);
            }
            return point;
        }
    }
}

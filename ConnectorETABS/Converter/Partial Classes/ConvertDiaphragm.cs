﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region DiaphragmToNative util functions

        private Diaphragm SetDiaphragm(Diaphragm diaphragm)
        {
            if (!tryWrapper(() => Doc.Document.Diaphragm.SetDiaphragm(diaphragm.name,
                diaphragm.semiRigid)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native diaphragm ${diaphragm.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return diaphragm;
        }
        #endregion
        public object DiaphragmToNative(Diaphragm diaphragm)
        {
            diaphragm = SetDiaphragm(diaphragm);
            return diaphragm.name;
        }

        #region DiaphragmToSpeckle util functions

        private Diaphragm GetDiaphragm(Diaphragm diaphragm)
        {
            bool semiRigid = false;
            if (tryWrapper(() => Doc.Document.Diaphragm.GetDiaphragm(diaphragm.name,
                ref semiRigid)))
            {
                diaphragm.semiRigid = semiRigid;
            }
            return diaphragm;
        }
        #endregion
        public Diaphragm DiaphragmToSpeckle(string name)
        {
            var diaphragm = new Diaphragm { name = name };
            diaphragm = GetDiaphragm(diaphragm);
            return diaphragm;
        }
    }
}

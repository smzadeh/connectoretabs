﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropLinkToNative util functions

        private PropLink SetAcceptanceCriteria(PropLink propLink)
        {
            if (propLink.AcceptanceCriteria != null)
            {
                bool[] Active = propLink.AcceptanceCriteria.Active.ToArray();
                double[] IOPos = propLink.AcceptanceCriteria.IOPos.ToArray();
                double[] LSPos = propLink.AcceptanceCriteria.LSPos.ToArray();
                double[] CPPos = propLink.AcceptanceCriteria.CPPos.ToArray();
                double[] IONeg = propLink.AcceptanceCriteria.IONeg.ToArray();
                double[] LSNeg = propLink.AcceptanceCriteria.LSNeg.ToArray();
                double[] CPNeg = propLink.AcceptanceCriteria.CPNeg.ToArray();
                if (!tryWrapper(() => Doc.Document.PropLink.SetAcceptanceCriteria(propLink.name,
                    propLink.AcceptanceCriteria.AcceptanceType, propLink.AcceptanceCriteria.Symmetric,
                    ref Active, ref IOPos, ref LSPos, ref CPPos, ref IONeg, ref LSNeg,
                    ref CPNeg)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field AcceptanceCriteria",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetDamper(PropLink propLink)
        {
            if (propLink.Damper != null)
            {
                bool[] DOF = propLink.Damper.DOF.ToArray();
                bool[] Fixed = propLink.Damper.Fixed.ToArray();
                bool[] Nonlinear = propLink.Damper.Nonlinear.ToArray();
                double[] ke = propLink.Damper.ke.ToArray();
                double[] ce = propLink.Damper.ce.ToArray();
                double[] k = propLink.Damper.k.ToArray();
                double[] c = propLink.Damper.c.ToArray();
                double[] CExp = propLink.Damper.CExp.ToArray();
                double DJ2 = propLink.Damper.DJ2;
                double DJ3 = propLink.Damper.DJ3;
                string Notes = propLink.Damper.Notes;
                string GUID = propLink.Damper.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetDamper(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref c, ref CExp,
                    DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field Damper",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetDamperBilinear(PropLink propLink)
        {
            if (propLink.DamperBilinear != null)
            {
                bool[] DOF = propLink.DamperBilinear.DOF.ToArray();
                bool[] Fixed = propLink.DamperBilinear.Fixed.ToArray();
                bool[] Nonlinear = propLink.DamperBilinear.Nonlinear.ToArray();
                double[] ke = propLink.DamperBilinear.ke.ToArray();
                double[] ce = propLink.DamperBilinear.ce.ToArray();
                double[] k = propLink.DamperBilinear.k.ToArray();
                double[] c = propLink.DamperBilinear.c.ToArray();
                double[] CY = propLink.DamperBilinear.cY.ToArray();
                double[] ForceLimit = propLink.DamperBilinear.ForceLimit.ToArray();
                double DJ2 = propLink.DamperBilinear.DJ2;
                double DJ3 = propLink.DamperBilinear.DJ3;
                string Notes = propLink.DamperBilinear.Notes;
                string GUID = propLink.DamperBilinear.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetDamperBilinear(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref c, ref CY, ref ForceLimit,
                    DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field DamperBilinear",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetDamperFrictionSpring(PropLink propLink)
        {
            if (propLink.DamperFrictionSpring != null)
            {
                bool[] DOF = propLink.DamperFrictionSpring.DOF.ToArray();
                bool[] Fixed = propLink.DamperFrictionSpring.Fixed.ToArray();
                bool[] Nonlinear = propLink.DamperFrictionSpring.Nonlinear.ToArray();
                double[] ke = propLink.DamperFrictionSpring.ke.ToArray();
                double[] ce = propLink.DamperFrictionSpring.ce.ToArray();
                double[] k = propLink.DamperFrictionSpring.k.ToArray();
                double[] k1 = propLink.DamperFrictionSpring.K1.ToArray();
                double[] k2 = propLink.DamperFrictionSpring.K2.ToArray();
                double[] u0 = propLink.DamperFrictionSpring.u0.ToArray();
                double[] us = propLink.DamperFrictionSpring.us.ToArray();
                int[] dir = propLink.DamperFrictionSpring.direction.ToArray();
                double DJ2 = propLink.DamperFrictionSpring.DJ2;
                double DJ3 = propLink.DamperFrictionSpring.DJ3;
                string Notes = propLink.DamperFrictionSpring.Notes;
                string GUID = propLink.DamperFrictionSpring.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetDamperFrictionSpring(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref k1, ref k2,
                    ref u0, ref us, ref dir, DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field DamperFrictionSpring",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetFrictionIsolator(PropLink propLink)
        {
            if (propLink.FrictionIsolator != null)
            {
                bool[] DOF = propLink.FrictionIsolator.DOF.ToArray();
                bool[] Fixed = propLink.FrictionIsolator.Fixed.ToArray();
                bool[] Nonlinear = propLink.FrictionIsolator.Nonlinear.ToArray();
                double[] ke = propLink.FrictionIsolator.Ke.ToArray();
                double[] ce = propLink.FrictionIsolator.Ce.ToArray();
                double[] k = propLink.FrictionIsolator.K.ToArray();
                double[] slow = propLink.FrictionIsolator.Slow.ToArray();
                double[] fast = propLink.FrictionIsolator.Fast.ToArray();
                double[] rate = propLink.FrictionIsolator.Rate.ToArray();
                double[] radius = propLink.FrictionIsolator.Radius.ToArray();
                double damping = propLink.FrictionIsolator.Damping;
                double DJ2 = propLink.FrictionIsolator.DJ2;
                double DJ3 = propLink.FrictionIsolator.DJ3;
                string Notes = propLink.FrictionIsolator.Notes;
                string GUID = propLink.FrictionIsolator.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetFrictionIsolator(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref slow, ref fast,
                    ref rate, ref radius, damping, DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field FrictionIsolator",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetGap(PropLink propLink)
        {
            if (propLink.Gap != null)
            {
                bool[] DOF = propLink.Gap.DOF.ToArray();
                bool[] Fixed = propLink.Gap.Fixed.ToArray();
                bool[] Nonlinear = propLink.Gap.Nonlinear.ToArray();
                double[] ke = propLink.Gap.Ke.ToArray();
                double[] ce = propLink.Gap.Ce.ToArray();
                double[] k = propLink.Gap.K.ToArray();
                double[] dis = propLink.Gap.Dis.ToArray();
                double DJ2 = propLink.Gap.DJ2;
                double DJ3 = propLink.Gap.DJ3;
                string Notes = propLink.Gap.Notes;
                string GUID = propLink.Gap.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetGap(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref dis,
                    DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field Gap",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetHook(PropLink propLink)
        {
            if (propLink.Hook != null)
            {
                bool[] DOF = propLink.Hook.DOF.ToArray();
                bool[] Fixed = propLink.Hook.Fixed.ToArray();
                bool[] Nonlinear = propLink.Hook.Nonlinear.ToArray();
                double[] ke = propLink.Hook.Ke.ToArray();
                double[] ce = propLink.Hook.Ce.ToArray();
                double[] k = propLink.Hook.K.ToArray();
                double[] dis = propLink.Hook.Dis.ToArray();
                double DJ2 = propLink.Hook.DJ2;
                double DJ3 = propLink.Hook.DJ3;
                string Notes = propLink.Hook.Notes;
                string GUID = propLink.Hook.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetHook(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref dis,
                    DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field Hook",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetLinear(PropLink propLink)
        {
            if (propLink.Linear != null)
            {
                bool[] DOF = propLink.Linear.DOF.ToArray();
                bool[] Fixed = propLink.Linear.Fixed.ToArray();
                double[] ke = propLink.Linear.Ke.ToArray();
                double[] ce = propLink.Linear.Ce.ToArray();
                double DJ2 = propLink.Linear.DJ2;
                double DJ3 = propLink.Linear.DJ3;
                string Notes = propLink.Linear.Notes;
                string GUID = propLink.Linear.GUID;
                bool kecoupled = propLink.Linear.KeCoupled;
                bool cecoupled = propLink.Linear.CeCoupled;
                if (!tryWrapper(() => Doc.Document.PropLink.SetLinear(propLink.name,
                    ref DOF, ref Fixed, ref ke, ref ce, DJ2, DJ3, kecoupled, cecoupled, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field Linear",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetMultiLinearElastic(PropLink propLink)
        {
            if (propLink.MultiLinearElastic != null)
            {
                bool[] DOF = propLink.MultiLinearElastic.DOF.ToArray();
                bool[] Fixed = propLink.MultiLinearElastic.Fixed.ToArray();
                bool[] Nonlinear = propLink.MultiLinearElastic.Nonlinear.ToArray();
                double[] ke = propLink.MultiLinearElastic.Ke.ToArray();
                double[] ce = propLink.MultiLinearElastic.Ce.ToArray();
                double DJ2 = propLink.MultiLinearElastic.DJ2;
                double DJ3 = propLink.MultiLinearElastic.DJ3;
                string Notes = propLink.MultiLinearElastic.Notes;
                string GUID = propLink.MultiLinearElastic.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetMultiLinearElastic(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field MultiLinearElastic",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetMultiLinearPlastic(PropLink propLink)
        {
            if (propLink.MultiLinearPlastic != null)
            {
                bool[] DOF = propLink.MultiLinearPlastic.DOF.ToArray();
                bool[] Fixed = propLink.MultiLinearPlastic.Fixed.ToArray();
                bool[] Nonlinear = propLink.MultiLinearPlastic.Nonlinear.ToArray();
                double[] ke = propLink.MultiLinearPlastic.Ke.ToArray();
                double[] ce = propLink.MultiLinearPlastic.Ce.ToArray();
                double DJ2 = propLink.MultiLinearPlastic.DJ2;
                double DJ3 = propLink.MultiLinearPlastic.DJ3;
                string Notes = propLink.MultiLinearPlastic.Notes;
                string GUID = propLink.MultiLinearPlastic.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetMultiLinearPlastic(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field MultiLinearPlastic",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetMultiLinearPoints(PropLink propLink)
        {
            if (propLink.MultiLinearPoints != null)
            {
                int DOF = propLink.MultiLinearPoints.DOF;
                int NumberPoints = propLink.MultiLinearPoints.NumberPoints;
                double[] F = propLink.MultiLinearPoints.F.ToArray();
                double[] D = propLink.MultiLinearPoints.D.ToArray();
                int MyType = propLink.MultiLinearPoints.MyType;
                double A1 = propLink.MultiLinearPoints.A1;
                double A2 = propLink.MultiLinearPoints.A2;
                double B1 = propLink.MultiLinearPoints.B1;
                double B2 = propLink.MultiLinearPoints.B2;
                double Eta = propLink.MultiLinearPoints.Eta;
                if (!tryWrapper(() => Doc.Document.PropLink.SetMultiLinearPoints(propLink.name,
                    DOF, NumberPoints, ref F, ref D, MyType, A1, A2, B1, B2, Eta)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field MultiLinearPoints",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetPDelta(PropLink propLink)
        {
            if (propLink.PDelta != null)
            {

                double[] value = propLink.PDelta.Value.ToArray();
                if (!tryWrapper(() => Doc.Document.PropLink.SetPDelta(propLink.name,
                    ref value)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field PDelta",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetPlasticWen(PropLink propLink)
        {
            if (propLink.PlasticWen != null)
            {
                bool[] DOF = propLink.PlasticWen.DOF.ToArray();
                bool[] Fixed = propLink.PlasticWen.Fixed.ToArray();
                bool[] Nonlinear = propLink.PlasticWen.Nonlinear.ToArray();
                double[] ke = propLink.PlasticWen.Ke.ToArray();
                double[] ce = propLink.PlasticWen.Ce.ToArray();
                double[] k = propLink.PlasticWen.K.ToArray();
                double[] yield = propLink.PlasticWen.Yield.ToArray();
                double[] ratio = propLink.PlasticWen.Ratio.ToArray();
                double[] exp = propLink.PlasticWen.Exp.ToArray();
                double DJ2 = propLink.PlasticWen.DJ2;
                double DJ3 = propLink.PlasticWen.DJ3;
                string Notes = propLink.PlasticWen.Notes;
                string GUID = propLink.PlasticWen.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetPlasticWen(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref yield,
                    ref ratio, ref exp, DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field PlasticWen",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetRubberIsolator(PropLink propLink)
        {
            if (propLink.RubberIsolator != null)
            {
                bool[] DOF = propLink.RubberIsolator.DOF.ToArray();
                bool[] Fixed = propLink.RubberIsolator.Fixed.ToArray();
                bool[] Nonlinear = propLink.RubberIsolator.Nonlinear.ToArray();
                double[] ke = propLink.RubberIsolator.Ke.ToArray();
                double[] ce = propLink.RubberIsolator.Ce.ToArray();
                double[] k = propLink.RubberIsolator.K.ToArray();
                double[] yield = propLink.RubberIsolator.Yield.ToArray();
                double[] ratio = propLink.RubberIsolator.Ratio.ToArray();
                double DJ2 = propLink.RubberIsolator.DJ2;
                double DJ3 = propLink.RubberIsolator.DJ3;
                string Notes = propLink.RubberIsolator.Notes;
                string GUID = propLink.RubberIsolator.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetRubberIsolator(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref yield,
                    ref ratio, DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field RubberIsolator",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetSpringData(PropLink propLink)
        {
            if (propLink.SpringData != null)
            {
                if (!tryWrapper(() => Doc.Document.PropLink.SetSpringData(propLink.name,
                    propLink.SpringData.DefinedForThisLength, propLink.SpringData.DefinedForThisArea)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field SpringData",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetTCFrictionIsolator(PropLink propLink)
        {
            if (propLink.TCFrictionIsolator != null)
            {
                bool[] DOF = propLink.TCFrictionIsolator.DOF.ToArray();
                bool[] Fixed = propLink.TCFrictionIsolator.Fixed.ToArray();
                bool[] Nonlinear = propLink.TCFrictionIsolator.Nonlinear.ToArray();
                double[] ke = propLink.TCFrictionIsolator.Ke.ToArray();
                double[] ce = propLink.TCFrictionIsolator.Ce.ToArray();
                double[] k = propLink.TCFrictionIsolator.K.ToArray();
                double[] slow = propLink.TCFrictionIsolator.Slow.ToArray();
                double[] fast = propLink.TCFrictionIsolator.Fast.ToArray();
                double[] rate = propLink.TCFrictionIsolator.Rate.ToArray();
                double[] slowT = propLink.TCFrictionIsolator.SlowT.ToArray();
                double[] fastT = propLink.TCFrictionIsolator.FastT.ToArray();
                double[] rateT = propLink.TCFrictionIsolator.RateT.ToArray();
                double[] radius = propLink.TCFrictionIsolator.Radius.ToArray();
                double kt = propLink.TCFrictionIsolator.Kt;
                double dis = propLink.TCFrictionIsolator.Dis;
                double dist = propLink.TCFrictionIsolator.Dist;
                double damping = propLink.TCFrictionIsolator.Damping;
                double DJ2 = propLink.TCFrictionIsolator.DJ2;
                double DJ3 = propLink.TCFrictionIsolator.DJ3;
                string Notes = propLink.TCFrictionIsolator.Notes;
                string GUID = propLink.TCFrictionIsolator.GUID;
                if (!tryWrapper(() => Doc.Document.PropLink.SetTCFrictionIsolator(propLink.name,
                    ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref slow, ref fast,
                    ref rate, ref radius, ref slowT, ref fastT, ref rateT, kt, dis, dist, damping,
                    DJ2, DJ3, Notes, GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field TCFrictionIsolator",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }
        private PropLink SetPropLinkWeightAndMass(PropLink propLink)
        {
            if (propLink.PropLinkWeightAndMass != null)
            {
                if (!tryWrapper(() => Doc.Document.PropLink.SetWeightAndMass(propLink.name,
                    propLink.PropLinkWeightAndMass.W, propLink.PropLinkWeightAndMass.M,
                    propLink.PropLinkWeightAndMass.R1, propLink.PropLinkWeightAndMass.R2,
                    propLink.PropLinkWeightAndMass.R3)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propLink ${propLink.name}" +
                        $"field PropLinkWeightAndMass",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propLink;
        }

        #endregion
        public object PropLinkToNative(PropLink propLink)
        {
            var funcList = new List<Func<PropLink, PropLink>>() { SetDamper,
                SetDamperBilinear, SetDamperFrictionSpring, SetFrictionIsolator, SetGap, SetHook,
                SetLinear, SetMultiLinearElastic, SetMultiLinearPlastic,SetMultiLinearPoints,
                SetPDelta, SetPlasticWen, SetRubberIsolator, SetSpringData, SetTCFrictionIsolator,
                SetAcceptanceCriteria, SetPropLinkWeightAndMass};
            foreach (var fun in funcList)
            {
                propLink = fun(propLink);
            }
            return propLink.name;
        }

        #region PropLinkToSpeckle util functions

        private PropLink GetAcceptanceCriteria(PropLink propLink)
        {
            var AcceptanceCriteria = new AcceptanceCriteria();
            bool[] Active = { };
            double[] IOPos = { }, LSPos = { }, CPPos = { }, IONeg = { }, LSNeg = { }, CPNeg = { };
            int AcceptanceType = 0; bool Symmetric = false;
            if (tryWrapper(() => Doc.Document.PropLink.GetAcceptanceCriteria(propLink.name,
                ref AcceptanceType, ref Symmetric, ref Active, ref IOPos, ref LSPos,
                ref CPPos, ref IONeg, ref LSNeg, ref CPNeg)))
            {
                propLink.AcceptanceCriteria = AcceptanceCriteria;
                propLink.AcceptanceCriteria.AcceptanceType = AcceptanceType;
                propLink.AcceptanceCriteria.Symmetric = Symmetric;
                propLink.AcceptanceCriteria.Active = Active.ToList();
                propLink.AcceptanceCriteria.IOPos = IOPos.ToList();
                propLink.AcceptanceCriteria.LSPos = LSPos.ToList();
                propLink.AcceptanceCriteria.CPPos = CPPos.ToList();
                propLink.AcceptanceCriteria.IONeg = IONeg.ToList();
                propLink.AcceptanceCriteria.LSNeg = LSNeg.ToList();
                propLink.AcceptanceCriteria.CPNeg = CPNeg.ToList();
            }
            return propLink;
        }
        private PropLink GetDamper(PropLink propLink)
        {
            var Damper = new Damper();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, c = { }, CExp = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.SetDamper(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref c, ref CExp,
                DJ2, DJ3, Notes, GUID)))
            {
                propLink.Damper = Damper;
                propLink.Damper.DOF = DOF.ToList();
                propLink.Damper.Fixed = Fixed.ToList();
                propLink.Damper.Nonlinear = Nonlinear.ToList();
                propLink.Damper.ke = ke.ToList();
                propLink.Damper.ce = ce.ToList();
                propLink.Damper.k = k.ToList();
                propLink.Damper.c = c.ToList();
                propLink.Damper.CExp = CExp.ToList();
                propLink.Damper.DJ2 = DJ2;
                propLink.Damper.DJ3 = DJ3;
                propLink.Damper.Notes = Notes;
                propLink.Damper.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetDamperBilinear(PropLink propLink)
        {
            var DamperBilinear = new DamperBilinear();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, c = { }, CY = { }, ForceLimit = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.GetDamperBilinear(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref c, ref CY,
                ref ForceLimit, ref DJ2, ref DJ3, ref Notes, ref GUID)))
            {
                propLink.DamperBilinear = DamperBilinear;
                propLink.DamperBilinear.DOF = DOF.ToList();
                propLink.DamperBilinear.Fixed = Fixed.ToList();
                propLink.DamperBilinear.Nonlinear = Nonlinear.ToList();
                propLink.DamperBilinear.ke = ke.ToList();
                propLink.DamperBilinear.ce = ce.ToList();
                propLink.DamperBilinear.k = k.ToList();
                propLink.DamperBilinear.c = c.ToList();
                propLink.DamperBilinear.cY = CY.ToList();
                propLink.DamperBilinear.ForceLimit = ForceLimit.ToList();
                propLink.DamperBilinear.DJ2 = DJ2;
                propLink.DamperBilinear.DJ3 = DJ3;
                propLink.DamperBilinear.Notes = Notes;
                propLink.DamperBilinear.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetDamperFrictionSpring(PropLink propLink)
        {
            var DamperFrictionSpring = new DamperFrictionSpring();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, k1 = { };
            double[] k2 = { }, u0 = { }, us = { };
            int[] dir = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.GetDamperFrictionSpring(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref k1, ref k2,
                ref u0, ref us, ref dir, ref DJ2, ref DJ3, ref Notes, ref GUID)))
            {
                propLink.DamperFrictionSpring = DamperFrictionSpring;
                propLink.DamperFrictionSpring.DOF = DOF.ToList();
                propLink.DamperFrictionSpring.Fixed = Fixed.ToList();
                propLink.DamperFrictionSpring.Nonlinear = Nonlinear.ToList();
                propLink.DamperFrictionSpring.ke = ke.ToList();
                propLink.DamperFrictionSpring.ce = ce.ToList();
                propLink.DamperFrictionSpring.k = k.ToList();
                propLink.DamperFrictionSpring.K1 = k1.ToList();
                propLink.DamperFrictionSpring.K2 = k2.ToList();
                propLink.DamperFrictionSpring.u0 = u0.ToList();
                propLink.DamperFrictionSpring.us = us.ToList();
                propLink.DamperFrictionSpring.direction = dir.ToList();
                propLink.DamperFrictionSpring.DJ2 = DJ2;
                propLink.DamperFrictionSpring.DJ3 = DJ3;
                propLink.DamperFrictionSpring.Notes = Notes;
                propLink.DamperFrictionSpring.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetFrictionIsolator(PropLink propLink)
        {
            var FrictionIsolator = new FrictionIsolator();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, slow = { };
            double[] fast = { }, rate = { }, radius = { };
            double damping = 0, DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.GetFrictionIsolator(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref slow, ref fast,
                ref rate, ref radius, ref damping, ref DJ2, ref DJ3, ref Notes, ref GUID)))
            {
                propLink.FrictionIsolator = FrictionIsolator;
                propLink.FrictionIsolator.DOF = DOF.ToList();
                propLink.FrictionIsolator.Fixed = Fixed.ToList();
                propLink.FrictionIsolator.Nonlinear = Nonlinear.ToList();
                propLink.FrictionIsolator.Ke = ke.ToList();
                propLink.FrictionIsolator.Ce = ce.ToList();
                propLink.FrictionIsolator.K = k.ToList();
                propLink.FrictionIsolator.Slow = slow.ToList();
                propLink.FrictionIsolator.Fast = fast.ToList();
                propLink.FrictionIsolator.Rate = rate.ToList();
                propLink.FrictionIsolator.Radius = radius.ToList();
                propLink.FrictionIsolator.Damping = damping;
                propLink.FrictionIsolator.DJ2 = DJ2;
                propLink.FrictionIsolator.DJ3 = DJ3;
                propLink.FrictionIsolator.Notes = Notes;
                propLink.FrictionIsolator.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetGap(PropLink propLink)
        {
            var Gap = new Gap();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, dis = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.SetGap(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref dis,
                DJ2, DJ3, Notes, GUID)))
            {
                propLink.Gap = Gap;
                propLink.Gap.DOF = DOF.ToList();
                propLink.Gap.Fixed = Fixed.ToList();
                propLink.Gap.Nonlinear = Nonlinear.ToList();
                propLink.Gap.Ke = ke.ToList();
                propLink.Gap.Ce = ce.ToList();
                propLink.Gap.K = k.ToList();
                propLink.Gap.Dis = dis.ToList();
                propLink.Gap.DJ2 = DJ2; propLink.Gap.DJ3 = DJ3;
                propLink.Gap.Notes = Notes; propLink.Gap.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetHook(PropLink propLink)
        {
            var Hook = new Hook();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, dis = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.SetHook(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref dis,
                DJ2, DJ3, Notes, GUID)))
            {
                propLink.Hook = Hook;
                propLink.Hook.DOF = DOF.ToList();
                propLink.Hook.Fixed = Fixed.ToList();
                propLink.Hook.Nonlinear = Nonlinear.ToList();
                propLink.Hook.Ke = ke.ToList();
                propLink.Hook.Ce = ce.ToList();
                propLink.Hook.K = k.ToList();
                propLink.Hook.Dis = dis.ToList();
                propLink.Hook.DJ2 = DJ2; propLink.Hook.DJ3 = DJ3;
                propLink.Hook.Notes = Notes; propLink.Hook.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetLinear(PropLink propLink)
        {
            var Linear = new Linear();
            bool[] DOF = { }, Fixed = { };
            double[] ke = { }, ce = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            bool kecoupled = false, cecoupled = false;
            if (tryWrapper(() => Doc.Document.PropLink.GetLinear(propLink.name,
                ref DOF, ref Fixed, ref ke, ref ce, ref DJ2, ref DJ3, ref kecoupled,
                ref cecoupled, ref Notes, ref GUID)))
            {
                propLink.Linear = Linear;
                propLink.Linear.DOF = DOF.ToList();
                propLink.Linear.Fixed = Fixed.ToList();
                propLink.Linear.Ke = ke.ToList();
                propLink.Linear.Ce = ce.ToList();
                propLink.Linear.DJ2 = DJ2;
                propLink.Linear.DJ3 = DJ3;
                propLink.Linear.KeCoupled = kecoupled;
                propLink.Linear.CeCoupled = cecoupled;
                propLink.Linear.Notes = Notes;
                propLink.Linear.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetMultiLinearElastic(PropLink propLink)
        {
            var MultiLinearElastic = new MultiLinearElastic();
            bool[] DOF = { }, Fixed = { }, NonLinear = { };
            double[] ke = { }, ce = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.GetMultiLinearElastic(propLink.name,
                ref DOF, ref Fixed, ref NonLinear, ref ke, ref ce, ref DJ2, ref DJ3,
                ref Notes, ref GUID)))
            {
                propLink.MultiLinearElastic = MultiLinearElastic;
                propLink.MultiLinearElastic.DOF = DOF.ToList();
                propLink.MultiLinearElastic.Fixed = Fixed.ToList();
                propLink.MultiLinearElastic.Nonlinear = NonLinear.ToList();
                propLink.MultiLinearElastic.Ke = ke.ToList();
                propLink.MultiLinearElastic.Ce = ce.ToList();
                propLink.MultiLinearElastic.DJ2 = DJ2;
                propLink.MultiLinearElastic.DJ3 = DJ3;
                propLink.MultiLinearElastic.Notes = Notes;
                propLink.MultiLinearElastic.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetMultiLinearPlastic(PropLink propLink)
        {
            var MultiLinearPlastic = new MultiLinearPlastic();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.SetMultiLinearPlastic(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, DJ2, DJ3, Notes, GUID)))
            {
                propLink.MultiLinearPlastic = MultiLinearPlastic;
                propLink.MultiLinearPlastic.DOF = DOF.ToList();
                propLink.MultiLinearPlastic.Fixed = Fixed.ToList();
                propLink.MultiLinearPlastic.Nonlinear = Nonlinear.ToList();
                propLink.MultiLinearPlastic.Ke = ke.ToList();
                propLink.MultiLinearPlastic.Ce = ce.ToList();
                propLink.MultiLinearPlastic.DJ2 = DJ2;
                propLink.MultiLinearPlastic.DJ3 = DJ3;
                propLink.MultiLinearPlastic.Notes = Notes;
                propLink.MultiLinearPlastic.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetMultiLinearPoints(PropLink propLink)
        {
            var MultiLinearPoints = new MultiLinearPoints();
            int DOF = 0, NumberPoints = 0, MyType = 0;
            double[] F = { }, D = { };
            double A1 = 0, A2 = 0, B1 = 0, B2 = 0, Eta = 0;
            if (tryWrapper(() => Doc.Document.PropLink.SetMultiLinearPoints(propLink.name,
                DOF, NumberPoints, ref F, ref D, MyType, A1, A2, B1, B2, Eta)))
            {
                propLink.MultiLinearPoints = MultiLinearPoints;
                propLink.MultiLinearPoints.NumberPoints = NumberPoints;
                propLink.MultiLinearPoints.F = F.ToList();
                propLink.MultiLinearPoints.D = D.ToList();
                propLink.MultiLinearPoints.MyType = MyType;
                propLink.MultiLinearPoints.A1 = A1;
                propLink.MultiLinearPoints.A2 = A2;
                propLink.MultiLinearPoints.B1 = B1;
                propLink.MultiLinearPoints.B2 = B2;
                propLink.MultiLinearPoints.Eta = Eta;
            }
            return propLink;
        }
        private PropLink GetPDelta(PropLink propLink)
        {
            var PDelta = new PDelta();
            double[] value = { };
            if (tryWrapper(() => Doc.Document.PropLink.SetPDelta(propLink.name,
                ref value)))
            {
                propLink.PDelta = PDelta;
                propLink.PDelta.Value = value.ToList();
            }
            return propLink;
        }
        private PropLink GetPlasticWen(PropLink propLink)
        {
            var PlasticWen = new PlasticWen();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, yield = { }, ratio = { }, exp = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.GetPlasticWen(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref yield,
                ref ratio, ref exp, ref DJ2, ref DJ3, ref Notes, ref GUID)))
            {
                propLink.PlasticWen = PlasticWen;
                propLink.PlasticWen.DOF = DOF.ToList();
                propLink.PlasticWen.Fixed = Fixed.ToList();
                propLink.PlasticWen.Nonlinear = Nonlinear.ToList();
                propLink.PlasticWen.Ke = ke.ToList();
                propLink.PlasticWen.Ce = ce.ToList();
                propLink.PlasticWen.K = k.ToList();
                propLink.PlasticWen.Yield = yield.ToList();
                propLink.PlasticWen.Ratio = ratio.ToList();
                propLink.PlasticWen.Exp = exp.ToList();
                propLink.PlasticWen.DJ2 = DJ2;
                propLink.PlasticWen.DJ3 = DJ3;
                propLink.PlasticWen.Notes = Notes;
                propLink.PlasticWen.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetRubberIsolator(PropLink propLink)
        {
            var RubberIsolator = new RubberIsolator();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, yield = { }, ratio = { }, exp = { };
            double DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.GetRubberIsolator(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref yield,
                ref ratio, ref DJ2, ref DJ3, ref Notes, ref GUID)))
            {
                propLink.RubberIsolator = RubberIsolator;
                propLink.RubberIsolator.DOF = DOF.ToList();
                propLink.RubberIsolator.Fixed = Fixed.ToList();
                propLink.RubberIsolator.Nonlinear = Nonlinear.ToList();
                propLink.RubberIsolator.Ke = ke.ToList();
                propLink.RubberIsolator.Ce = ce.ToList();
                propLink.RubberIsolator.K = k.ToList();
                propLink.RubberIsolator.Yield = yield.ToList();
                propLink.RubberIsolator.Ratio = ratio.ToList();
                propLink.RubberIsolator.DJ2 = DJ2;
                propLink.RubberIsolator.DJ3 = DJ3;
                propLink.RubberIsolator.Notes = Notes;
                propLink.RubberIsolator.GUID = GUID;
            }
            return propLink;
        }

        private PropLink GetSpringData(PropLink propLink)
        {
            var SpringData = new SpringData();
            double DefinedForThisLength = 0, DefinedForThisArea = 0;
            if (tryWrapper(() => Doc.Document.PropLink.SetSpringData(propLink.name,
                DefinedForThisLength, DefinedForThisArea)))
            {
                propLink.SpringData = SpringData;
                propLink.SpringData.DefinedForThisLength = DefinedForThisLength;
                propLink.SpringData.DefinedForThisArea = DefinedForThisArea;
            }
            return propLink;
        }
        private PropLink GetTCFrictionIsolator(PropLink propLink)
        {
            var TCFrictionIsolator = new TCFrictionIsolator();
            bool[] DOF = { }, Fixed = { }, Nonlinear = { };
            double[] ke = { }, ce = { }, k = { }, slow = { }, fast = { };
            double[] rate = { }, slowT = { }, fastT = { }, rateT = { };
            double[] radius = { };
            double kt = 0, dis = 0, dist = 0, damping = 0, DJ2 = 0, DJ3 = 0;
            string Notes = "", GUID = "";
            if (tryWrapper(() => Doc.Document.PropLink.SetTCFrictionIsolator(propLink.name,
                ref DOF, ref Fixed, ref Nonlinear, ref ke, ref ce, ref k, ref slow, ref fast,
                ref rate, ref radius, ref slowT, ref fastT, ref rateT, kt, dis, dist, damping,
                DJ2, DJ3, Notes, GUID)))
            {
                propLink.TCFrictionIsolator = TCFrictionIsolator;
                propLink.TCFrictionIsolator.DOF = DOF.ToList();
                propLink.TCFrictionIsolator.Fixed = Fixed.ToList();
                propLink.TCFrictionIsolator.Nonlinear = Nonlinear.ToList();
                propLink.TCFrictionIsolator.Ke = ke.ToList();
                propLink.TCFrictionIsolator.Ce = ce.ToList();
                propLink.TCFrictionIsolator.K = k.ToList();
                propLink.TCFrictionIsolator.Slow = slow.ToList();
                propLink.TCFrictionIsolator.Fast = fast.ToList();
                propLink.TCFrictionIsolator.Rate = rate.ToList();
                propLink.TCFrictionIsolator.SlowT = slowT.ToList();
                propLink.TCFrictionIsolator.FastT = fast.ToList();
                propLink.TCFrictionIsolator.RateT = rateT.ToList();
                propLink.TCFrictionIsolator.Radius = radius.ToList();
                propLink.TCFrictionIsolator.Kt = kt;
                propLink.TCFrictionIsolator.Dis = dis;
                propLink.TCFrictionIsolator.Dist = dist;
                propLink.TCFrictionIsolator.Damping = damping;
                propLink.TCFrictionIsolator.DJ2 = DJ2;
                propLink.TCFrictionIsolator.DJ3 = DJ3;
                propLink.TCFrictionIsolator.Notes = Notes;
                propLink.TCFrictionIsolator.GUID = GUID;
            }
            return propLink;
        }
        private PropLink GetPropLinkWeightAndMass(PropLink propLink)
        {
            var PropLinkWeightAndMass = new PropLinkWeightAndMass();
            double W = 0, M = 0, R1 = 0, R2 = 0, R3 = 0;
            if (tryWrapper(() => Doc.Document.PropLink.GetWeightAndMass(
                propLink.name, ref W, ref M, ref R1, ref R2,
                ref R3)))
            {
                propLink.PropLinkWeightAndMass = PropLinkWeightAndMass;
                propLink.PropLinkWeightAndMass.W = W;
                propLink.PropLinkWeightAndMass.M = M;
                propLink.PropLinkWeightAndMass.R1 = R1;
                propLink.PropLinkWeightAndMass.R2 = R2;
                propLink.PropLinkWeightAndMass.R3 = R3;
            }
            return propLink;
        }

        #endregion
        public PropLink PropLinkToSpeckle(string name)
        {
            PropLink propLink = new PropLink { name = name };
            var funcList = new List<Func<PropLink, PropLink>>() { GetAcceptanceCriteria,
                GetDamper, GetDamperBilinear, GetDamperFrictionSpring, GetFrictionIsolator,
                GetGap, GetHook, GetLinear, GetMultiLinearElastic, GetMultiLinearPlastic,
                GetMultiLinearPoints, GetPDelta, GetPlasticWen, GetRubberIsolator,
                GetSpringData, GetTCFrictionIsolator, GetTCFrictionIsolator,
                GetPropLinkWeightAndMass};
            foreach (var fun in funcList)
            {
                propLink = fun(propLink);
            }
            return propLink;
        }
    }
}

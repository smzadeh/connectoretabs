﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropLineSpringToNative util functions

        private PropLineSpring SetPropLineSpring(PropLineSpring propLineSpring)
        {
            if (!tryWrapper(() => Doc.Document.PropLineSpring.SetLineSpringProp(propLineSpring.name,
                propLineSpring.u1, propLineSpring.u2, propLineSpring.u3, propLineSpring.r1,
                propLineSpring.nonlinearOption2, propLineSpring.nonlinearOption3,
                propLineSpring.color, propLineSpring.notes, propLineSpring.iGUID)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native propLineSpring ${propLineSpring.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return propLineSpring;
        }
        #endregion
        public object PropLineSpringToNative(PropLineSpring propLineSpring)
        {
            propLineSpring = SetPropLineSpring(propLineSpring);
            return propLineSpring.name;
        }

        #region PropLineSpringToSpeckle util functions

        private PropLineSpring GetPropLineSpring(PropLineSpring propLineSpring)
        {
            string notes = "", iGUID = "";
            double u1 = 0, u2 = 0, u3 = 0, r1 = 0;
            int nonlinearOption2 = 0, nonlinearOption3 = 0, color = 0;
            if (tryWrapper(() => Doc.Document.PropLineSpring.GetLineSpringProp(propLineSpring.name,
                ref u1, ref u2, ref u3, ref r1, ref nonlinearOption2, ref nonlinearOption3,
                ref color, ref notes, ref iGUID)))
            {
                propLineSpring.u1 = u1; propLineSpring.u2 = u2; propLineSpring.u3 = u3;
                propLineSpring.r1 = r1; propLineSpring.nonlinearOption2 = nonlinearOption2;
                propLineSpring.nonlinearOption3 = nonlinearOption3;
                propLineSpring.color = color;
                propLineSpring.notes = notes;
                propLineSpring.iGUID = iGUID;
            }
            return propLineSpring;
        }
        #endregion
        public PropLineSpring PropLineSpringToSpeckle(string name)
        {
            var propLineSpring = new PropLineSpring { name = name };
            propLineSpring = GetPropLineSpring(propLineSpring);
            return propLineSpring;
        }
    }
}

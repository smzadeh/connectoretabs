﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System.Linq;
using System;
using System.Collections.Generic;
using ET = ETABSv1;


namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropAreaToNative util functions

        private PropArea SetDeck(PropArea propArea)
        {
            if (propArea.deck != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetDeck(propArea.name,
                (ET.eDeckType)propArea.deck.deckType, (ET.eShellType)propArea.deck.shellType,
                propArea.deck.matProp.name, propArea.deck.thickness)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type Deck",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }
        private PropArea SetDeckFilled(PropArea propArea)
        {
            if (propArea.deckFilled != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetDeckFilled(propArea.name,
                    propArea.deckFilled.slabDepth, propArea.deckFilled.ribDepth, propArea.deckFilled.ribWidthTop,
                    propArea.deckFilled.ribWidthBot, propArea.deckFilled.ribSpacing,
                    propArea.deckFilled.shearThickness, propArea.deckFilled.unitWeight,
                    propArea.deckFilled.shearStudDia, propArea.deckFilled.shearStudHt,
                    propArea.deckFilled.shearStudFu)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type DeckFilled",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }
        private PropArea SetDeckSolidSlab(PropArea propArea)
        {
            if (propArea.deckSolidSlab != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetDeckSolidSlab(propArea.name,
                    propArea.deckSolidSlab.slabDepth, propArea.deckSolidSlab.shearStudDia,
                    propArea.deckSolidSlab.shearStudHt, propArea.deckSolidSlab.shearStudFu)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type DeckSolidSlab",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }

        private PropArea SetDeckUnfilled(PropArea propArea)
        {
            if (propArea.deckUnfilled != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetDeckUnfilled(propArea.name,
                    propArea.deckUnfilled.ribDepth, propArea.deckUnfilled.ribWidthTop,
                    propArea.deckUnfilled.ribWidthBot, propArea.deckUnfilled.ribSpacing,
                    propArea.deckUnfilled.shearThickness, propArea.deckUnfilled.unitWeight)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type DeckUnfilled",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }
        private PropArea SetDeckmodifiers(PropArea propArea)
        {
            if (propArea.modifiers != null)
            {
                double[] mod = propArea.modifiers.value.ToArray();
                if (!tryWrapper(() => Doc.Document.PropArea.SetModifiers(propArea.name,
                    ref mod)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" modifiers",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }
        private PropArea SetShellDesign(PropArea propArea)
        {
            if (propArea.shellDesign != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetShellDesign(propArea.name,
                    propArea.shellDesign.matProp.name, propArea.shellDesign.steelLayoutOption,
                    propArea.shellDesign.designCoverTopDir1, propArea.shellDesign.designCoverTopDir2,
                    propArea.shellDesign.designCoverBotDir1, propArea.shellDesign.designCoverBotDir2)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type ShellDesign",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }
        private PropArea SetShellLayer_1(PropArea propArea)
        {
            if (propArea.shellLayer_1 != null)
            {
                int num = propArea.shellLayer_1.numberLayers;
                string[] layerName = propArea.shellLayer_1.layerName.ToArray();
                double[] dist = propArea.shellLayer_1.dist.ToArray(), thickness = propArea.shellLayer_1.thickness.ToArray();
                int[] myType = propArea.shellLayer_1.myType.ToArray();
                int[] numIntegrationPts = propArea.shellLayer_1.numIntegrationPts.ToArray();
                string[] matProp = propArea.shellLayer_1.matProp.Select(prop => prop.name).ToArray();
                double[] matAng = propArea.shellLayer_1.matAng.ToArray();
                int[] s11Type = propArea.shellLayer_1.s11Type.ToArray();
                int[] s22Type = propArea.shellLayer_1.s22Type.ToArray();
                int[] s12Type = propArea.shellLayer_1.s12Type.ToArray();
                if (!tryWrapper(() => Doc.Document.PropArea.SetShellLayer_1(propArea.name,
                    ref num, ref layerName, ref dist, ref thickness,
                    ref myType, ref numIntegrationPts, ref matProp, ref matAng, ref s11Type,
                    ref s22Type, ref s12Type)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type ShellLayer_1",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }

        private PropArea SetSlab(PropArea propArea)
        {
            if (propArea.slab != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetSlab(propArea.name,
                    (ET.eSlabType)propArea.slab.slabType, (ET.eShellType)propArea.slab.shellType,
                    propArea.slab.matProp.name, propArea.slab.thickness, propArea.slab.color,
                    propArea.slab.notes, propArea.slab.GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type Slab",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }
        private PropArea SetSlabRibbed(PropArea propArea)
        {
            if (propArea.slabRibbed != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetSlabRibbed(propArea.name,
                    propArea.slabRibbed.overallDepth, propArea.slabRibbed.slabThickness,
                    propArea.slabRibbed.stemWidthTop, propArea.slabRibbed.stemWidthBot,
                    propArea.slabRibbed.ribSpacing, propArea.slabRibbed.ribsParallelTo)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type SlabRibbed",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }

        private PropArea SetSlabWaffle(PropArea propArea)
        {
            if (propArea.slabWaffle != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetSlabWaffle(propArea.name,
                    propArea.slabWaffle.overallDepth, propArea.slabWaffle.slabThickness,
                    propArea.slabWaffle.stemWidthTop, propArea.slabWaffle.stemWidthBot,
                    propArea.slabWaffle.ribSpacingDir1, propArea.slabWaffle.ribSpacingDir2)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type SlabWaffle",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }
        private PropArea SetWall(PropArea propArea)
        {
            if (propArea.wall != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetWall(propArea.name,
                    (ET.eWallPropType)propArea.wall.wallPropType, (ET.eShellType)propArea.wall.shellType,
                    propArea.wall.matProp.name, propArea.wall.thickness, propArea.wall.color,
                    propArea.wall.notes, propArea.wall.GUID)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type Wall",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }

        private PropArea SetWallAutoSelectList(PropArea propArea)
        {
            if (propArea.wallAutoSelectList != null)
            {
                if (!tryWrapper(() => Doc.Document.PropArea.SetWallAutoSelectList(propArea.name,
                    propArea.wallAutoSelectList.autoSelectList.ToArray(), propArea.wallAutoSelectList.startingProperty)))
                {
                    ConversionErrors.Add(new SpeckleException($"Failed to create native propArea ${propArea.name}" +
                        $" of type WallAutoSelect",
                        level: Sentry.SentryLevel.Warning));
                }
            }
            return propArea;
        }

        #endregion
        public object PropAreaToNative(PropArea propArea)
        {
            var funcList = new List<Func<PropArea, PropArea>>(){ SetDeck , SetDeckFilled ,SetDeckSolidSlab,
                SetDeckUnfilled, SetDeckmodifiers, SetShellDesign, SetShellLayer_1, SetSlab, SetSlabRibbed,
                SetSlabWaffle, SetWall, SetWallAutoSelectList};

            foreach (var fun in funcList)
            {
                propArea = fun(propArea);
            }
            return propArea.name;
        }
        #region PropAreaToSpeckle util functions

        private PropArea GetDeck(PropArea propArea)
        {
            ET.eDeckType deckType = 0;
            ET.eShellType shellType = 0;
            int color = 0;
            double thickness = 0;
            string matProp = "", notes = "", guid = "";
            if (tryWrapper(() => Doc.Document.PropArea.GetDeck(propArea.name, ref deckType,
                ref shellType, ref matProp, ref thickness, ref color, ref notes, ref guid)))
            {
                propArea.deck = new Deck();
                propArea.deck.deckType = (int)deckType;
                propArea.deck.shellType = (int)shellType;
                propArea.deck.matProp = PropMaterialToSpeckle(matProp);
                propArea.deck.thickness = thickness;
                propArea.deck.color = color; propArea.deck.notes = notes;
                propArea.deck.GUID = guid;
            }
            return propArea;
        }
        private PropArea GetDeckFilled(PropArea propArea)
        {
            double slabDepth = 0, ribDepth = 0, ribWidthTop = 0, ribWidthBot = 0, ribSpacing = 0;
            double shearThickness = 0, unitWeight = 0, shearStudDia = 0, shearStudHt = 0;
            double shearStudFu = 0;
            if (tryWrapper(() => Doc.Document.PropArea.GetDeckFilled(propArea.name,
                ref slabDepth, ref ribDepth, ref ribWidthTop, ref ribWidthBot,
                ref ribSpacing, ref shearThickness, ref unitWeight, ref shearStudDia,
                ref shearStudHt, ref shearStudFu)))
            {
                propArea.deckFilled = new DeckFilled();
                propArea.deckFilled.slabDepth = slabDepth;
                propArea.deckFilled.ribDepth = ribDepth;
                propArea.deckFilled.ribWidthTop = ribWidthTop;
                propArea.deckFilled.ribWidthBot = ribWidthBot;
                propArea.deckFilled.ribSpacing = ribSpacing;
                propArea.deckFilled.shearThickness = shearThickness;
                propArea.deckFilled.unitWeight = unitWeight;
                propArea.deckFilled.shearStudDia = shearStudDia;
                propArea.deckFilled.shearStudHt = shearStudHt;
                propArea.deckFilled.shearStudFu = shearStudFu;
            }
            return propArea;
        }
        
        private PropArea GetDeckSolidSlab(PropArea propArea)
        {
            double slabDepth = 0, shearStudDia = 0, shearStudHt = 0, shearStudFu = 0;
            if (tryWrapper(() => Doc.Document.PropArea.GetDeckSolidSlab(propArea.name,
                ref slabDepth, ref shearStudDia, ref shearStudDia, ref shearStudFu)))
            {
                propArea.deckSolidSlab = new DeckSolidSlab();
                propArea.deckSolidSlab.slabDepth = slabDepth;
                propArea.deckSolidSlab.shearStudDia = shearStudDia;
                propArea.deckSolidSlab.shearStudHt = shearStudHt;
                propArea.deckSolidSlab.shearStudFu = shearStudFu;
            }
            return propArea;
        }
        private PropArea GetDeckUnfilled(PropArea propArea)
        {
            double ribDepth = 0, ribWidthTop = 0, ribWidthBot = 0, ribSpacing = 0, shearThickness = 0;
            double unitWeight = 0;
            if (tryWrapper(() => Doc.Document.PropArea.GetDeckUnfilled(propArea.name,
                ref ribDepth, ref ribWidthTop, ref ribWidthBot, ref ribSpacing, ref shearThickness,
                ref unitWeight)))
            {
                propArea.deckUnfilled = new DeckUnfilled();
                propArea.deckUnfilled.ribDepth = ribDepth;
                propArea.deckUnfilled.ribWidthTop = ribWidthTop;
                propArea.deckUnfilled.ribWidthBot = ribWidthBot;
                propArea.deckUnfilled.ribSpacing = ribSpacing;
                propArea.deckUnfilled.shearThickness = shearThickness;
                propArea.deckUnfilled.unitWeight = unitWeight;
            }
            return propArea;
        }
        private PropArea GetShellDesign(PropArea propArea)
        {
            string matProp = ""; int steelLayoutOption = 0;
            double designCoverTopDir1 = 0, designCoverTopDir2 = 0, designCoverBotDir1 = 0, designCoverBotDir2 = 0;
            if (tryWrapper(() => Doc.Document.PropArea.GetShellDesign(propArea.name,
                ref matProp, ref steelLayoutOption, ref designCoverTopDir1, ref designCoverTopDir2,
                ref designCoverBotDir1, ref designCoverBotDir2)))
            {
                propArea.shellDesign = new ShellDesign();
                propArea.shellDesign.matProp = PropMaterialToSpeckle(matProp);
                propArea.shellDesign.steelLayoutOption = steelLayoutOption;
                propArea.shellDesign.designCoverTopDir1 = designCoverTopDir1;
                propArea.shellDesign.designCoverTopDir2 = designCoverTopDir2;
                propArea.shellDesign.designCoverBotDir1 = designCoverBotDir1;
                propArea.shellDesign.designCoverBotDir2 = designCoverBotDir2;
            }
            return propArea;
        }
        
        private PropArea GetShellLayer_1(PropArea propArea)
        {
            int numberLayers = 0;
            string[] layerName = { }, matProp = { };
            double[] dist = { }, thickness = { }, matAng = { };
            int[] myType = { }, numIntegrationPts = { }, s11Type = { };
            int[] s22Type = { }, s12Type = { };
            if (tryWrapper(() => Doc.Document.PropArea.GetShellLayer_1(propArea.name,
                ref numberLayers, ref layerName, ref dist, ref thickness, ref myType,
                ref numIntegrationPts, ref matProp, ref matAng, ref s11Type, ref s22Type, ref s22Type)))
            {
                if (numberLayers > 0)
                {
                    propArea.shellLayer_1 = new ShellLayer_1();
                    propArea.shellLayer_1.numberLayers = numberLayers;
                    propArea.shellLayer_1.layerName.AddRange(layerName.ToList());
                    propArea.shellLayer_1.dist.AddRange(dist.ToList());
                    propArea.shellLayer_1.thickness.AddRange(thickness.ToList());
                    propArea.shellLayer_1.myType.AddRange(myType.ToList());
                    propArea.shellLayer_1.numIntegrationPts.AddRange(numIntegrationPts.ToList());
                    for (int index = 0; index < numberLayers; index++)
                    {
                        propArea.shellLayer_1.matProp.Add(PropMaterialToSpeckle(matProp[index]));
                    }
                    propArea.shellLayer_1.matAng.AddRange(matAng.ToList());
                    propArea.shellLayer_1.s11Type.AddRange(s11Type.ToList());
                    propArea.shellLayer_1.s22Type.AddRange(s22Type.ToList());
                    propArea.shellLayer_1.s12Type.AddRange(s12Type.ToList());
                }
            }
            return propArea;
        }
        private PropArea GetSlab(PropArea propArea)
        {
            ET.eSlabType slabType = 0; ET.eShellType shellType = 0; string matProp = ""; double thickness = 0;
            int color = -1; string notes = ""; string GUID = "";
            if (tryWrapper(() => Doc.Document.PropArea.GetSlab(propArea.name,
                ref slabType, ref shellType, ref matProp, ref thickness, ref color, ref notes, ref GUID)))
            {
                propArea.slab = new Slab();
                propArea.slab.slabType = (int)slabType;
                propArea.slab.shellType = (int)shellType;
                propArea.slab.matProp = PropMaterialToSpeckle(matProp);
                propArea.slab.thickness = thickness;
                propArea.slab.color = color;
                propArea.slab.notes = notes;
                propArea.slab.GUID = GUID;
            }
            return propArea;
        }
        
        private PropArea GetSlabRibbed(PropArea propArea)
        {
            double overallDepth = 0, slabThickness = 0, stemWidthTop = 0, stemWidthBot = 0, ribSpacing = 0;
            int ribsParallelTo = 0;
            if (tryWrapper(() => Doc.Document.PropArea.GetSlabRibbed(propArea.name,
                ref overallDepth, ref slabThickness, ref stemWidthTop, ref stemWidthBot, ref ribSpacing, 
                ref ribsParallelTo)))
            {
                propArea.slabRibbed = new SlabRibbed();
                propArea.slabRibbed.overallDepth = overallDepth;
                propArea.slabRibbed.slabThickness = slabThickness;
                propArea.slabRibbed.stemWidthTop = stemWidthTop;
                propArea.slabRibbed.stemWidthBot = stemWidthBot;
                propArea.slabRibbed.ribSpacing = ribSpacing;
                propArea.slabRibbed.ribsParallelTo = ribsParallelTo;
            }
            return propArea;
        }
        private PropArea GetSlabWaffle(PropArea propArea)
        {
            double overallDepth = 0, slabThickness = 0, stemWidthTop = 0;
            double stemWidthBot = 0, ribSpacingDir1 = 0, ribSpacingDir2 = 0;
            if (tryWrapper(() => Doc.Document.PropArea.GetSlabWaffle(propArea.name, ref overallDepth,
                ref slabThickness, ref stemWidthTop, ref stemWidthBot, ref ribSpacingDir1,
                ref ribSpacingDir2)))
            {
                propArea.slabWaffle = new SlabWaffle();
                propArea.slabWaffle.overallDepth = overallDepth;
                propArea.slabWaffle.slabThickness = slabThickness;
                propArea.slabWaffle.stemWidthTop = stemWidthTop;
                propArea.slabWaffle.stemWidthBot = stemWidthBot;
                propArea.slabWaffle.ribSpacingDir1 = ribSpacingDir1;
                propArea.slabWaffle.ribSpacingDir2 = ribSpacingDir2;
            }
            return propArea;
        }
        private PropArea GetWall(PropArea propArea)
        {
            ET.eWallPropType wallPropType = 0;
            ET.eShellType shellType = 0;
            string matProp = ""; double thickness = 0; int color = -1;
            string notes = ""; string GUID = "";
            if (tryWrapper(() => Doc.Document.PropArea.GetWall(propArea.name,
                ref wallPropType, ref shellType, ref matProp, ref thickness, ref color,
                ref notes, ref GUID)))
            {
                propArea.wall = new Wall();
                propArea.wall.wallPropType = (int)wallPropType;
                propArea.wall.shellType = (int)shellType;
                propArea.wall.matProp = PropMaterialToSpeckle(matProp);
                propArea.wall.thickness = thickness;
                propArea.wall.color = color;
                propArea.wall.notes = notes; propArea.wall.GUID = GUID;

            }
            return propArea;
        }
        private PropArea GetWallAutoSelectList(PropArea propArea)
        {
            string[] autoSelectList ={ }; string startingProperty = "";
            if (tryWrapper(() => Doc.Document.PropArea.GetWallAutoSelectList(propArea.name,
                ref autoSelectList, ref startingProperty)))
            {
                if (autoSelectList.Length > 0)
                {
                    propArea.wallAutoSelectList = new WallAutoSelectList();
                    propArea.wallAutoSelectList.autoSelectList.AddRange(autoSelectList.ToList());
                    propArea.wallAutoSelectList.startingProperty = startingProperty;
                }
            }
            return propArea;
        }
        
        #endregion
        public PropArea PropAreaToSpeckle(string name)
        {
            var propArea = new PropArea { name = name };
            var funcList = new List<Func<PropArea, PropArea>>() { GetDeck, 
                GetDeckFilled, GetDeckSolidSlab, GetDeckUnfilled, 
                GetShellDesign, GetShellLayer_1, GetSlab, GetSlabRibbed, 
                GetSlabWaffle, GetWall, GetWallAutoSelectList };

            foreach (var fun in funcList)
            {
                propArea = fun(propArea);
            }
            return propArea;
        }
    }
}

﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region DesignSteelToNative util functions

        private (DesignSteel, bool) SetDesignSteelCodeName(DesignSteel designSteel)
        {
            if (!tryWrapper(() => Doc.Document.DesignSteel.SetCode(designSteel.codeName)))
            {
                return (designSteel, false);
            }
            return (designSteel, true);
        }

        private int AISC_LRFD93_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC_LRFD93.SetPreference(prefItem, prefValue);
        }
        private int AISC_LRFD93_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC_LRFD93.SetOverwrite(frameName, item, value);
        }
        private int AISC360_05_IBC2006_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC360_05_IBC2006.SetPreference(prefItem, prefValue);
        }
        private int AISC360_05_IBC2006_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC360_05_IBC2006.SetOverwrite(frameName, item, value);
        }
        private int Australian_AS4100_98_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Australian_AS4100_98.SetPreference(prefItem, prefValue);
        }
        private int Australian_AS4100_98_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Australian_AS4100_98.SetOverwrite(frameName, item, value);
        }
        private int BS5950_2000_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.BS5950_2000.SetPreference(prefItem, prefValue);
        }
        private int BS5950_2000_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.BS5950_2000.SetOverwrite(frameName, item, value);
        }
        private int Canadian_S16_09_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Canadian_S16_09.SetPreference(prefItem, prefValue);
        }
        private int Canadian_S16_09_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Canadian_S16_09.SetOverwrite(frameName, item, value);
        }
        private int Chinese_2010_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2010.SetPreference(prefItem, prefValue);
        }
        private int Chinese_2010_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2010.SetOverwrite(frameName, item, value);
        }
        private int Chinese_2018_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2018.SetPreference(prefItem, prefValue);
        }
        private int Chinese_2018_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2018.SetOverwrite(frameName, item, value);
        }

        private int Eurocode_3_2005_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Eurocode_3_2005.SetPreference(prefItem, prefValue);
        }
        private int Eurocode_3_2005_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Eurocode_3_2005.SetOverwrite(frameName, item, value);
        }
        private int Indian_IS_800_2007_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Indian_IS_800_2007.SetPreference(prefItem, prefValue);
        }
        private int Indian_IS_800_2007_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Indian_IS_800_2007.SetOverwrite(frameName, item, value);
        }
        private int Italian_NTC_2008_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2008.SetPreference(prefItem, prefValue);
        }
        private int Italian_NTC_2008_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2008.SetOverwrite(frameName, item, value);
        }

        private int Italian_NTC_2018_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2018.SetPreference(prefItem, textValue, prefValue);
        }
        private int Italian_NTC_2018_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2018.SetOverwrite(frameName, item, textValue, value);
        }
        private int NewZealand_NZS3404_97_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.NewZealand_NZS3404_97.SetPreference(prefItem, prefValue);
        }
        private int NewZealand_NZS3404_97_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.NewZealand_NZS3404_97.SetOverwrite(frameName, item, value);
        }
        private int SP16_13330_2011_SetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.SP16_13330_2011.SetPreference(prefItem, prefValue);
        }
        private int SP16_13330_2011_SetSteelOverwrite(string frameName, int item, double value, string textValue = "")
        {
            return Doc.Document.DesignSteel.SP16_13330_2011.SetOverwrite(frameName, item, value);
        }

        private DesignSteel SetPrefOverWrite(DesignSteel designSteel,
                Func<int, double, string, int> setPreferece, Func<string, int, double, string, int> setOverWrite)
        {
            if (designSteel.preferenceItem.Count > 0)
            {
                for (int index = 0; index < designSteel.preferenceItem.Count; index++)
                {
                    tryWrapper(() => setPreferece(designSteel.preferenceItem[index],
                        designSteel.preferenceValue[index],
                        designSteel.preferenceTextValue[index]));
                }
            }
            for (int index = 0; index < designSteel.overWrites.Count; index++)
            {

                for (int itemsIndex = 0; itemsIndex < designSteel.overWrites[index].items.Count;
                    itemsIndex++)
                {
                    tryWrapper(() => setOverWrite(
                        designSteel.overWrites[index].frameName,
                        designSteel.overWrites[index].items[itemsIndex],
                        designSteel.overWrites[index].values[itemsIndex],
                        designSteel.overWrites[index].textValues[itemsIndex]));
                }

            }
            return designSteel;
        }

        private DesignSteel SetDesignSteelPrefsOversWrites(DesignSteel designSteel)
        {
            switch (designSteel.codeName)
            {
                case "AISC_LRFD93":
                    return SetPrefOverWrite(designSteel,
                        AISC_LRFD93_SetSteelPreference, AISC_LRFD93_SetSteelOverwrite);
                case "AISC360_05_IBC2006":
                    return SetPrefOverWrite(designSteel,
                        AISC360_05_IBC2006_SetSteelPreference, AISC360_05_IBC2006_SetSteelOverwrite);
                case "Australian_AS4100_98":
                    return SetPrefOverWrite(designSteel,
                        Australian_AS4100_98_SetSteelPreference, Australian_AS4100_98_SetSteelOverwrite);
                case "BS5950_2000":
                    return SetPrefOverWrite(designSteel,
                        BS5950_2000_SetSteelPreference, BS5950_2000_SetSteelOverwrite);
                case "Canadian_S16_09":
                    return SetPrefOverWrite(designSteel,
                        Canadian_S16_09_SetSteelPreference, Canadian_S16_09_SetSteelOverwrite);
                case "Chinese_2010":
                    return SetPrefOverWrite(designSteel,
                        Chinese_2010_SetSteelPreference, Chinese_2010_SetSteelOverwrite);
                case "Chinese_2018":
                    return SetPrefOverWrite(designSteel,
                        Chinese_2018_SetSteelPreference, Chinese_2018_SetSteelOverwrite);
                case "Eurocode_3_2005":
                    return SetPrefOverWrite(designSteel,
                        Eurocode_3_2005_SetSteelPreference, Eurocode_3_2005_SetSteelOverwrite);
                case "Indian_IS_800_2007":
                    return SetPrefOverWrite(designSteel,
                        Indian_IS_800_2007_SetSteelPreference, Indian_IS_800_2007_SetSteelOverwrite);
                case "Italian_NTC_2008":
                    return SetPrefOverWrite(designSteel,
                        Italian_NTC_2008_SetSteelPreference, Italian_NTC_2008_SetSteelOverwrite);
                case "Italian_NTC_2018":
                    return SetPrefOverWrite(designSteel,
                        Italian_NTC_2018_SetSteelPreference, Italian_NTC_2018_SetSteelOverwrite);
                case "NewZealand_NZS3404_97":
                    return SetPrefOverWrite(designSteel,
                        NewZealand_NZS3404_97_SetSteelPreference, NewZealand_NZS3404_97_SetSteelOverwrite);
                case "SP16_13330_2011":
                    return SetPrefOverWrite(designSteel,
                        SP16_13330_2011_SetSteelPreference, SP16_13330_2011_SetSteelOverwrite);
                default:
                    ConversionErrors.Add(new SpeckleException($"Unsupported design steel code named {designSteel.codeName}",
                    level: Sentry.SentryLevel.Warning));
                    return designSteel;
            }
        }

        #endregion
        public object DesignSteelToNative(DesignSteel designSteel)
        {
            bool isCreated = false;
            (designSteel, isCreated) = SetDesignSteelCodeName(designSteel);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native designSteel ${designSteel.codeName}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            designSteel = SetDesignSteelPrefsOversWrites(designSteel);
            return designSteel.codeName;
        }
        #region DesignSteelToSpeckle util functions

        private DesignSteel GetDesignSteelCodeName(DesignSteel designSteel)
        {
            var codeName = "";
            if (tryWrapper(() => Doc.Document.DesignSteel.GetCode(ref codeName)))
            {
                designSteel.codeName = codeName;
            }
            return designSteel;
        }

        private int AISC_LRFD93_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC_LRFD93.GetPreference(prefItem, ref prefValue);
        }
        private int AISC_LRFD93_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC_LRFD93.GetOverwrite(frameName, item, ref value, ref progDet);
        }

        private int AISC360_05_IBC2006_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC360_05_IBC2006.GetPreference(prefItem, ref prefValue);
        }
        private int AISC360_05_IBC2006_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.AISC360_05_IBC2006.GetOverwrite(frameName, item, ref value, ref progDet);
        }

        private int Australian_AS4100_98_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Australian_AS4100_98.GetPreference(prefItem, ref prefValue);
        }
        private int Australian_AS4100_98_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Australian_AS4100_98.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int BS5950_2000_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.BS5950_2000.GetPreference(prefItem, ref prefValue);
        }
        private int BS5950_2000_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.BS5950_2000.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Canadian_S16_09_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Canadian_S16_09.GetPreference(prefItem, ref prefValue);
        }
        private int Canadian_S16_09_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Canadian_S16_09.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Chinese_2010_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2010.GetPreference(prefItem, ref prefValue);
        }
        private int Chinese_2010_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2010.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Chinese_2018_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2018.GetPreference(prefItem, ref prefValue);
        }
        private int Chinese_2018_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Chinese_2018.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Eurocode_3_2005_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Eurocode_3_2005.GetPreference(prefItem, ref prefValue);
        }
        private int Eurocode_3_2005_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Eurocode_3_2005.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Indian_IS_800_2007_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Indian_IS_800_2007.GetPreference(prefItem, ref prefValue);
        }
        private int Indian_IS_800_2007_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Indian_IS_800_2007.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Italian_NTC_2008_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2008.GetPreference(prefItem, ref prefValue);
        }
        private int Italian_NTC_2008_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2008.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int Italian_NTC_2018_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2018.GetPreference(prefItem, ref textValue, ref prefValue);
        }
        private int Italian_NTC_2018_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.Italian_NTC_2018.GetOverwrite(frameName, item, ref textValue, ref value, ref progDet);
        }
        private int NewZealand_NZS3404_97_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.NewZealand_NZS3404_97.GetPreference(prefItem, ref prefValue);
        }
        private int NewZealand_NZS3404_97_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.NewZealand_NZS3404_97.GetOverwrite(frameName, item, ref value, ref progDet);
        }
        private int SP16_13330_2011_GetSteelPreference(int prefItem, double prefValue, string textValue = "")
        {
            return Doc.Document.DesignSteel.SP16_13330_2011.GetPreference(prefItem, ref prefValue);
        }
        private int SP16_13330_2011_GetSteelOverwrite(string frameName, int item, double value, bool progDet, string textValue = "")
        {
            return Doc.Document.DesignSteel.SP16_13330_2011.GetOverwrite(frameName, item, ref value, ref progDet);
        }

        private DesignSteel GetPrefOverWrite(DesignSteel designSteel,
                Func<int, double, string, int> getPreference, Func<string, int, double, bool, string, int> getOverWrite)
        {
            designSteel.preferenceItem = new List<int>();
            designSteel.preferenceValue = new List<double>();
            for (int itemNumber = 0; itemNumber < 200; itemNumber++)
            {
                double value = 0; string textValue = "";
                if (tryWrapper(() => getPreference(itemNumber, value, textValue)))
                {
                    designSteel.preferenceItem.Add(itemNumber);
                    designSteel.preferenceValue.Add(value);
                    if (textValue != "") designSteel.preferenceTextValue.Add(textValue);
                }
                else
                {
                    break;
                }
            }
            List<string> frameNames = ConnectorETABS.ConnectorETABSUtils.GetAllFrameNames(Doc);
            designSteel.overWrites = new List<OverWrite>();
            for (int index = 0; index < frameNames.Count; index++)
            {
                var overWrite = new OverWrite();
                for (int itemNumber = 0; itemNumber < 200; itemNumber++)
                {
                    double value = 0; bool progDet = false; string textValue = "";
                    if (tryWrapper(() => getOverWrite(frameNames[index], itemNumber, value, progDet, textValue)))
                    {
                        overWrite.frameName = frameNames[index];
                        overWrite.items.Add(itemNumber);
                        overWrite.values.Add(value);
                        if (textValue != "") overWrite.textValues.Add(textValue);
                    }
                    else
                    {
                        designSteel.overWrites.Add(overWrite);
                        break;
                    }
                }
            }
            return designSteel;
        }

        private DesignSteel GetDesignSteelPrefsOverWrites(DesignSteel designSteel)
        {
            switch (designSteel.codeName)
            {
                case "AISC_LRFD93":
                    return GetPrefOverWrite(designSteel,
                        AISC_LRFD93_GetSteelPreference, AISC_LRFD93_GetSteelOverwrite);
                case "AISC360_05_IBC2006":
                    return GetPrefOverWrite(designSteel,
                        AISC360_05_IBC2006_GetSteelPreference, AISC360_05_IBC2006_GetSteelOverwrite);
                case "Australian_AS4100_98":
                    return GetPrefOverWrite(designSteel,
                        Australian_AS4100_98_GetSteelPreference, Australian_AS4100_98_GetSteelOverwrite);
                case "BS5950_2000":
                    return GetPrefOverWrite(designSteel,
                        BS5950_2000_GetSteelPreference, BS5950_2000_GetSteelOverwrite);
                case "Canadian_S16_09":
                    return GetPrefOverWrite(designSteel,
                        Canadian_S16_09_GetSteelPreference, Canadian_S16_09_GetSteelOverwrite);
                case "Chinese_2010":
                    return GetPrefOverWrite(designSteel,
                        Chinese_2010_GetSteelPreference, Chinese_2010_GetSteelOverwrite);
                case "Chinese_2018":
                    return GetPrefOverWrite(designSteel,
                        Chinese_2018_GetSteelPreference, Chinese_2018_GetSteelOverwrite);
                case "Eurocode_3_2005":
                    return GetPrefOverWrite(designSteel,
                        Eurocode_3_2005_GetSteelPreference, Eurocode_3_2005_GetSteelOverwrite);
                case "Indian_IS_800_2007":
                    return GetPrefOverWrite(designSteel,
                        Indian_IS_800_2007_GetSteelPreference, Indian_IS_800_2007_GetSteelOverwrite);
                case "Italian_NTC_2008":
                    return GetPrefOverWrite(designSteel,
                        Italian_NTC_2008_GetSteelPreference, Italian_NTC_2008_GetSteelOverwrite);
                case "Italian_NTC_2018":
                    return GetPrefOverWrite(designSteel,
                        Italian_NTC_2018_GetSteelPreference, Italian_NTC_2018_GetSteelOverwrite);
                case "NewZealand_NZS3404_97":
                    return GetPrefOverWrite(designSteel,
                        NewZealand_NZS3404_97_GetSteelPreference, NewZealand_NZS3404_97_GetSteelOverwrite);
                case "SP16_13330_2011":
                    return GetPrefOverWrite(designSteel,
                        SP16_13330_2011_GetSteelPreference, SP16_13330_2011_GetSteelOverwrite);
                default:
                    ConversionErrors.Add(new SpeckleException($"Unsupported design steel code named {designSteel.codeName}",
                    level: Sentry.SentryLevel.Warning));
                    return designSteel;
            }
        }

        #endregion
        public DesignSteel DesignSteelToSpeckle(string name)
        {
            var designSteel = new DesignSteel { codeName = name };
            designSteel = GetDesignSteelCodeName(designSteel);
            designSteel = GetDesignSteelPrefsOverWrites(designSteel);
            return designSteel;
        }
    }
}

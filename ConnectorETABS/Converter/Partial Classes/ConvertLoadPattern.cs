﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using ET = ETABSv1;
using ConnectorETABS;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region LoadPatternToNative util functions

        private bool speckleLoadPatNameExistsInModel(LoadPattern speckleLoadPat)
        {
            var names = ConnectorETABSUtils.GetAllLoadPatternNames(Doc);
            return names.Contains(speckleLoadPat.name);
        }
        private LoadPattern mergeLoadPattern(LoadPattern loadPattern)
        {
            tryWrapper(() => Doc.Document.LoadPatterns.SetLoadType(loadPattern.name,
                    (ET.eLoadPatternType)loadPattern.loadPatterntype));
            tryWrapper(() => Doc.Document.LoadPatterns.SetSelfWTMultiplier(loadPattern.name,
               loadPattern.selfWTMultiplier));
            return loadPattern;
        }
        private LoadPattern AddLoadPattern(LoadPattern loadPattern)
        {
            if (speckleLoadPatNameExistsInModel(loadPattern))
            {
                return mergeLoadPattern(loadPattern);
            }
            if (!tryWrapper(() => Doc.Document.LoadPatterns.Add(loadPattern.name,
                (ET.eLoadPatternType)loadPattern.loadPatterntype, loadPattern.selfWTMultiplier)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native loadPattern ${loadPattern.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return loadPattern;
        }
        #endregion

        public object LoadPatternToNative(LoadPattern loadPattern)
        {
            loadPattern = AddLoadPattern(loadPattern);
            return loadPattern.name;
        }
        #region LoadPatternToSpeckle util functions

        private LoadPattern GetLoadPatternType(LoadPattern loadPattern)
        {
            var loadPatternType = ET.eLoadPatternType.Dead;
            if (tryWrapper(() => Doc.Document.LoadPatterns.GetLoadType(loadPattern.name,
                ref loadPatternType)))
            {
                loadPattern.loadPatterntype = (int)loadPatternType;
            }
            return loadPattern;
        }
        private LoadPattern GetSelfWTMultiplier(LoadPattern loadPattern)
        {
            double selfWTMultiplier = 0;
            if (tryWrapper(() => Doc.Document.LoadPatterns.GetSelfWTMultiplier(loadPattern.name,
                ref selfWTMultiplier)))
            {
                loadPattern.selfWTMultiplier = selfWTMultiplier;
            }
            return loadPattern;
        }

        #endregion

        public LoadPattern LoadPatternToSpeckle(string name)
        {
            var loadPattern = new LoadPattern { name = name };
            var funcList = new List<Func<LoadPattern, LoadPattern>>() { GetLoadPatternType,
                GetSelfWTMultiplier};
            foreach (var fun in funcList)
            {
                loadPattern = fun(loadPattern);
            }
            return loadPattern;
        }
    }
}

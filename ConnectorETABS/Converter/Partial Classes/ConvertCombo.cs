﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using ET = ETABSv1;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region ComboToNative util functions

        private (Combo, bool) SetCombo(Combo combo)
        {
            for (int index = 0; index < combo.numberItems; index++)
            {
                var cNameType = (ET.eCNameType)combo.cNameType[index];
                if (!tryWrapper(() => Doc.Document.RespCombo.SetCaseList_1(combo.name,
                    ref cNameType, combo.cName[index], combo.modeNumber[index], combo.sf[index])))
                {
                    return (combo, false);
                }
                combo.cNameType[index] = (int)cNameType;
            }
            return (combo, true);
        }
        #endregion

        public object ComboToNative(Combo combo)
        {
            bool isCreated = false;
            (combo, isCreated) = SetCombo(combo);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native combo ${combo.name}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            return combo.name;
        }

        #region ComboToSpeckle util functions
        private Combo GetCombo(Combo combo)
        {
            int NumberItems = 0;
            ET.eCNameType[] CNameType = { };
            string[] CName = { };
            int[] ModeNumber = { };
            double[] SF = { };
            if (tryWrapper(() => Doc.Document.RespCombo.GetCaseList_1(combo.name,
                ref NumberItems, ref CNameType, ref CName, ref ModeNumber, ref SF)))
            {
                if (NumberItems > 0)
                {
                    for (int index = 0; index < NumberItems; index++)
                    {
                        combo.cNameType.Add((int)CNameType[index]);
                        combo.cName.Add(CName[index]);
                        combo.modeNumber.Add(ModeNumber[index]);
                        combo.sf.Add(SF[index]);
                    }
                }
            }
            return combo;
        }
        #endregion
        public Combo ComboToSpeckle(string name)
        {
            var combo = new Combo { name = name };
            combo = GetCombo(combo);
            return combo;
        }

    }
}

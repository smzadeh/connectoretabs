﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region AreaToNative util functions

        /// <summary>
        /// Add area by points. If points already exist, their
        /// fields are merged.
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        private (Area, bool) AddByPoint(Area area)
        {
            foreach (var point in area.points)
            {
                ConvertToNative(point);
            }
            string areaName = area.name;
            string[] points = area.points.Select(point => point.name).ToArray();
            if (!tryWrapper(() => Doc.Document.AreaObj.AddByPoint(area.points.Count,
                ref points, ref areaName)))
            {
                return (area, false);
            }
            area.name = areaName;
            return (area, true);
        }

        private Area SetProperty(Area area)
        {
            if (area.section != null)
            {
                ConvertToNative(area.section);
                tryWrapper(() => Doc.Document.AreaObj.SetProperty(area.name,
                area.section.name));
            }
            return area;
        }

        private Area SetDiaphragm(Area area)
        {
            if (area.diaphragm != null)
            {
                ConvertToNative(area.diaphragm);
                tryWrapper(() => Doc.Document.AreaObj.SetDiaphragm(area.name,
                area.diaphragm.name));
            }
            return area;
        }
        private Area SetEdgeConstraint(Area area)
        {
            if (area.edgeConstraintExists != null)
            {
                tryWrapper(() => Doc.Document.AreaObj.SetEdgeConstraint(area.name,
                (bool)area.edgeConstraintExists));
            }
            return area;
        }
        private Area SetGroupAssign(Area area)
        {
            if (area.groupAssign != null)
            {
                foreach (var group in area.groupAssign.groupAssign)
                {
                    tryWrapper(() => Doc.Document.AreaObj.SetGroupAssign(area.name, group));
                }
            }
            return area;
        }

        private Area SetLoadTemperature(Area area)
        {
            if (area.loadTemperature != null)
            {
                for (int index = 0; index < area.loadTemperature.numberItems; index++)
                {
                    LoadPatternToNative(area.loadTemperature.loadPat[index]);
                    tryWrapper(() => Doc.Document.AreaObj.SetLoadTemperature(area.name,
                        area.loadTemperature.loadPat[index].name, area.loadTemperature.myType[index],
                        area.loadTemperature.val[index]));
                }
            }
            return area;
        }
        private Area SetLoadUniform(Area area)
        {
            if (area.loadUniform != null)
            {
                for (int index = 0; index < area.loadUniform.numberItems; index++)
                {
                    LoadPatternToNative(area.loadUniform.loadPat[index]);
                    tryWrapper(() => Doc.Document.AreaObj.SetLoadUniform(area.name,
                        area.loadUniform.loadPat[index].name, area.loadUniform.value[index],
                        area.loadUniform.dir[index]));
                }
            }
            return area;
        }
        private Area SetLoadUniformToFrame(Area area)
        {
            if (area.loadUniformToFrame != null)
            {
                for (int index = 0; index < area.loadUniformToFrame.numberItems; index++)
                {
                    LoadPatternToNative(area.loadUniformToFrame.loadPat[index]);
                    tryWrapper(() => Doc.Document.AreaObj.SetLoadUniformToFrame(area.name,
                        area.loadUniformToFrame.loadPat[index].name, area.loadUniformToFrame.value[index],
                        area.loadUniformToFrame.dir[index], area.loadUniformToFrame.distType[index]));
                }
            }
            return area;
        }

        private Area SetLoadWindPressure(Area area)
        {
            if (area.loadWindPressure != null)
            {
                for (int index = 0; index < area.loadWindPressure.numberItems; index++)
                {
                    LoadPatternToNative(area.loadWindPressure.loadPat[index]);
                    tryWrapper(() => Doc.Document.AreaObj.SetLoadWindPressure(area.name,
                        area.loadWindPressure.loadPat[index].name, area.loadWindPressure.myType[index],
                        area.loadWindPressure.cp[index]));
                }
            }
            return area;
        }
        private Area SetLocalAxes(Area area)
        {
            if (area.localAxes != null)
            {
                tryWrapper(() => Doc.Document.AreaObj.SetLocalAxes(area.name,
                    area.localAxes.ang));
            }
            return area;
        }
        private Area SetMass(Area area)
        {
            if (area.mass != null)
            {
                tryWrapper(() => Doc.Document.AreaObj.SetMass(area.name,
                    (double)area.mass));
            }
            return area;
        }
        private Area SetMaterialOverwrite(Area area)
        {
            if (area.materialOverwrite != null)
            {
                tryWrapper(() => Doc.Document.AreaObj.SetMaterialOverwrite(area.name,
                    area.materialOverwrite.propName));
            }
            return area;
        }

        private Area SetModifiers(Area area)
        {
            if (area.modifiers != null)
            {
                double[] mod = area.modifiers.value.ToArray();
                tryWrapper(() => Doc.Document.AreaObj.SetModifiers(area.name,
                    ref mod));
            }
            return area;
        }
        private Area SetOpening(Area area)
        {
            if (area.opening != null)
            {
                double[] mod = area.modifiers.value.ToArray();
                tryWrapper(() => Doc.Document.AreaObj.SetOpening(area.name,
                    area.opening.isOpening));
            }
            return area;
        }
        private Area SetPier(Area area)
        {
            if (area.pier != null)
            {
                tryWrapper(() => Doc.Document.AreaObj.SetPier(area.name,
                    area.pier.pierName));
            }
            return area;
        }
        private Area SetSpandrel(Area area)
        {
            if (area.spandrel != null)
            {
                tryWrapper(() => Doc.Document.AreaObj.SetSpandrel(area.name,
                    area.spandrel.spandrelName));
            }
            return area;
        }

        private Area SetSpringAssignment(Area area)
        {
            if (area.springAssignment != null)
            {
                tryWrapper(() => Doc.Document.AreaObj.SetSpringAssignment(area.name,
                    area.springAssignment.springProp));
            }
            return area;
        }

        #endregion
        public object AreaToNative(Area area)
        {
            bool isCreated = false;
            (area, isCreated) = AddByPoint(area);
            if (!isCreated)
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native area ${area.name}",
                    level: Sentry.SentryLevel.Warning));
                return null;
            }
            var funcList = new List<Func<Area, Area>>() { SetProperty,
                SetDiaphragm, SetEdgeConstraint, SetGroupAssign, SetLoadTemperature,
                SetLoadUniform, SetLoadUniformToFrame, SetLoadWindPressure, SetLocalAxes,
                SetMass, SetMaterialOverwrite, SetModifiers, SetOpening,
                SetPier, SetSpandrel, SetSpringAssignment};
            foreach (var fun in funcList)
            {
                area = fun(area);
            }
            return area.name;
        }
        #region AreaToSpeckle util functions

        private Area GetPoints(Area area)
        {
            int numberPoints = 0;
            string[] points = { };
            if (tryWrapper(() => Doc.Document.AreaObj.GetPoints(area.name,
                ref numberPoints, ref points)))
            {
                if (numberPoints > 0)
                {
                    for (int index = 0; index < numberPoints; index++)
                    {
                        area.points.Add(PointToSpeckle(points[index]));
                    }
                }
            }
            return area;
        }
        private Area GetSection(Area area)
        {
            string section = "";
            if (tryWrapper(() => Doc.Document.AreaObj.GetProperty(area.name,
                ref section)))
            {
                area.section = PropAreaToSpeckle(section);
            }
            return area;
        }
        private Area GetDiaphragm(Area area)
        {
            string diaphragmName = "";
            if (tryWrapper(() => Doc.Document.AreaObj.GetDiaphragm(area.name,
                ref diaphragmName)))
            {
                area.diaphragm = DiaphragmToSpeckle(diaphragmName);
            }
            return area;
        }
        private Area GetEdgeConstraint(Area area)
        {
            bool edgeConstraintExists = false;
            if (tryWrapper(() => Doc.Document.AreaObj.GetEdgeConstraint(area.name,
                ref edgeConstraintExists)))
            {
                area.edgeConstraintExists = edgeConstraintExists;
            }
            return area;
        }

        private Area GetGroupAssign(Area area)
        {
            var groupAssign = new GroupAssign();
            int numberGroups = 0;
            string[] Groups = { };
            if (tryWrapper(() => Doc.Document.AreaObj.GetGroupAssign(area.name,
                ref numberGroups, ref Groups)))
            {
                if (numberGroups > 0)
                {
                    area.groupAssign = groupAssign;
                    foreach (var group in Groups)
                    {
                        area.groupAssign.groupAssign.Add(group);
                    }
                }
            }
            return area;
        }

        private Area GetLoadTemperature(Area area)
        {
            var loadTemperature = new LoadTemperature();
            int numberItems = 0;
            string[] areaName = { }, loadPat = { }, PatternName = { };
            int[] myType = { }, dir = { };
            double[] Val = { };
            if (tryWrapper(() => Doc.Document.AreaObj.GetLoadTemperature(area.name,
                    ref numberItems, ref areaName, ref loadPat, ref myType, ref Val,
                    ref PatternName)))
            {
                if (numberItems > 0)
                {
                    area.loadTemperature = loadTemperature;
                    area.loadTemperature.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        area.loadTemperature.name.Add(areaName[index]);
                        area.loadTemperature.loadPat.Add(LoadPatternToSpeckle(loadPat[index]));
                        area.loadTemperature.myType.Add(myType[index]);
                        area.loadTemperature.val.Add(Val[index]);
                        area.loadTemperature.patternName.Add(PatternName[index]);
                    }
                }
            }
            return area;
        }

        private Area GetLoadUniform(Area area)
        {
            var loadUniform = new LoadUniform();
            int numberItems = 0;
            string[] areaName = { }, loadPat = { }, cSys = { };
            int[] myType = { }, dir = { };
            double[] Val = { };
            if (tryWrapper(() => Doc.Document.AreaObj.GetLoadUniform(area.name,
                    ref numberItems, ref areaName, ref loadPat, ref cSys, ref dir,
                    ref Val)))
            {
                if (numberItems > 0)
                {
                    area.loadUniform = loadUniform;
                    area.loadUniform.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        area.loadUniform.areaName.Add(areaName[index]);
                        area.loadUniform.loadPat.Add(LoadPatternToSpeckle(loadPat[index]));
                        area.loadUniform.cSys.Add(cSys[index]);
                        area.loadUniform.dir.Add(dir[index]);
                        area.loadUniform.value.Add(Val[index]);
                    }
                }
            }
            return area;
        }

        private Area GetLoadUniformToFrame(Area area)
        {
            var loadUniformToFrame = new LoadUniformToFrame();
            int numberItems = 0;
            string[] areaName = { }, loadPat = { }, cSys = { };
            int[] myType = { }, dir = { }, distType = { };
            double[] Val = { };
            if (tryWrapper(() => Doc.Document.AreaObj.GetLoadUniformToFrame(area.name,
                    ref numberItems, ref areaName, ref loadPat, ref cSys, ref dir,
                    ref Val, ref distType)))
            {
                if (numberItems > 0)
                {
                    area.loadUniformToFrame = loadUniformToFrame;
                    area.loadUniformToFrame.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        area.loadUniformToFrame.areaName.Add(areaName[index]);
                        area.loadUniformToFrame.loadPat.Add(LoadPatternToSpeckle(loadPat[index]));
                        area.loadUniformToFrame.cSys.Add(cSys[index]);
                        area.loadUniformToFrame.dir.Add(dir[index]);
                        area.loadUniformToFrame.value.Add(Val[index]);
                        area.loadUniformToFrame.distType.Add(distType[index]);
                    }
                }
            }
            return area;
        }

        private Area GetLoadWindPressure(Area area)
        {
            var loadWindPressure = new LoadWindPressure();
            int numberItems = 0;
            string[] areaName = { }, loadPat = { }, cSys = { };
            int[] myType = { };
            double[] cp = { };
            if (tryWrapper(() => Doc.Document.AreaObj.GetLoadWindPressure(area.name,
                    ref numberItems, ref areaName, ref loadPat, ref myType, ref cp)))
            {
                if (numberItems > 0)
                {
                    area.loadWindPressure = loadWindPressure;
                    area.loadWindPressure.numberItems = numberItems;
                    for (int index = 0; index < numberItems; index++)
                    {
                        area.loadWindPressure.areaName.Add(areaName[index]);
                        area.loadWindPressure.loadPat.Add(LoadPatternToSpeckle(loadPat[index]));
                        area.loadWindPressure.myType.Add(myType[index]);
                        area.loadWindPressure.cp.Add(cp[index]);
                    }
                }
            }
            return area;
        }

        private Area GetLocalAxes(Area area)
        {
            var localAxes = new LocalAxes();
            double ang = 0; bool advanced = false;
            if (tryWrapper(() => Doc.Document.AreaObj.GetLocalAxes(area.name,
                ref ang, ref advanced)))
            {
                area.localAxes = localAxes;
                area.localAxes.ang = ang; area.localAxes.advanced = advanced;
            }
            return area;
        }

        private Area GetMass(Area area)
        {
            double mass = 0;
            if (tryWrapper(() => Doc.Document.AreaObj.GetMass(area.name,
                ref mass)))
            {
                area.mass = mass;
            }
            return area;
        }

        private Area GetMaterialOverwrite(Area area)
        {
            var materialOverwrite = new MaterialOverwrite();
            string propName = "";
            if (tryWrapper(() => Doc.Document.AreaObj.GetMaterialOverwrite(area.name,
                ref propName)))
            {
                area.materialOverwrite = materialOverwrite;
                area.materialOverwrite.propName = propName;
            }
            return area;
        }
        private Area GetModifiers(Area area)
        {
            var modifiers = new Modifiers();
            double[] mod = { };
            if (tryWrapper(() => Doc.Document.AreaObj.GetModifiers(area.name,
                ref mod)))
            {
                foreach (var item in mod.ToList())
                {
                    area.modifiers = modifiers;
                    area.modifiers.value.Add(item);
                }
            }
            return area;
        }
        private Area GetOpening(Area area)
        {
            var opening = new Opening();
            bool isOpening = false;
            if (tryWrapper(() => Doc.Document.AreaObj.GetOpening(area.name,
                ref isOpening)))
            {
                area.opening = opening;
                area.opening.isOpening = isOpening;
            }
            return area;
        }
        private Area GetPier(Area area)
        {
            var pier = new Pier();
            string pierName = "";
            if (tryWrapper(() => Doc.Document.AreaObj.GetPier(area.name, ref pierName)))
            {
                area.pier = pier;
                area.pier.pierName = pierName;
            }
            return area;
        }

        private Area GetSpandrel(Area area)
        {
            var spandrel = new Spandrel();
            string spandrelName = "";
            if (tryWrapper(() => Doc.Document.AreaObj.GetSpandrel(area.name,
                ref spandrelName)))
            {
                area.spandrel = spandrel;
                area.spandrel.spandrelName = spandrelName;
            }
            return area;
        }
        private Area GetSpringAssignment(Area area)
        {
            var springAssignment = new SpringAssignment();
            string springProp = "";
            if (tryWrapper(() => Doc.Document.AreaObj.GetSpringAssignment(area.name,
                ref springProp)))
            {
                area.springAssignment = springAssignment;
                area.springAssignment.springProp = springProp;
            }
            return area;
        }

        #endregion
        public Area AreaToSpeckle(string name)
        {
            var area = new Area { name = name };
            var funcList = new List<Func<Area, Area>>() { GetPoints, GetSection, GetDiaphragm,
                GetEdgeConstraint, GetGroupAssign, GetLoadTemperature, GetLoadUniform,
                GetLoadUniformToFrame, GetLoadWindPressure, GetLocalAxes, GetMass,
                GetMaterialOverwrite, GetModifiers, GetOpening, GetPier, GetSpandrel,
                GetSpringAssignment};
            foreach (var fun in funcList)
            {
                area = fun(area);
            }
            return area;
        }
    }
}

﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region PropAreaSpringToNative util functions

        private PropAreaSpring SetPropAreaSpring(PropAreaSpring propAreaSpring)
        {
            if (!tryWrapper(() => Doc.Document.PropAreaSpring.SetAreaSpringProp(propAreaSpring.name,
                propAreaSpring.u1, propAreaSpring.u2, propAreaSpring.u3, propAreaSpring.nonlinearOption3,
                propAreaSpring.springOption, propAreaSpring.soilProfile, propAreaSpring.endLengthRatio,
                propAreaSpring.period, propAreaSpring.color, propAreaSpring.notes, propAreaSpring.iGUID)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native propAreaSpring ${propAreaSpring.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return propAreaSpring;
        }
        #endregion
        public object PropAreaSpringToNative(PropAreaSpring propAreaSpring)
        {
            propAreaSpring = SetPropAreaSpring(propAreaSpring);
            return propAreaSpring.name;
        }

        #region PropAreaSpringToSpeckle util functions

        private PropAreaSpring GetPropAreaSpring(PropAreaSpring propAreaSpring)
        {
            string soilProfile = "", notes = "", iGUID = "";
            double u1 = 0, u2 = 0, u3 = 0, endLengthRatio = 0, period = 0;
            int nonlinearOption3 = 0, springOption = 0, color = 0;

            if (tryWrapper(() => Doc.Document.PropAreaSpring.GetAreaSpringProp(propAreaSpring.name,
                ref u1, ref u2, ref u3, ref nonlinearOption3, ref springOption, ref soilProfile,
                ref endLengthRatio, ref period, ref color, ref notes, ref iGUID)))
            {
                propAreaSpring.u1 = u1; propAreaSpring.u2 = u2; propAreaSpring.u3 = u3;
                propAreaSpring.nonlinearOption3 = nonlinearOption3;
                propAreaSpring.springOption = springOption;
                propAreaSpring.soilProfile = soilProfile;
                propAreaSpring.endLengthRatio = endLengthRatio;
                propAreaSpring.period = period; propAreaSpring.color = color;
                propAreaSpring.notes = notes; propAreaSpring.iGUID = iGUID;
            }
            return propAreaSpring;
        }
        #endregion
        public PropAreaSpring PropAreaSpringToSpeckle(string name)
        {
            var propAreaSpring = new PropAreaSpring { name = name };
            propAreaSpring = GetPropAreaSpring(propAreaSpring);
            return propAreaSpring;
        }
    }
}

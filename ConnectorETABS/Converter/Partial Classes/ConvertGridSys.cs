﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region GridSysToNative util functions

        private GridSys SetGridSys(GridSys gridSys)
        {
            if (!tryWrapper(() => Doc.Document.GridSys.SetGridSys(gridSys.name,
                gridSys.x, gridSys.y, gridSys.rz)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native gridSys ${gridSys.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return gridSys;
        }
        #endregion
        public object GridSysToNative(GridSys gridSys)
        {
            gridSys = SetGridSys(gridSys);
            return gridSys.name;
        }

        #region GridSysToSpeckle util functions

        private GridSys GetGridSys(GridSys gridSys)
        {
            double x = 0, y = 0, rz = 0;
            if (tryWrapper(() => Doc.Document.GridSys.GetGridSys(gridSys.name,
                ref x, ref y, ref rz)))
            {
                gridSys.x = x;
                gridSys.y = y;
                gridSys.rz = rz;
            }
            return gridSys;
        }
        #endregion
        public GridSys GridSysToSpeckle(string name)
        {
            var gridSys = new GridSys { name = name };
            gridSys = GetGridSys(gridSys);
            return gridSys;
        }
    }
}

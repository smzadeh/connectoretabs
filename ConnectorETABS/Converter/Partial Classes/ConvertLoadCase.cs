﻿using Objects.ETABSObjects;
using Speckle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using ET = ETABSv1;

namespace Objects.Converter.ETABS
{
    public partial class ConverterETABS
    {
        #region LoadCaseToNative util functions
        private LoadCase SetDesignType(LoadCase loadCase)
        {
            if (!tryWrapper(() => Doc.Document.LoadCases.SetDesignType(loadCase.name,
                loadCase.designTypeOption, (ET.eLoadPatternType)loadCase.designType)))
            {
                ConversionErrors.Add(new SpeckleException($"Failed to create native loadCase ${loadCase.name}",
                    level: Sentry.SentryLevel.Warning));
            }
            return loadCase;
        }

        private LoadCase SetStaticLinear(LoadCase loadCase)
        {
            if (loadCase.StaticLinear != null)
            {
                tryWrapper(() => Doc.Document.LoadCases.StaticLinear.SetInitialCase(loadCase.name,
                    loadCase.StaticLinear.InitialCase));
                string[] loadType = loadCase.StaticLinear.LoadType.ToArray();
                string[] loadName = loadCase.StaticLinear.LoadName.ToArray();
                double[] SF = loadCase.StaticLinear.SF.ToArray();
                int numberLoads = loadCase.StaticLinear.NumberLoads;
                tryWrapper(() => Doc.Document.LoadCases.StaticLinear.SetLoads(loadCase.name,
                    numberLoads, ref loadType, ref loadName, ref SF));
            }
            return loadCase;
        }
        private LoadCase SetStaticNonlinear(LoadCase loadCase)
        {
            if (loadCase.StaticNonlinear != null)
            {
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetGeometricNonlinearity(loadCase.name,
                    loadCase.StaticNonlinear.NLGeomType));
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetHingeUnloading(loadCase.name,
                    loadCase.StaticNonlinear.HingeUnloading));
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetInitialCase(loadCase.name,
                        loadCase.StaticNonlinear.InitialCase));
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetLoadApplication(loadCase.name,
                        loadCase.StaticNonlinear.LoadApplication.LoadControl,
                        loadCase.StaticNonlinear.LoadApplication.DispType,
                        loadCase.StaticNonlinear.LoadApplication.Displ,
                        loadCase.StaticNonlinear.LoadApplication.Monitor,
                        loadCase.StaticNonlinear.LoadApplication.DOF,
                        loadCase.StaticNonlinear.LoadApplication.PointName,
                        loadCase.StaticNonlinear.LoadApplication.GDispl));
                string[] loadType = loadCase.StaticNonlinear.LoadType.ToArray();
                string[] loadName = loadCase.StaticNonlinear.LoadName.ToArray();
                double[] SF = loadCase.StaticNonlinear.SF.ToArray();
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetLoads(loadCase.name,
                    loadCase.StaticNonlinear.NumberLoads, ref loadType, ref loadName, ref SF));
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetMassSource(loadCase.name,
                    loadCase.StaticNonlinear.MassSource));
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetModalCase(loadCase.name,
                    loadCase.StaticNonlinear.ModalCase));
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetResultsSaved(loadCase.name,
                    loadCase.StaticNonlinear.SaveMultipleSteps,
                    loadCase.StaticNonlinear.MinSavedStates,
                    loadCase.StaticNonlinear.MaxSavedStates,
                    loadCase.StaticNonlinear.PositiveOnly));
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.SetSolControlParameters(loadCase.name,
                    loadCase.StaticNonlinear.MaxTotalSteps, loadCase.StaticNonlinear.MaxFailedSubSteps,
                    loadCase.StaticNonlinear.MaxIterCS, loadCase.StaticNonlinear.MaxIterNR,
                    loadCase.StaticNonlinear.TolConvD, loadCase.StaticNonlinear.UseEventStepping,
                    loadCase.StaticNonlinear.TolEventD,
                    loadCase.StaticNonlinear.MaxLineSearchPerIter, loadCase.StaticNonlinear.TolLineSearch,
                    loadCase.StaticNonlinear.LineSearchStepFact));
                double TolConvF = loadCase.StaticNonlinear.TolConvF;
                double AccelFact = loadCase.StaticNonlinear.AccelFact;
                int MaxIter = loadCase.StaticNonlinear.MaxIter;
                bool NoStop = loadCase.StaticNonlinear.NoStop;
                tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetTargetForceParameters(loadCase.name,
                    ref TolConvF, ref MaxIter, ref AccelFact, ref NoStop));
            }
            return loadCase;
        }

        private LoadCase SetModalHistoryLinear(LoadCase loadCase)
        {
            if (loadCase.ModalHistoryLinear != null)
            {
                tryWrapper(() => Doc.Document.LoadCases.ModHistLinear.SetCase(loadCase.name));
                string[] LoadType = loadCase.ModalHistoryLinear.LoadType.ToArray();
                string[] LoadName = loadCase.ModalHistoryLinear.LoadName.ToArray();
                string[] Func = loadCase.ModalHistoryLinear.Func.ToArray();
                double[] SF = loadCase.ModalHistoryLinear.SF.ToArray();
                double[] Tf = loadCase.ModalHistoryLinear.Tf.ToArray();
                double[] At = loadCase.ModalHistoryLinear.At.ToArray();
                string[] CSys = loadCase.ModalHistoryLinear.CSys.ToArray();
                double[] Ang = loadCase.ModalHistoryLinear.Ang.ToArray();
                tryWrapper(() => Doc.Document.LoadCases.ModHistLinear.SetLoads(loadCase.name,
                    loadCase.ModalHistoryLinear.NumberLoads, ref LoadType, ref LoadName,
                    ref Func, ref SF, ref Tf, ref At, ref CSys, ref Ang));
            }
            return loadCase;
        }
        private LoadCase SetResponseSpectrum(LoadCase loadCase)
        {
            if (loadCase.ResponseSpectrum != null)
            {
                tryWrapper(() => Doc.Document.LoadCases.ResponseSpectrum.SetCase(loadCase.name));
                tryWrapper(() => Doc.Document.LoadCases.ResponseSpectrum.SetEccentricity(loadCase.name,
                    loadCase.ResponseSpectrum.Eccentricity));
                string[] LoadName = loadCase.ResponseSpectrum.LoadName.ToArray();
                string[] Func = loadCase.ResponseSpectrum.Func.ToArray();
                double[] SF = loadCase.ResponseSpectrum.SF.ToArray();
                string[] CSys = loadCase.ResponseSpectrum.CSys.ToArray();
                double[] Ang = loadCase.ResponseSpectrum.Ang.ToArray();
                tryWrapper(() => Doc.Document.LoadCases.ResponseSpectrum.SetLoads(loadCase.name,
                    loadCase.ResponseSpectrum.NumberLoads, ref LoadName,
                    ref Func, ref SF, ref CSys, ref Ang));
                tryWrapper(() => Doc.Document.LoadCases.ResponseSpectrum.SetModalCase(loadCase.name,
                    loadCase.ResponseSpectrum.ModalCase));
            }
            return loadCase;
        }
        private LoadCase SetHyperStatic(LoadCase loadCase)
        {
            if (loadCase.HyperStatic != null)
            {
                tryWrapper(() => Doc.Document.LoadCases.HyperStatic.SetCase(loadCase.name));
                tryWrapper(() => Doc.Document.LoadCases.HyperStatic.SetBaseCase(loadCase.name,
                    loadCase.HyperStatic.BaseCase));
            }
            return loadCase;
        }

        #endregion
        public object LoadCaseToNative(LoadCase loadCase)
        {
            var funcList = new List<Func<LoadCase, LoadCase>>(){ SetDesignType , SetStaticLinear,
                SetStaticNonlinear, SetModalHistoryLinear, SetResponseSpectrum};
            foreach (var fun in funcList)
            {
                loadCase = fun(loadCase);
            }
            return loadCase.name;
        }

        #region LoadCaseToSpeckle util functions

        private LoadCase GetLoadCase(LoadCase loadCase)
        {
            ET.eLoadCaseType loadCaseType = ET.eLoadCaseType.LinearStatic;
            ET.eLoadPatternType loadPattern = ET.eLoadPatternType.Dead;
            int subType = 0, designTypeOption = 0, auto = 0;
            if (tryWrapper(() => Doc.Document.LoadCases.GetTypeOAPI_1(loadCase.name,
                ref loadCaseType, ref subType, ref loadPattern, ref designTypeOption, ref auto)))
            {
                loadCase.caseType = (int)loadCaseType;
                loadCase.caseSubType = subType;
                loadCase.designType = (int)loadPattern;
                loadCase.designTypeOption = designTypeOption;
            }
            return loadCase;
        }

        private LoadCase GetStaticLinear(LoadCase loadCase)
        {
            {
                string initialCase = "";
                if (tryWrapper(() => Doc.Document.LoadCases.StaticLinear.GetInitialCase(loadCase.name,
                    ref initialCase)))
                {
                    loadCase.StaticLinear.InitialCase = initialCase;
                }
            }
            {
                int NumberLoads = 0;
                string[] LoadType = { }, LoadName = { }; double[] SF = { };
                if (tryWrapper(() => Doc.Document.LoadCases.StaticLinear.GetLoads(loadCase.name,
                    ref NumberLoads, ref LoadType, ref LoadName, ref SF)))
                {
                    loadCase.StaticLinear.NumberLoads = NumberLoads;
                    if (NumberLoads > 0)
                        for (int index = 0; index < NumberLoads; index++)
                        {
                            loadCase.StaticLinear.LoadType.Add(LoadType[index]);
                            loadCase.StaticLinear.LoadName.Add(LoadName[index]);
                            loadCase.StaticLinear.SF.Add(SF[index]);
                        }
                }
            }
            return loadCase;
        }

        private LoadCase GetStaticNonlinearTargetForceParameters(LoadCase loadCase)
        {
            double TolConvF = 0; int MaxIter = 0; double AccelFact = 0; bool NoStop = false;
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetTargetForceParameters(loadCase.name,
                ref TolConvF, ref MaxIter, ref AccelFact, ref NoStop)))
            {
                loadCase.StaticNonlinear.TolConvF = TolConvF;
                loadCase.StaticNonlinear.MaxIter = MaxIter;
                loadCase.StaticNonlinear.AccelFact = AccelFact;
                loadCase.StaticNonlinear.NoStop = NoStop;
            }
            return loadCase;
        }

        private LoadCase GetStaticNonlinearSolControlParameters(LoadCase loadCase)
        {
            int MaxTotalSteps = 0, MaxFailedSubSteps = 0, MaxIterCS = 0, MaxIterNR = 0;
            int MaxLineSearchPerIter = 0;
            double TolConvD = 0, TolEventD = 0, TolLineSearch = 0, LineSearchStepFact = 0;
            bool UseEventStepping = false;
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetSolControlParameters(loadCase.name,
                ref MaxTotalSteps, ref MaxFailedSubSteps, ref MaxIterCS, ref MaxIterNR,
                ref TolConvD, ref UseEventStepping, ref TolEventD, ref MaxLineSearchPerIter,
                ref TolLineSearch, ref LineSearchStepFact)))
            {
                loadCase.StaticNonlinear.MaxTotalSteps = MaxTotalSteps;
                loadCase.StaticNonlinear.MaxFailedSubSteps = MaxFailedSubSteps;
                loadCase.StaticNonlinear.MaxIterCS = MaxIterCS;
                loadCase.StaticNonlinear.MaxIterNR = MaxIterNR;
                loadCase.StaticNonlinear.TolConvD = TolConvD;
                loadCase.StaticNonlinear.UseEventStepping = UseEventStepping;
                loadCase.StaticNonlinear.TolEventD = TolEventD;
                loadCase.StaticNonlinear.MaxLineSearchPerIter = MaxLineSearchPerIter;
                loadCase.StaticNonlinear.TolLineSearch = TolLineSearch;
                loadCase.StaticNonlinear.LineSearchStepFact = LineSearchStepFact;
            }
            return loadCase;
        }
        private LoadCase GetStaticNonlinearResultsSaved(LoadCase loadCase)
        {
            bool SaveMultipleSteps = false, PositiveOnly = true;
            int MinSavedStates = 10, MaxSavedStates = 100;
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetResultsSaved(loadCase.name,
                ref SaveMultipleSteps, ref MinSavedStates, ref MaxSavedStates, ref PositiveOnly)))
            {
                loadCase.StaticNonlinear.SaveMultipleSteps = SaveMultipleSteps;
                loadCase.StaticNonlinear.MinSavedStates = MinSavedStates;
                loadCase.StaticNonlinear.MaxSavedStates = MaxSavedStates;
                loadCase.StaticNonlinear.PositiveOnly = PositiveOnly;
            }
            return loadCase;
        }


        private LoadCase GetStaticNonlinearModalCase(LoadCase loadCase)
        {
            string modalCase = "";
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetModalCase(loadCase.name,
                ref modalCase)))
            {
                loadCase.StaticNonlinear.ModalCase = modalCase;
            }
            return loadCase;
        }

        private LoadCase GetStaticNonlinearMassSource(LoadCase loadCase)
        {
            string massSource = "";
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetMassSource(loadCase.name,
                ref massSource)))
            {
                loadCase.StaticNonlinear.MassSource = massSource;
            }
            return loadCase;
        }

        private LoadCase GetStaticNonlinearLoads(LoadCase loadCase)
        {
            int numberLoads = 0;
            string[] loadType = { }, loadName = { };
            double[] SF = { };
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetLoads(loadCase.name,
                ref numberLoads, ref loadType, ref loadName, ref SF)))
            {
                var loadApplication = new LoadApplication();
                loadCase.StaticNonlinear.NumberLoads = numberLoads;
                loadCase.StaticNonlinear.LoadType = loadType.ToList();
                loadCase.StaticNonlinear.LoadName = loadName.ToList();
                loadCase.StaticNonlinear.SF = SF.ToList();
            }
            return loadCase;
        }
        private LoadCase GetStaticNonlinearLoadApplication(LoadCase loadCase)
        {
            int LoadControl = 0, DispType = 0, Monitor = 0, DOF = 0;
            double Displ = 0;
            string PointName = "", GDispl = "";
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetLoadApplication(loadCase.name,
                ref LoadControl, ref DispType, ref Displ, ref Monitor, ref DOF, ref PointName, ref GDispl)))
            {
                var loadApplication = new LoadApplication();
                loadCase.StaticNonlinear.LoadApplication = loadApplication;
                loadCase.StaticNonlinear.LoadApplication.LoadControl = LoadControl;
                loadCase.StaticNonlinear.LoadApplication.DispType = DispType;
                loadCase.StaticNonlinear.LoadApplication.Displ = Displ;
                loadCase.StaticNonlinear.LoadApplication.Monitor = Monitor;
                loadCase.StaticNonlinear.LoadApplication.DOF = DOF;
                loadCase.StaticNonlinear.LoadApplication.PointName = PointName;
                loadCase.StaticNonlinear.LoadApplication.GDispl = GDispl;
            }
            return loadCase;
        }
        private LoadCase GetStaticNonlinearInitialCase(LoadCase loadCase)
        {
            string initialCase = "";
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetInitialCase(loadCase.name,
                ref initialCase)))
            {
                loadCase.StaticNonlinear.InitialCase = initialCase;
            }
            return loadCase;
        }

        private LoadCase GetStaticNonlinearHingeUnloading(LoadCase loadCase)
        {
            int UnloadType = 0;
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetHingeUnloading(loadCase.name,
                ref UnloadType)))
            {
                loadCase.StaticNonlinear.HingeUnloading = UnloadType;
            }
            return loadCase;
        }
        private LoadCase GetStaticNonlinearGeometricNonlinearity(LoadCase loadCase)
        {
            int NLGeomType = 0;
            if (tryWrapper(() => Doc.Document.LoadCases.StaticNonlinear.GetGeometricNonlinearity(loadCase.name,
                ref NLGeomType)))
            {
                loadCase.StaticNonlinear.NLGeomType = NLGeomType;
            }
            return loadCase;
        }



        private LoadCase GetStaticNonlinear(LoadCase loadCase)
        {
            var funcList = new List<Func<LoadCase, LoadCase>>(){
                GetStaticNonlinearGeometricNonlinearity,
            GetStaticNonlinearHingeUnloading,
            GetStaticNonlinearInitialCase,
            GetStaticNonlinearLoadApplication,
            GetStaticNonlinearLoads,
            GetStaticNonlinearMassSource,
            GetStaticNonlinearModalCase,
            GetStaticNonlinearResultsSaved,
            GetStaticNonlinearSolControlParameters,
            GetStaticNonlinearTargetForceParameters};
            foreach (var fun in funcList)
            {
                loadCase = fun(loadCase);
            }
            return loadCase;
        }
        private LoadCase GetModalHistoryLinear(LoadCase loadCase)
        {
            {
                string[] LoadType = { }, LoadName = { }, Func = { }, CSys = { };
                double[] SF = { }, Tf = { }, At = { }, Ang = { };
                int numberLoads = 0;
                if (tryWrapper(() => Doc.Document.LoadCases.ModHistLinear.GetLoads(loadCase.name,
                    ref numberLoads, ref LoadType, ref LoadName, ref Func, ref SF, ref Tf,
                    ref At, ref CSys, ref Ang)))
                {
                    if (numberLoads > 0)
                    {
                        loadCase.ModalHistoryLinear = new ModalHistoryLinear();
                        loadCase.ModalHistoryLinear.NumberLoads = numberLoads;
                        loadCase.ModalHistoryLinear.LoadType = LoadType.ToList();
                        loadCase.ModalHistoryLinear.LoadName = LoadName.ToList();
                        loadCase.ModalHistoryLinear.Func = Func.ToList();
                        loadCase.ModalHistoryLinear.SF = SF.ToList();
                        loadCase.ModalHistoryLinear.Tf = Tf.ToList();
                        loadCase.ModalHistoryLinear.At = At.ToList();
                        loadCase.ModalHistoryLinear.CSys = CSys.ToList();
                        loadCase.ModalHistoryLinear.Ang = Ang.ToList();
                    }
                }
            }
            return loadCase;
        }

        private LoadCase GetResponseSpectrum(LoadCase loadCase)
        {
            {
                double eccen = 0;
                if (tryWrapper(() => Doc.Document.LoadCases.ResponseSpectrum.GetEccentricity(loadCase.name,
                    ref eccen)))
                {
                    if (loadCase.ResponseSpectrum == null)
                        loadCase.ResponseSpectrum = new ResponseSpectrum();
                    loadCase.ResponseSpectrum.Eccentricity = eccen;
                }
            }
            {
                int numberLoads = 0;
                string[] LoadName = { }, Func = { }, CSys = { };
                double[] SF = { }, Ang = { };
                if (tryWrapper(() => Doc.Document.LoadCases.ResponseSpectrum.GetLoads(loadCase.name,
                    ref numberLoads, ref LoadName, ref Func, ref SF, ref CSys, ref Ang)))
                {
                    if (loadCase.ResponseSpectrum == null)
                        loadCase.ResponseSpectrum = new ResponseSpectrum();
                    loadCase.ResponseSpectrum.NumberLoads = numberLoads;
                    loadCase.ResponseSpectrum.LoadName = LoadName.ToList();
                    loadCase.ResponseSpectrum.Func = Func.ToList();
                    loadCase.ResponseSpectrum.CSys = CSys.ToList();
                    loadCase.ResponseSpectrum.SF = SF.ToList();
                    loadCase.ResponseSpectrum.Ang = Ang.ToList();
                }
            }
            {
                string modalCase = "";
                if (tryWrapper(() => Doc.Document.LoadCases.ResponseSpectrum.GetModalCase(loadCase.name,
                    ref modalCase)))
                {
                    if (loadCase.ResponseSpectrum == null)
                        loadCase.ResponseSpectrum = new ResponseSpectrum();
                    loadCase.ResponseSpectrum.ModalCase = modalCase;
                }
            }
            return loadCase;
        }
        private LoadCase GetHyperStatic(LoadCase loadCase)
        {
            {
                string Hypercase = "";
                if (tryWrapper(() => Doc.Document.LoadCases.HyperStatic.GetBaseCase(loadCase.name,
                    ref Hypercase)))
                {
                    if (loadCase.HyperStatic == null)
                        loadCase.HyperStatic = new HyperStatic();
                    loadCase.HyperStatic.BaseCase = Hypercase;
                }
            }
            return loadCase;
        }
        #endregion

        public LoadCase LoadCaseToSpeckle(string name)
        {
            LoadCase loadCase = new LoadCase() { name = name };
            var funcList = new List<Func<LoadCase, LoadCase>>(){ GetLoadCase,
                GetStaticLinear, GetStaticNonlinear, GetModalHistoryLinear,
                GetResponseSpectrum, GetHyperStatic};
            foreach (var fun in funcList)
            {
                loadCase = fun(loadCase);
            }
            return loadCase;
        }
    }
}

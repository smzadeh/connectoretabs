﻿using ConnectorETABS.ETABS.UI;
using Speckle.Core.Logging;
using Speckle.DesktopUI;
using System;
using System.Timers;
using ET = ETABSv1;
using SD = System.Diagnostics;
using SW = System.Windows;

namespace ConnectorETABS
{
    public class cPlugin
    {
        public static ET.cPluginCallback pluginCallback { get; set; }
        public static Bootstrapper Bootstrapper { get; set; }
        public static bool isSpeckleClosed { get; set; } = false;

        public Timer SelectionTimer;
        public static void OpenOrFocusSpeckle(UIDocument doc)
        {
            try
            {
                Setup.Init("ConnectorETABS");
                if (Bootstrapper != null)
                {
                    Bootstrapper.ShowRootView();
                    return;
                }

                Bootstrapper = new Bootstrapper()
                {
                    Bindings = new ConnectorBindingsETABS(doc)
                };

                if (SW.Application.Current != null)
                    new StyletAppLoader() { Bootstrapper = Bootstrapper };
                else
                    new App(Bootstrapper);



                var processes = SD.Process.GetProcesses();
                IntPtr ptr = IntPtr.Zero;
                foreach (var process in processes)
                {
                    if (process.ProcessName.ToLower().Contains("etabs"))
                    {
                        ptr = process.MainWindowHandle;
                        break;
                    }
                }
                if (ptr != IntPtr.Zero)
                {
                    //SW.Application.Current.MainWindow.Closed += SpeckleWindowClosed;
                    Bootstrapper.Start(SW.Application.Current);
                    Bootstrapper.SetParent(ptr);
                    SW.Application.Current.MainWindow.Closed += SpeckleWindowClosed;
                }
            }
            catch
            {
                Bootstrapper = null;
            }
        }

        private static void SpeckleWindowClosed(object sender, EventArgs e)
        {
            isSpeckleClosed = true;
        }

        private void SelectionTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (isSpeckleClosed == true)
            {
                pluginCallback.Finish(0);
            }
        }

        private static UIDocument AttachToRunningInstance()
        {
            try
            {
                ET.cHelper helper = new ET.Helper();
                var etabsObject = helper.GetObject("CSI.ETABS.API.ETABSObject");
                var doc = new UIDocument();
                doc.Document = etabsObject.SapModel;
                return doc;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// This is an ETABS required function for information on the plugin
        /// </summary>
        public int Info(ref string Text)
        {
            Text = "This is a Speckle plugin for ETABS";
            return 0;
        }
        /// <summary>
        /// ETABS plugin main function with specific parameters
        /// </summary>
        /// <param name="SapModel"></param>
        /// <param name="ISapPlugin"></param>
        public void Main(ref ET.cSapModel SapModel, ref ET.cPluginCallback ISapPlugin)
        {
            pluginCallback = ISapPlugin;
            UIDocument doc = AttachToRunningInstance();
            if (doc == null)
            {
                ISapPlugin.Finish(0);
                return;
            }
            try
            {
                OpenOrFocusSpeckle(doc);
                SelectionTimer = new Timer(2000) { AutoReset = true, Enabled = true };
                SelectionTimer.Elapsed += SelectionTimer_Elapsed;
                SelectionTimer.Start();
            }
            catch
            {
                ISapPlugin.Finish(0);
                return;
            }
        }
    }
}
